export default function FiltroGeral() {
  $(document).ready(function () {
    
    var urlPrevia = "";
    // var urlPrevia = "/previa-fg-moretti";

    $("select#filtroGeralAreas, select#filtroGeralAplicacoes").on("change", function () {
      var areaSelected = $("select#filtroGeralAreas").val();
      var aplicacaoSelected = $("select#filtroGeralAplicacoes").val();

      if (areaSelected == "") {
        areaSelected = 0;
      }
      if (aplicacaoSelected == "") {
        aplicacaoSelected = 0;
      }
      console.log(areaSelected, aplicacaoSelected, 0);
      filtroGeral(areaSelected, aplicacaoSelected, 0, false);
    });

    var filtroGeral = function (areaSelected, aplicacaoSelected, servicoSelected, show) {
      var url = urlPrevia + "/filtro/" + areaSelected + "/" + aplicacaoSelected + "/" + servicoSelected;

      $.ajax({
        type: "GET",
        url: url,
        beforeSend: function () {},
        success: function (data, textStatus, jqXHR) {
          console.log(data);
          if (aplicacaoSelected == 0) {
            $("#filtroGeralAplicacoes").html("");
            // $("#filtroGaleriaServicos").html("");
            var optionAplicacoes = "<option value='' selected>Tipo de Obra • Aplicação</option>";
            $("#filtroGeralAplicacoes").append(optionAplicacoes);

            data.aplicacoes.forEach((aplicacao) => {
              optionAplicacoes = "<option value='" + aplicacao.id + "'>" + aplicacao.titulo + "</option>";
              $("#filtroGeralAplicacoes").append(optionAplicacoes);
            });
          }

          $("#filtroGeralServicos").html("");

          var optionServicos = "<option value='' selected>Serviço (digite ou selecione)</option>";
          $("#filtroGeralServicos").append(optionServicos);

          data.servicos.forEach((servico) => {
            optionServicos = "<option value='" + servico.id + "'>" + servico.titulo + "</option>";
            $("#filtroGeralServicos").append(optionServicos);
          });

          if (show) {
            window.location.href = window.location.origin + urlPrevia + "/resultados-filtro/" + areaSelected + "/" + aplicacaoSelected + "/" + servicoSelected;
          }

        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR, textStatus, errorThrown);
        },
      });
    };

    $("#btnFiltroGeral").on("click", function (e) {
      e.preventDefault();

      var areaSelected = $("select#filtroGeralAreas").val();
      var aplicacaoSelected = $("select#filtroGeralAplicacoes").val();
      var servicoSelected = $("select#filtroGeralServicos").val();

      if(areaSelected == "") {
        areaSelected = 0;
      }
      if(aplicacaoSelected == "") {
        aplicacaoSelected = 0;
      }
      if(servicoSelected == "") {
        servicoSelected = 0;
      }

      console.log(areaSelected, aplicacaoSelected, servicoSelected);
      filtroGeral(areaSelected, aplicacaoSelected, servicoSelected, true);
    });

    //     var showObrasFiltradas = function(obras, areasObra) {
    //       // console.log(areasObra);
    //       $("#todasObrasGaleria").html("").css({ display: "none", opacity: 0 });

    //       obras.forEach((obra) => {
    //         var htmlObrasArea = "<a href='"+window.location.origin+urlPrevia+"/galeria-de-imagens/"+obra.slug+"' class='galeria-obra' id='obra"+obra.id+"'><div class='capa-obra'><img src='"+window.location.origin+urlPrevia+"/assets/img/obras/"+obra.capa+"' alt='' class='img-obra'><span class='overlay-aplicacao'>"+obra.aplicacao_titulo+"</span></div><p class='titulo-obra'>"+obra.titulo+"</p><div class='areas-obra' id='areasObra"+obra.id+"'></div></a>";
    //         $("#todasObrasGaleria").append(htmlObrasArea);

    //         areasObra.forEach((area) => {
    //           if(area.obra_id == obra.id) {
    //             var htmlAreas = "<div class='obra-area' id='area"+area.area_id+"'>"+area.area_titulo+"</div>";
    //             $("#areasObra"+area.obra_id).append(htmlAreas);
    //           }
    //         });
    //       });
    //       // Removendo margin-right de todos os elementos multiplos de 3
    //       $(".galeria-obra:nth-child(3n)").css("margin-right", "0");

    //       $("#todasObrasGaleria").show().animate({ opacity: 1 }, 1600, "swing");
    //     }
  });
}
