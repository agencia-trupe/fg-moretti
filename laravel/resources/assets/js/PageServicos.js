export default function PageServicos() {
  $(document).ready(function () {
    
    var urlPrevia = "";
    // var urlPrevia = "/previa-fg-moretti";

    $(".servicos .itens-submenu .link-submenu")
      .on("click", function (e) {
        e.preventDefault();

        $(".servicos .itens-submenu .link-submenu.active").removeClass("active");
        $(this).addClass("active");

        $(".sublinks-servicos").html("").css("padding", "0");

        $(this).children(".sublinks-servicos").css("padding", "5px 15px 10px 15px").toggle("fast");

        var url = window.location.origin+urlPrevia+"/servicos/grupo/"+$(this).attr("grupo_id");

        $.ajax({
          type: "GET",
          url: url,
          success: function (data, textStatus, jqXHR) {
            $(".sublink-submenu").html("").show();

            data.servicos.forEach((servico) => {
              var htmlSublinks = "<p class='sublink-submenu' id='linkServico"+servico.id+"' servico_slug='"+servico.slug+"'><span>•</span>"+servico.titulo+"</p>";
              $("#grupo" + servico.grupo_id + " .sublinks-servicos").append(htmlSublinks);
            });

            var servicoId = localStorage.getItem("servicoId");

            if (servicoId) {
              $("section.servicos .sublink-submenu#linkServico" + servicoId).addClass("active");
            }
          },
          error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR, textStatus, errorThrown);
          },
        });
      })
      .on("click", ".sublink-submenu", function (e) {
        e.stopPropagation();

        $(".servicos .sublinks-servicos .sublink-submenu.active").removeClass("active");
        $(this).addClass("active");

        var url = window.location.origin+urlPrevia+"/servicos/list/"+$(this).attr("servico_slug");

        getDadosServicos(url);
      });

    var getDadosServicos = function (url) {
      $("#conteudoSubmenuServicos").fadeOut("slow", function () {
        $.ajax({
          type: "GET",
          url: url,
          success: function (data, textStatus, jqXHR) {
            console.log(data);
            $("#conteudoSubmenuServicos").html("").show();

            var htmlServico = "<h4 class='titulo-conteudo'>"+data.servico.titulo+"</h4><div class='texto-conteudo'>"+data.servico.texto+"</div>";
            $("#conteudoSubmenuServicos").append(htmlServico);

            if(data.areas.length > 0) {
              var htmlDivAreasAplicacoes = "<div class='areas-aplicacoes'></div>";
              $("#conteudoSubmenuServicos").append(htmlDivAreasAplicacoes);
              
              data.areas.forEach((area) => {
                var htmlAreas = "<div class='area' id='divArea"+area.id+"'><a href='"+window.location.origin+urlPrevia+"/areas-de-negocio/"+area.slug+"' class='link-area' id='area"+area.id+"'>"+area.titulo+"</a></div>";
                $("#conteudoSubmenuServicos .areas-aplicacoes").append(htmlAreas);

                data.aplicacoes.forEach((aplicacao) => {
                  if(aplicacao.area_id == area.id) {
                    var htmlAplicacao = "<a href='"+window.location.origin+urlPrevia+"/aplicacoes/"+aplicacao.slug+"' class='link-aplicacao' id='aplicacao"+aplicacao.id+"'>» "+aplicacao.titulo+"</a>";
                    $("#conteudoSubmenuServicos #divArea"+aplicacao.area_id).append(htmlAplicacao);
                  }
                });
              });
            }
          
            if(data.imagens.length > 0) {
              var htmlDivImagens = "<div class='servico-imagens'></div>";
              $("#conteudoSubmenuServicos").append(htmlDivImagens);
            }

            data.imagens.forEach((imagem) => {
              var htmlImagens = "<img src='"+window.location.origin+urlPrevia+"/assets/img/servicos/"+imagem.imagem+"' alt='' class='img-servico'>";
              $("#conteudoSubmenuServicos .servico-imagens").append(htmlImagens);
            });

            // SERVIÇOS - ROTAS PARA AREAS DE NEGOCIO
            $("#conteudoSubmenuServicos .areas-aplicacoes .link-area").click(function (e) {
              e.preventDefault();
              localStorage.setItem("areaId", $(this).attr("id"));
              window.location.href = $(this).attr("href");
            });

            // SERVIÇOS - ROTAS PARA APLICAÇÕES
            $("#conteudoSubmenuServicos .areas-aplicacoes .link-aplicacao").click(function (e) {
              e.preventDefault();
              localStorage.setItem("aplicacaoId", $(this).attr("id"));
              window.location.href = $(this).attr("href");
            });
          },
          error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR, textStatus, errorThrown);
          },
        });
      });
    };
  });
}
