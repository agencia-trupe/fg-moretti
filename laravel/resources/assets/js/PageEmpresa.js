export default function PageEmpresa() {
  $(document).ready(function () {
    
    var urlPrevia = "";
    // var urlPrevia = "/previa-fg-moretti";

    // TROCA SUBMENU EMPRESA
    $(".empresa .itens-submenu .link-submenu").click(function (e) {
      e.preventDefault();

      $(".empresa .itens-submenu .link-submenu.active").removeClass("active");
      $(this).addClass("active");

      getDadosEmpresa(this.href);
    });

    var getDadosEmpresa = function (url) {
      $("#conteudoSubmenuEmpresa").fadeOut("slow", function () {
        $.ajax({
          type: "GET",
          url: url,
          success: function (data, textStatus, jqXHR) {
            console.log(data);
            $("#conteudoSubmenuEmpresa").html("").show();

            var html = "<div class='img-empresa'><img src='"+window.location.origin+urlPrevia+"/assets/img/layout/"+data.imagem+"' alt='' class='img-empresa'></div><div class='textos-empresa'><h3 class='titulo'>"+data.titulo+"</h3><div class='texto'>"+data.resultado.texto+"</div></div>";

            if (data.submenu == "historia") {
              $("#conteudoSubmenuEmpresa").append(html);
            }

            if (data.submenu == "nossa-otica") {
              $("#conteudoSubmenuEmpresa").append(html);
              var visao = "<div class='visao'><h4 class='titulo-caixa'>VISÃO</h4><div class='texto-caixa'>"+data.resultado.visao+"</div></div>";
              var missao = "<div class='missao'><h4 class='titulo-caixa'>MISSÃO</h4><div class='texto-caixa'>"+data.resultado.missao+"</div></div>";
              var valores = "<div class='valores'><h4 class='titulo-caixa'>VALORES</h4><div class='texto-caixa'>"+data.resultado.valores +"</div></div>";
              $(".textos-empresa").append(visao, missao, valores);
            }

            if (data.submenu == "corpo-tecnico") {
              $("#conteudoSubmenuEmpresa").append(html);

              data.equipe.forEach((element) => {
                var equipe = "<div class='equipe'><img src='"+window.location.origin+urlPrevia+"/assets/img/corpo-tecnico-equipe/"+element.foto+"' alt='' class='img-equipe'><div class='dados'><h6 class='titulo'>"+element.nome+"</h6><div class='texto'>"+element.texto_sobre+"</div></div></div>";
                $(".textos-empresa").append(equipe);
              });
            }

            window.history.pushState("","", urlPrevia + "/empresa/" + data.submenu
            );
          },
          error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR, textStatus, errorThrown);
          },
        });
      });
    };
  });
}