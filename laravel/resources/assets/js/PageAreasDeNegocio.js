export default function PageAreasDeNegocio() {
  $(document).ready(function () {

    var urlPrevia = "";
    // var urlPrevia = "/previa-fg-moretti";
  
    // TROCA SUBMENU ÁREAS DE NEGÓCIO
    $(".areas-de-negocio .areas .link-area").click(function (e) {
      e.preventDefault();
  
      $(".areas-de-negocio .areas .link-area.active").removeClass("active");
      $(this).addClass("active");
      
      getDadosAreasDeNegocio(this.href);
    });
  
    var getDadosAreasDeNegocio = function (url) {
      $("#conteudoAreaDeNegocio").fadeOut("slow", function () {
        $.ajax({
          type: "GET",
          url: url,
          success: function (data, textStatus, jqXHR) {
            console.log(data);
            $("#conteudoAreaDeNegocio").html("").show();
            
            var htmlGeral = "<div class='img-titulo' id='area"+data.areas.id+"'><img src='"+window.location.origin+urlPrevia+"/assets/img/areas-de-negocio/"+data.areas.imagem+"' class='img-area'><h4 class='titulo'>"+data.areas.titulo+"</h4></div><div class='texto-pt1'>"+data.areas.texto_inicial+"</div><div class='aplicacoes'><h6 class='titulo' id='aplicacaoArea"+data.areas.id+"'>APLICAÇÕES</h6></div><div class='texto-pt2'>"+data.areas.texto_servicos+"</div><div class='servicos'><h6 class='titulo' id='servicoArea"+data.areas.id+"'>SERVIÇOS</h6></div>";
            $("#conteudoAreaDeNegocio").append(htmlGeral);
            
            data.aplicacoes.forEach((aplicacao) => {
              var htmlAplicacoes = "<a href='"+window.location.origin+urlPrevia+"/aplicacoes/"+aplicacao.slug+"' class='aplicacao' id='aplicacao"+aplicacao.id+"'><span>»</span>"+aplicacao.titulo+"</a>";
              $("#conteudoAreaDeNegocio .aplicacoes").append(htmlAplicacoes);
            });
            
            data.servicos.forEach((servico) => {
              var htmlServicos = "<a href='"+window.location.origin+urlPrevia+"/servicos/"+servico.id+"' class='servico' id='grupo"+servico.grupo_id+"' servico='"+servico.id+"'><p class='nome'><span>•</span>"+servico.titulo+"</p><img src='"+window.location.origin+urlPrevia+"/assets/img/layout//setinha-dupla-vinho.svg"+"' alt='' class='img-seta'></a>";
              $("#conteudoAreaDeNegocio .servicos").append(htmlServicos);
            });

            // ÁREAS DE NEGOCIO - ROTA PARA SERVIÇOS
            $("section.areas-de-negocio .servicos .servico").click(function(e) {
              e.preventDefault();
              localStorage.setItem("grupoId", $(this).attr("id"));
              localStorage.setItem("servicoId", $(this).attr("servico"));
              window.location.href = $(this).attr("href");
            });
  
            window.history.pushState("","", urlPrevia + "/areas-de-negocio/" + data.areas.slug
            );
          },
          error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR, textStatus, errorThrown);
          },
        });
      });
    };
  });
}
  