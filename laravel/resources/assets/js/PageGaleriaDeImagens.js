export default function PageGaleriaDeImagens() {
  $(document).ready(function () {
    
    var urlPrevia = "";
    // var urlPrevia = "/previa-fg-moretti";

    // Removendo margin-right de todos os elementos multiplos de 3
    $(".galeria-obra:nth-child(3n)").css("margin-right", "0");

    // BTN VER MAIS - GALERIA DE IMAGENS / OBRAS
    var itensObras = $(".galeria-obra");
    var spliceItensObras = 6;
    if (itensObras.length <= spliceItensObras) {
      $(".btn-mais-obras").hide();
    }

    var setDivObras = function (itens, spliceItensObras) {
      console.log(itens);

      var spliceItens = itens.splice(0, spliceItensObras);
      $(".galeria-obras .centralizado").append(spliceItens);
      $(spliceItens).show();

      if (itens.length <= 0) {
        $(".btn-mais-obras").hide();
      } else {
        $(".btn-mais-obras").show();
      }

      $(".galeria-obras .centralizado").show();
    };

    $(".btn-mais-obras").click(function () {
      setDivObras(itensObras, spliceItensObras);
    });

    $(".galeria-obra").hide();
    setDivObras(itensObras, spliceItensObras);

    // FILTRO GALERIA DE IMAGENS

    $("select#filtroGaleriaAreas, select#filtroGaleriaAplicacoes").on("change", function () {
      var areaSelected = $("select#filtroGaleriaAreas").val();
      var aplicacaoSelected = $("select#filtroGaleriaAplicacoes").val();
      
      if(areaSelected == "") {
        areaSelected = 0;
      }
      if(aplicacaoSelected == "") {
        aplicacaoSelected = 0;
      }
      
      console.log(areaSelected, aplicacaoSelected, 0);
      filtroGaleria(areaSelected, aplicacaoSelected, 0, false);
    });

    var filtroGaleria = function (areaSelected, aplicacaoSelected, servicoSelected, show) {
      var url = urlPrevia + "/galeria-de-imagens/filtro/" + areaSelected + "/" + aplicacaoSelected + "/" + servicoSelected;
      console.log(url);

      $.ajax({
        type: "GET",
        url: url,
        beforeSend: function () {
        },
        success: function (data, textStatus, jqXHR) {
          // console.log(data);
          if (aplicacaoSelected == 0) {
            $("#filtroGaleriaAplicacoes").html("");
            var optionAplicacoes = "<option value='' selected>Tipo de Obra • Aplicação</option>";
            $("#filtroGaleriaAplicacoes").append(optionAplicacoes);

            data.aplicacoes.forEach((aplicacao) => {
              optionAplicacoes = "<option value='"+aplicacao.id+"'>"+aplicacao.titulo+"</option>";
              $("#filtroGaleriaAplicacoes").append(optionAplicacoes);
            });
          }

          $("#filtroGaleriaServicos").html("");

          var optionServicos = "<option value='' selected>Serviço (digite ou selecione)</option>";
          $("#filtroGaleriaServicos").append(optionServicos);

          data.servicos.forEach((servico) => {
            optionServicos = "<option value='"+servico.id+"'>"+servico.titulo+"</option>";
            $("#filtroGaleriaServicos").append(optionServicos);
          });

          if (show) {
            showObrasFiltradas(data.obras, data.areasObra);
          }
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR, textStatus, errorThrown);
        },
      });
    };

    $("#btnFiltroGaleria").on("click", function (e) {
      e.preventDefault();
      
      var areaSelected = $("select#filtroGaleriaAreas").val();
      var aplicacaoSelected = $("select#filtroGaleriaAplicacoes").val();
      var servicoSelected = $("select#filtroGaleriaServicos").val();
      
      if(areaSelected == "") {
        areaSelected = 0;
        //servicoSelected = 0;
      }
      if(aplicacaoSelected == "") {
        aplicacaoSelected = 0;
        //servicoSelected = 0;
      }
      if(servicoSelected == "") {
        servicoSelected = 0;
      }

      console.log(areaSelected, aplicacaoSelected, servicoSelected);
      filtroGaleria(areaSelected, aplicacaoSelected, servicoSelected, true);
    });
      
    var showObrasFiltradas = function(obras, areasObra) {
      // console.log(areasObra);
      $("#todasObrasGaleria").html("").css({ display: "none", opacity: 0 });

      obras.forEach((obra) => {
        var htmlObrasArea = "<a href='"+window.location.origin+urlPrevia+"/galeria-de-imagens/"+obra.slug+"' class='galeria-obra' id='obra"+obra.id+"'><div class='capa-obra'><img src='"+window.location.origin+urlPrevia+"/assets/img/obras/"+obra.capa+"' alt='' class='img-obra'><span class='overlay-aplicacao'>"+obra.aplicacao_titulo+"</span></div><p class='titulo-obra'>"+obra.titulo+"</p><div class='areas-obra' id='areasObra"+obra.id+"'></div></a>";
        $("#todasObrasGaleria").append(htmlObrasArea);

        areasObra.forEach((area) => {
          if(area.obra_id == obra.id) {
            var htmlAreas = "<div class='obra-area' id='area"+area.area_id+"'>"+area.area_titulo+"</div>";
            $("#areasObra"+area.obra_id).append(htmlAreas);
          }
        });
      });
      
      $(".galeria-obras .centralizado").show().animate({ display: "block", opacity: 1, height: "100%" }, 1600, "swing");

      // Removendo margin-right de todos os elementos multiplos de 3
      $(".galeria-obra:nth-child(3n)").css("margin-right", "0");

      // // BTN VER MAIS - GALERIA DE IMAGENS / OBRAS
      var itensObrasAjax = $(".galeria-obra");
      if (itensObrasAjax.length <= spliceItensObras) {
        $(".btn-mais-obras").hide();
      }

      itensObras = itensObrasAjax;

      $(".galeria-obra").hide();
      setDivObras(itensObrasAjax, spliceItensObras);

    };
  });
}
