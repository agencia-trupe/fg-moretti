export default function PageAplicacoes() {
    $(document).ready(function () {
      
      var urlPrevia = "";
      // var urlPrevia = "/previa-fg-moretti";
  
      // Abertura dos dados das áreas de negócio
      $("section.aplicacoes .conteudo-submenu h2.titulo-area").click(function (e) {
        e.preventDefault();

        $(this).toggleClass("active");

        $(this).parent().children('.texto-area').toggle();
        $(this).parent().children('.servicos-area').toggle();
      });

      // TROCA SUBMENU APLICAÇÕES
      $(".aplicacoes .itens-submenu .link-submenu").click(function (e) {
        e.preventDefault();
  
        $(".aplicacoes .itens-submenu .link-submenu.active").removeClass("active");
        $(this).addClass("active");
  
        getDadosAplicacoes(this.href);
      });
  
      var getDadosAplicacoes = function (url) {
        $("#conteudoSubmenuAplicacoes").fadeOut("slow", function () {
          $.ajax({
            type: "GET",
            url: url,
            success: function (data, textStatus, jqXHR) {
              console.log(data);
              $("#conteudoSubmenuAplicacoes").html("").show();
  
              var htmlGeral = "<div class='img-aplicacao'><img src='"+window.location.origin+urlPrevia+"/assets/img/aplicacoes/"+data.aplicacoes.imagem+"' alt=''></div><div class='textos-aplicacao'><h3 class='titulo'>"+data.aplicacoes.titulo+"</h3><div class='texto'>"+data.aplicacoes.texto+"</div></div><div class='areas' id='aplicacoesAreas'></div>";
              $("#conteudoSubmenuAplicacoes").append(htmlGeral);
  
              data.aplicacoesAreas.forEach((area) => {
                var htmlAreas = "<div class='area'><h2 class='titulo-area' id='tituloArea"+area.area_id+"'>"+area.area_titulo+"<img src='"+window.location.origin+urlPrevia+"/assets/img/layout/setinha-dupla.svg"+"' alt='' class='img-seta-link'><img src='"+window.location.origin+urlPrevia+"/assets/img/layout/seta-dupla-abaixo.svg"+"' alt='' class='img-seta-active'></h2><div class='texto-area' style='display: none;'>"+area.texto_area+"</div><div class='servicos-area' id='servicosArea"+area.area_id+"' style='display: none;'></div></div>";
                $("#aplicacoesAreas").append(htmlAreas);

                data.aplicacoesServicos.forEach((servico) => {

                  if(area.area_id == servico.area_id) {
                    console.log(servico);
                    var htmlServicos = "<a href='"+window.location.origin+urlPrevia+"/servicos/"+servico.servico_id+"' class='servico' id='grupo"+servico.grupo_id+"' servico='"+servico.servico_id+"'><p class='nome'><span>•</span>"+servico.servico_titulo+"</p><img src='"+window.location.origin+urlPrevia+"/assets/img/layout/setinha-dupla-vinho.svg"+"' alt='' class='img-seta'></a>";
                    $("#servicosArea"+servico.area_id).append(htmlServicos);
                  }
                });
              });

              // Abertura dos dados das áreas de negócio
              $("section.aplicacoes .conteudo-submenu h2.titulo-area").click(function (e) {
                e.preventDefault();

                $("section.aplicacoes .conteudo-submenu h2.titulo-area").removeClass("active");
                $(this).addClass("active");

                $(this).parent().children('.texto-area').toggle();
                $(this).parent().children('.servicos-area').toggle();
              });

              // APLICAÇÕES - ROTA PARA SERVIÇOS
              $("section.aplicacoes .servicos-area .servico").click(function(e) {
                e.preventDefault();
                localStorage.setItem("grupoId", $(this).attr("id"));
                localStorage.setItem("servicoId", $(this).attr("servico"));
                window.location.href = $(this).attr("href");
              });

              window.history.pushState("","", urlPrevia + "/aplicacoes/" + data.aplicacoes.slug
              );
            },
            error: function (jqXHR, textStatus, errorThrown) {
              console.log(jqXHR, textStatus, errorThrown);
            },
          });
        });
      };
    });
  }