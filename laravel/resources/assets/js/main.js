import AjaxSetup from "./AjaxSetup";
import MobileToggle from "./MobileToggle";
import FiltroGeral from "./FiltroGeral";
import PageEmpresa from "./PageEmpresa";
import PageAreasDeNegocio from "./PageAreasDeNegocio";
import PageAplicacoes from "./PageAplicacoes";
import PageServicos from "./PageServicos";
import PageGaleriaDeImagens from "./PageGaleriaDeImagens";
import Ordenacao from "./Ordenacao";

AjaxSetup();
MobileToggle();
FiltroGeral();
PageEmpresa();
PageAreasDeNegocio();
PageAplicacoes();
PageServicos();
PageGaleriaDeImagens();
Ordenacao();

$(document).ready(function () {
  var urlPrevia = "";
  // var urlPrevia = "/previa-fg-moretti";

  // HOME - BANNERS
  $(".banners").cycle({
    slides: ".banner",
    fx: "fade",
    speed: 600,
  });

  // SLIDES + PAGERS
  $(".slides").cycle({
    slides: ".slide",
    fx: "fade",
    speed: 600,
    pager: ".cycle-pager",
  });

  // SELECT COM OPÇÃO DE DIGITAR
  $(".filtro select.servicos").select2();

  // ARTIGOS TÉCNICOS
  $(".artigos-tecnicos .artigos")
    .masonry({
      itemSelector: ".artigo",
      columnWidth: ".artigo",
      percentPosition: true,
    })
    .on("layoutComplete", function (event, laidOutItems) {
      $(".artigos-tecnicos .artigos").masonry("layout");
    });

  // BUSCA HEADER
  $(".link-busca").click(function (e) {
    e.preventDefault();
    $(".busca-header").show();
    e.stopPropagation();
  });
  $(window).scroll(function () {
    $(".busca-header").hide();
  });
  $(".busca-header").click(function (e) {
    e.stopPropagation();
  });
  $(document).click(function () {
    $(".busca-header").hide();
  });

  // Salvando o estado da página no localStorage (clique do link para saber em qual incluir a classe active)
  var areaId = localStorage.getItem("areaId");
  var aplicacaoId = localStorage.getItem("aplicacaoId");
  var grupoId = localStorage.getItem("grupoId");

  if (areaId) {
    $("section.areas-de-negocio .link-area#" + areaId).addClass("active");
  }

  if (aplicacaoId) {
    $("section.aplicacoes .link-submenu#" + aplicacaoId).addClass("active");
  }

  if (grupoId) {
    $("section.servicos .link-submenu#" + grupoId).addClass("active");
    $("section.servicos .link-submenu#" + grupoId)
      .children(".sublinks-servicos")
      .click();
  }

  // FOOTER - ROTAS PARA AREAS DE NEGOCIO
  $(".footer .areas-de-negocio .area").click(function (e) {
    e.preventDefault();
    localStorage.setItem("areaId", $(this).attr("id"));
    window.location.href = $(this).attr("href");
  });

  // FOOTER - ROTAS PARA APLICAÇÕES
  $(".footer .aplicacoes .aplicacao").click(function (e) {
    e.preventDefault();
    localStorage.setItem("aplicacaoId", $(this).attr("id"));
    window.location.href = $(this).attr("href");
  });

  // FOOTER - ROTAS PARA SERVIÇOS
  $(".footer .servicos .grupo").click(function (e) {
    e.preventDefault();
    localStorage.setItem("grupoId", $(this).attr("id"));
    window.location.href = window.location.origin + urlPrevia + "/servicos";
  });

  // FILTRO - ROTAS PARA SERVIÇOS
  $(".resultados-filtro .servicos .link-filtro").click(function (e) {
    e.preventDefault();
    localStorage.setItem("grupoId", $(this).attr("id"));
    localStorage.setItem("servicoId", $(this).attr("servico"));
    window.location.href = $(this).attr("href");
  });

  // FILTRO - ROTAS PARA APLICAÇÕES
  $(".resultados-filtro .aplicacoes .link-filtro").click(function (e) {
    e.preventDefault();
    localStorage.setItem("aplicacaoId", $(this).attr("id"));
    window.location.href = $(this).attr("href");
  });

  // FILTRO - ROTAS PARA ÁREAS
  $(".resultados-filtro .areas .link-filtro").click(function (e) {
    e.preventDefault();
    localStorage.setItem("areaId", $(this).attr("id"));
    window.location.href = $(this).attr("href");
  });

  // HEADER NAV - ROTA PARA AREAS DE NEGOCIO
  $("header .itens-menu #area1").click(function (e) {
    e.preventDefault();
    $("section.areas-de-negocio .link-area").first().addClass("active");
    localStorage.setItem("areaId", $(this).attr("id"));
    window.location.href = $(this).attr("href");
  });

  // HEADER NAV - ROTA PARA APLICAÇÕES
  $("header .itens-menu #aplicacao1").click(function (e) {
    e.preventDefault();
    $("section.aplicacoes .link-area").first().addClass("active");
    localStorage.setItem("aplicacaoId", $(this).attr("id"));
    window.location.href = $(this).attr("href");
  });

  // HOME - ROTA PARA AREAS DE NEGOCIO
  $("section.home .areas .area").click(function (e) {
    e.preventDefault();
    localStorage.setItem("areaId", $(this).attr("id"));
    window.location.href = $(this).attr("href");
  });

  // ÁREAS DE NEGOCIO - ROTA PARA APLICAÇÕES
  $("section.areas-de-negocio .aplicacoes .aplicacao").click(function (e) {
    e.preventDefault();
    localStorage.setItem("aplicacaoId", $(this).attr("id"));
    window.location.href = $(this).attr("href");
  });

  // ÁREAS DE NEGOCIO - ROTA PARA SERVIÇOS
  $("section.areas-de-negocio .servicos .servico").click(function (e) {
    e.preventDefault();
    localStorage.setItem("grupoId", $(this).attr("id"));
    localStorage.setItem("servicoId", $(this).attr("servico"));
    window.location.href = $(this).attr("href");
  });

  // APLICAÇÕES - ROTA PARA SERVIÇOS
  $("section.aplicacoes .servicos-area .servico").click(function (e) {
    e.preventDefault();
    localStorage.setItem("grupoId", $(this).attr("id"));
    localStorage.setItem("servicoId", $(this).attr("servico"));
    window.location.href = $(this).attr("href");
  });

  // GALERIA DE IMAGENS-SHOW - ROTA PARA ÁREAS DE NEGÓCIO
  $("section.informacoes-obra .areas-servicos .link-area").click(function (e) {
    e.preventDefault();
    localStorage.setItem("areaId", $(this).attr("id"));
    window.location.href = $(this).attr("href");
  });

  // GALERIA DE IMAGENS-SHOW - ROTA PARA SERVIÇOS
  $("section.informacoes-obra .areas-servicos .link-servico").click(function (
    e
  ) {
    e.preventDefault();
    localStorage.setItem("grupoId", $(this).attr("id"));
    localStorage.setItem("servicoId", $(this).attr("servico"));
    window.location.href = $(this).attr("href");
  });

  // RESULTADOS BUSCA - ROTA PARA APLICACOES
  $("section.resultados-busca .resultado .link-resultado.aplicacao").click(
    function (e) {
      e.preventDefault();
      localStorage.setItem("aplicacaoId", $(this).attr("id"));
      window.location.href = $(this).attr("href");
    }
  );

  // RESULTADOS BUSCA - ROTA PARA ÁREAS DE NEGÓCIO
  $("section.resultados-busca .resultado .link-resultado.area").click(function (
    e
  ) {
    e.preventDefault();
    localStorage.setItem("areaId", $(this).attr("id"));
    window.location.href = $(this).attr("href");
  });

  // RESULTADOS BUSCA - ROTA PARA SERVIÇOS
  $("section.resultados-busca .resultado .link-resultado.servico").click(
    function (e) {
      e.preventDefault();
      localStorage.setItem("grupoId", $(this).attr("id"));
      localStorage.setItem("servicoId", $(this).attr("servico"));
      window.location.href = $(this).attr("href");
    }
  );

  // FOOTER - acive no titulo dos grupos ÁREAS DE NEGÓCIO, APLICAÇÕES E SERVIÇOS
  if (window.location.pathname.search("/areas-de-negocio") === 0) {
    $(".footer .areas-de-negocio .titulo-areas").addClass("active");
    $(".footer .aplicacoes .titulo-aplicacoes").removeClass("active");
    $(".footer .servicos .titulo-servicos").removeClass("active");
  }

  if (window.location.pathname.search("/aplicacoes") === 0) {
    $(".footer .areas-de-negocio .titulo-areas").removeClass("active");
    $(".footer .aplicacoes .titulo-aplicacoes").addClass("active");
    $(".footer .servicos .titulo-servicos").removeClass("active");
  }

  if (window.location.pathname.search("/servicos") === 0) {
    $(".footer .areas-de-negocio .titulo-areas").removeClass("active");
    $(".footer .aplicacoes .titulo-aplicacoes").removeClass("active");
    $(".footer .servicos .titulo-servicos").addClass("active");
  }

  // AVISO DE COOKIES
  $(".aviso-cookies").hide();

  if (window.location.href == routeHome) {
    $(".aviso-cookies").show();

    $(".aceitar-cookies").click(function () {
      var url = window.location.origin + "/aceite-de-cookies";

      $.ajax({
        type: "POST",
        url: url,
        success: function (data, textStatus, jqXHR) {
          $(".aviso-cookies").hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR, textStatus, errorThrown);
        },
      });
    });
  }

  // ÁREA DO CLIENTE - máscaras
  $(".cnpj").mask("00.000.000/0000-00", { reverse: true });
  $(".telefone-usuario").mask("(00) 000000000");

  // ÁREA DO CLIENTE - select obras - add usuario
  $("select#obrasGerente").on("change", function () {
    var obraId = $("select#obrasGerente").val();
    var gerenteId = $(".form-edit-gerente .input-gerente").val();

    $(".form-add-usuario .input-obra").val(obraId);

    console.log(obraId, gerenteId);
    showUsuariosGerente(obraId, gerenteId);
  });

  var showUsuariosGerente = function (obraId, gerenteId) {
    var url =
      urlPrevia +
      "/area-do-cliente/identificacao/list/" +
      obraId +
      "/" +
      gerenteId;

    $.ajax({
      type: "GET",
      url: url,
      beforeSend: function () {},
      success: function (data, textStatus, jqXHR) {
        console.log(data);
        // $(".situacao-usuario").html("");
        $(".usuarios-gerente-obra").html("");
        data.usuarios.forEach((usuario) => {
          var ativo = usuario.ativo == 1 ? 'ATIVO' : 'PENDENTE';
          var formUsuario =
            "<div class='dados-usuario'><p class='email-usuario'>" +
            usuario.email +
            "</p><p class='situacao-usuario'>"+ativo+"</p><a href='" +
            window.location.origin +
            urlPrevia +
            "/area-do-cliente/identificacao/excluir/" +
            data.gerente.id +
            "/" +
            data.obra.id +
            "/" +
            usuario.id +
            "' class='link-excluir-usuario'><img src='" +
            window.location.origin +
            urlPrevia +
            "/assets/img/layout/areacliente-x-fechar.svg" +
            "' alt='Excluir' class='img-excluir'></a></div>";
          $(".usuarios-gerente-obra").append(formUsuario);
        });
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR, textStatus, errorThrown);
      },
    });
  };

  $(".link-excluir-usuario").click(function (e) {
    e.preventDefault();
    console.log($(this).attr("href"));
    if (
      confirm(
        "Tem certeza que deseja excluir o acesso do usuário para a obra selecionada?"
      )
    ) {
      window.location.href = $(this).attr("href");
    }
  });
});
