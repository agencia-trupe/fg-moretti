@extends('frontend.common.template')

@section('content')

<section class="resultados-filtro">
    <div class="centralizado">
        <h2 class="titulo">RESULTADO DO FILTRO</h2>

        @if(count($areas))
        <div class="resultados areas">
            @foreach($areas as $area)
            <div class="resultado">
                <div class="secao"><span>//</span>áreas de negócio</div>
                <a href="{{ route('areas-de-negocio', $area->slug) }}" class="link-filtro" id="area{{$area->id}}">{{ $area->titulo }}
                </a>
            </div>
            @endforeach
        </div>
        @endif

        @if(count($aplicacoes))
        <div class="resultados aplicacoes">
            @foreach($aplicacoes as $aplicacao)
            <div class="resultado">
                <div class="secao"><span>//</span>aplicações</div>
                <a href="{{ route('aplicacoes', $aplicacao->slug) }}" class="link-filtro" id="aplicacao{{$aplicacao->id}}">{{ $aplicacao->titulo }}
                </a>
            </div>
            @endforeach
        </div>
        @endif

        @if(count($servicos))
        <div class="resultados servicos">
            @foreach($servicos as $servico)
            <div class="resultado">
                <div class="secao"><span>//</span>serviços</div>
                <a href="{{ route('servicos.show', $servico->id) }}" class="link-filtro" id="grupo{{$servico->grupo_id}}" servico="{{$servico->id}}" >{{ $servico->titulo }}
                </a>
            </div>
            @endforeach
        </div>
        @endif

        @if(count($obras))
        <div class="resultados">
            @foreach($obras as $obra)
            <div class="resultado">
                <div class="secao"><span>//</span>obras • galeria de imagens</div>
                <a href="{{ route('galeria-de-imagens.show', $obra->slug) }}" class="link-filtro">{{ $obra->titulo }}
                </a>
            </div>
            @endforeach
        </div>
        @endif

        @if(!count($aplicacoes) && !count($servicos) && !count($obras) && !count($areas))
        <div class="nenhum-resultado">
            Nenhum resultado encontrado para o filtro selecionado.
        </div>
        @endif

    </div>

    @include('frontend.filtro')

</section>

@endsection