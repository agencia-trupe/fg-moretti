@extends('frontend.common.template')

@section('content')

<section class="termos-de-uso">
    <div class="centralizado">
        <h2 class="titulo">TERMOS DE USO DO SITE</h2>
        <div class="texto">{!! $termos->texto !!}</div>
    </div>

    @include('frontend.filtro')

</section>

@endsection