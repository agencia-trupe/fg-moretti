@extends('frontend.common.template')

@section('content')

<section class="clientes">
    <div class="centralizado">
        @foreach($clientes as $cliente)
        @if($cliente->link)
        <a href="{{ $cliente->link }}" target="_blank" class="cliente" title="{{ $cliente->nome }}" id="cliente{{$cliente->id}}">
            <img src="{{ asset('assets/img/clientes/'.$cliente->imagem) }}" alt="{{ $cliente->nome }}" class="img-cliente">
        </a>
        @else
        <div class="cliente" title="{{ $cliente->nome }}" id="cliente{{$cliente->id}}">
            <img src="{{ asset('assets/img/clientes/'.$cliente->imagem) }}" alt="{{ $cliente->nome }}" class="img-cliente">
        </div>
        @endif
        @endforeach
    </div>

    @include('frontend.filtro')

</section>

@endsection