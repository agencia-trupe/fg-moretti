@extends('frontend.common.template')

@section('content')

<section class="pagina-contato">
    <div class="centralizado">
        <div class="contatos">
            @php
            $telefone = "+55".str_replace(" ", "", $contato->telefone);
            @endphp
            <a href="tel:{{ $telefone }}" class="telefone">{{ $contato->telefone }}</a>
            <p class="endereco">{{ $contato->endereco }}</p>
            <p class="atendimento">Horários de Atendimento:</p>
            <p class="horario">{{ $contato->horario }}</p>
        </div>
        <div class="fale-conosco">
            <h4 class="titulo">FALE CONOSCO</h4>
            <form action="{{ route('contato.post') }}" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="dados">
                    <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                    <input type="text" name="empresa" placeholder="empresa" value="{{ old('empresa') }}" required>
                    <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}">
                    <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    <select name="assunto" class="assunto">
                        <option value="" selected>assunto</option>
                        @foreach($assuntos as $key => $assunto)
                        <option value="{{ $key }}">{{ $assunto }}</option>
                        @endforeach
                    </select>
                </div>
                <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                <div class="buttons-contato">
                    <div class="anexar-arquivo">
                        <label for="files" class="btn-anexar">ANEXAR ARQUIVO</label>
                        <input type="file" id="files" name="arquivo" style="visibility:hidden; display:none;">
                    </div>
                    <button type="submit" class="btn-contato">ENVIAR</button>
                </div>
            </form>
            @if($errors->any())
            <div class="flash flash-erro">
                @foreach($errors->all() as $error)
                {!! $error !!}<br>
                @endforeach
            </div>
            @endif

            @if(session('enviado'))
            <div class="flash flash-sucesso">
                <p>Mensagem enviada com sucesso!</p>
            </div>
            @endif
        </div>
    </div>

    <div class="contato-mapa">
        {!! $contato->google_maps !!}
    </div>

    @include('frontend.filtro')

</section>

@endsection