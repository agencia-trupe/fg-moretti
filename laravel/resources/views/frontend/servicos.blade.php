@extends('frontend.common.template')

@section('content')

<section class="servicos">
    <div class="centralizado">

        <div class="submenu">
            <h4 class="titulo-menu">SERVIÇOS</h4>
            <div class="itens-submenu">
                @foreach($grupos as $grupo)
                <div class="link-submenu" id="grupo{{$grupo->id}}" grupo_id="{{$grupo->id}}">
                    <div class="nome-link">
                        <p class="nome"><span>•</span>{{ $grupo->titulo }}</p>
                        <img src="{{ asset('assets/img/layout/setinha-dupla-vinho.svg') }}" alt="" class="img-seta-link">
                        <img src="{{ asset('assets/img/layout/seta-dupla-abaixo-vinho.svg') }}" alt="" class="img-seta-active">
                    </div>
                    <div class="sublinks-servicos" style="display: none;"></div>
                </div>
                @endforeach
            </div>
        </div>

        @if(Tools::routeIs('servicos.show'))
        <div class="conteudo-submenu" id="conteudoSubmenuServicos">
            <h4 class="titulo-conteudo">{{ $servicoShow->titulo }}</h4>
            <div class="texto-conteudo">{!! $servicoShow->texto !!}</div>
            @if($areas)
            <div class="areas-aplicacoes">
                @foreach($areas as $area)
                <div class="area" id="divArea{{$area->id}}">
                    <a href="{{ route('areas-de-negocio', ['area' => $area->slug]) }}" class="link-area" id="area{{$area->id}}">{{ $area->titulo }}</a>
                    @if($aplicacoes)
                    @foreach($aplicacoes as $aplicacao)
                    @if($aplicacao->area_id == $area->id)
                    <a href="{{ route('aplicacoes', ['aplicacao' => $aplicacao->slug]) }}" class="link-aplicacao" id="aplicacao{{$aplicacao->id}}">» {{ $aplicacao->titulo }}</a>
                    @endif
                    @endforeach
                    @endif
                </div>
                @endforeach
            </div>
            @endif
            @if($imagens)
            <div class="servico-imagens">
                @foreach($imagens as $imagem)
                <img src="{{ asset('assets/img/servicos/'.$imagem->imagem) }}" alt="" class="img-servico">
                @endforeach
            </div>
            @endif
        </div>
        @else
        <div class="conteudo-submenu" id="conteudoSubmenuServicos">
            <h4 class="titulo-conteudo">SOBRE NOSSOS SERVIÇOS</h4>
            <div class="texto-conteudo">{!! $textoServicos->texto !!}</div>
        </div>
        @endif

    </div>

    @include('frontend.filtro')

</section>

@endsection