@extends('frontend.common.template')

@section('content')

<section class="politica-de-privacidade">
    <div class="centralizado">
        <h2 class="titulo">POLÍTICA DE PRIVACIDADE</h2>
        <div class="texto">{!! $politica->texto !!}</div>
    </div>

    @include('frontend.filtro')

</section>

@endsection