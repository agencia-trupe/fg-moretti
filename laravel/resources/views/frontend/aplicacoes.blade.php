@extends('frontend.common.template')

@section('content')

<section class="aplicacoes">
    <div class="centralizado">
        <div class="submenu">
            <h4 class="titulo-menu">APLICAÇÕES</h4>
            <div class="itens-submenu">
                @foreach($aplicacoes as $aplicacao)
                <a href="{{ route('getDadosAplicacoes', ['aplicacao' => $aplicacao->slug]) }}" class="link-submenu" id="aplicacao{{ $aplicacao->id }}">
                    <div class="nome-link">
                        <span>•</span>{{ $aplicacao->titulo }}
                    </div>
                    <span class="link-hover">»</span>
                </a>
                @endforeach
            </div>
        </div>

        <div class="conteudo-submenu" id="conteudoSubmenuAplicacoes">
            <div class="img-aplicacao">
                <img src="{{ asset('assets/img/aplicacoes/'.$aplicacaoInicial->imagem) }}" alt="">
            </div>
            <div class="textos-aplicacao">
                <h3 class="titulo">{{ $aplicacaoInicial->titulo }}</h3>
                <div class="texto">{!! $aplicacaoInicial->texto !!}</div>
            </div>
            <div class="areas" id="aplicacoesAreas">
                @foreach($aplicacoesAreas as $area)
                <div class="area">
                    <h2 class="titulo-area" id="tituloArea{{$area->area_id}}">{{ $area->area_titulo }}
                        <img src="{{ asset('assets/img/layout/setinha-dupla.svg') }}" alt="" class="img-seta-link">
                        <img src="{{ asset('assets/img/layout/seta-dupla-abaixo.svg') }}" alt="" class="img-seta-active">
                    </h2>
                    @if($area->texto_area)
                    <div class="texto-area">{!! $area->texto_area !!}</div>
                    @endif
                    <div class="servicos-area" id="servicosArea{{$area->area_id}}" style="display: none;">
                        @foreach($aplicacoesServicos as $servico)
                        @if($servico->area_id == $area->area_id)
                        <a href="{{ route('servicos.show', $servico->servico_id) }}" class="servico" id="grupo{{$servico->grupo_id}}" servico="{{$servico->servico_id}}">
                            <p class="nome"><span>•</span>{{ $servico->servico_titulo }}</p>
                            <img src="{{ asset('assets/img/layout/setinha-dupla-vinho.svg') }}" alt="" class="img-seta">
                        </a>
                        @endif
                        @endforeach
                    </div>
                </div>
                @endforeach
            </div>
            <!-- troca de submenus via AJAX -->
        </div>
    </div>

    @include('frontend.filtro')

</section>

@endsection