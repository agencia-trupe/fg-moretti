@extends('frontend.common.template')

@section('content')

<section class="home">

    <div class="banners">
        @foreach($banners as $banner)
        <div class="banner" style="background-image: url({{ asset('assets/img/banners-header/'.$banner->imagem) }})">
            <span class="overlay-banner">{{ $banner->titulo }}</span>
        </div>
        @endforeach
    </div>

    <div class="areas">
        @foreach($areas as $area)
        <a href="{{ route('areas-de-negocio', ['area' => $area->slug]) }}" class="area" id="area{{$area->id}}">
            {{ $area->titulo }}
        </a>
        @endforeach
    </div>

    <div class="slides">
        @foreach($slides as $slide)
        <a href="{{ $slide->link }}" class="slide" style="background-image: url({{ asset('assets/img/slides-home/'.$slide->imagem) }})">
            <div class="slide-gradiente"></div>
            <span class="overlay-slide"><img src="{{ asset('assets/img/layout/setinha-dupla.svg') }}" alt="" class="img-seta">{{ $slide->frase }}</span>
        </a>
        @endforeach
        <div class=cycle-pager></div>
    </div>

    <div class="faixa-texto" id="faixaTexto">
        <div class="centralizado">
            <div class="titulo">{{ $texto->titulo }}</div>
            <div class="texto">{!! $texto->texto !!}</div>
            <a href="{{ route('empresa', ['submenu' => 'historia']) }}" class="saiba-mais">SAIBA MAIS »</a>
        </div>
    </div>

    <div class="obras-destaques">
        <h1 class="titulo-destaque">OBRAS EM DESTAQUE</h1>
        <div class="obras">
            @foreach($destaques as $destaque)
            <div class="obra">
                <a href="{{ route('galeria-de-imagens.show', $destaque->obra_slug) }}" class="obra">
                    <img src="{{ asset('assets/img/obras/'.$destaque->obra_capa) }}" class="img-obra" alt="{{ $destaque->obra_titulo }}">
                    <p class="titulo-obra">{{ $destaque->obra_titulo }}</p>
                    <hr class="linha-destaque">
                </a>
                @php
                $areas = App\Models\ObraArea::where('obra_id', $destaque->obra_id)->join('areas_de_negocio', 'areas_de_negocio.id', '=', 'obras_areas.area_id')->select('areas_de_negocio.id as area_id', 'areas_de_negocio.titulo as titulo', 'areas_de_negocio.slug as area_slug')->orderBy('area_id', 'ASC')->get();
                @endphp

                @foreach($areas as $area)
                @php
                switch($area->area_id) {
                case 1:
                $titulo = "Projeto e Consultoria";
                break;
                case 2:
                $titulo = "Instrumentação";
                break;
                case 3:
                $titulo = "Investigação Geotécnica";
                break;
                case 4:
                $titulo = "Obras Geotécnicas";
                break;
                }
                @endphp
                <p class="area-destaque">» {{ $titulo }}</p>
                @endforeach
            </div>
            @endforeach
        </div>
    </div>

    @include('frontend.filtro')

</section>

@endsection