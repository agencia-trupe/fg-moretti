<div class="itens-menu">
    <a href="{{ route('empresa', ['submenu' => 'historia']) }}" @if(Tools::routeIs('empresa*')) class="link-nav active" @endif class="link-nav"><span>»</span>empresa</a>
    <a href="{{ route('areas-de-negocio', ['area' => $area1->slug]) }}" @if(Tools::routeIs('areas-de-negocio*')) class="link-nav active" @endif class="link-nav" id="area{{$area1->id}}"><span>»</span>áreas de negócio</a>
    <a href="{{ route('aplicacoes', ['aplicacao' => $aplicacao1->slug]) }}" @if(Tools::routeIs('aplicacoes*')) class="link-nav active" @endif class="link-nav" id="aplicacao{{$aplicacao1->id}}"><span>»</span>aplicações</a>
    <a href="{{ route('servicos') }}" @if(Tools::routeIs('servicos*')) class="link-nav active" @endif class="link-nav" id="linkNavServicos"><span>»</span>serviços</a>
    <a href="{{ route('galeria-de-imagens.index') }}" @if(Tools::routeIs('galeria-de-imagens*')) class="link-nav active" @endif class="link-nav"><span>»</span>galeria de imagens</a>
    <a href="{{ route('artigos-tecnicos') }}" @if(Tools::routeIs('artigos-tecnicos*')) class="link-nav active" @endif class="link-nav"><span>»</span>artigos técnicos</a>
    <a href="{{ route('clientes') }}" @if(Tools::routeIs('clientes*')) class="link-nav active" @endif class="link-nav"><span>»</span>clientes</a>
    <a href="{{ route('contato') }}" @if(Tools::routeIs('contato*')) class="link-nav active" @endif class="link-nav"><span>»</span>contato</a>
</div>