<header>

    <div class="header-topo">
        <div class="centralizado">
            @php $whatsapp = "+55".str_replace(" ", "", $contato->celular) @endphp
            <a href="https://api.whatsapp.com/send?phone={{ $whatsapp }}" class="link-celular"><img src="{{ asset('assets/img/layout/ico-whatsapp.svg') }}" alt="" class="img-celular">{{ $contato->celular }}</a>
            <div class="redes-sociais">
                <a href="{{ $contato->facebook }}" target="_blank" class="facebook"><img src="{{ asset('assets/img/layout/ico-facebook.svg') }}" alt=""></a>
                <a href="{{ $contato->linkedin }}" target="_blank" class="linkedin"><img src="{{ asset('assets/img/layout/ico-linkedin.svg') }}" alt=""></a>
                <a href="{{ $contato->instagram }}" target="_blank" class="instagram"><img src="{{ asset('assets/img/layout/ico-instagram.svg') }}" alt=""></a>
            </div>
            <a href="{{ route('area-do-cliente.login') }}" @if(Tools::routeIs('area-do-cliente*')) class="link-area-cliente active" @endif class="link-area-cliente"><img src="{{ asset('assets/img/layout/ico-cadeado.svg') }}" alt="" class="img-cadeado">ÁREA DO CLIENTE</a>
            <a href="#" class="link-busca">BUSCA <img src="{{ asset('assets/img/layout/ico-busca.svg') }}" alt="" class="img-lupa"></a>
        </div>
        <div class="busca-header" style="display: none">
            <form action="{{ route('busca') }}" method="POST">
                {!! csrf_field() !!}
                <input type="text" name="busca" class="input-busca" placeholder="digite o que procura...">
                <button type="submit" class="btn-buscar">BUSCAR<img src="{{ asset('assets/img/layout/ico-busca.svg') }}" alt="" class="img-btn-buscar"></button>
            </form>
        </div>
    </div>

    @if(Tools::routeIs('home'))
    <div class="header-home">
        <a href="{{ route('home') }}" class="logo-home" title="{{ $config->title }}"><img src="{{ asset('assets/img/layout/marca-fg-moretti-engenharia.svg') }}" alt="" class="img-logo-home"></a>
        <nav>
            @include('frontend.common.nav')
        </nav>
    </div>
    @else
    <div class="header-paginas">
        <div class="left">
            <a href="{{ route('home') }}" class="logo" title="{{ $config->title }}"><img src="{{ asset('assets/img/layout/marca-fg-moretti-engenharia.svg') }}" alt="" class="img-logo"></a>
            <nav>
                @include('frontend.common.nav')
            </nav>
        </div>
        <div class="right">
            @if(Tools::routeIs('busca'))
            <div class="sem-img-menu"></div>
            <span class="overlay-titulo">BUSCA AVANÇADA</span>
            @elseif(Tools::routeIs('filtro'))
            <div class="sem-img-menu"></div>
            <span class="overlay-titulo">FILTRO</span>
            @elseif(Tools::routeIs('termos-de-uso'))
            <div class="sem-img-menu-cinza"></div>
            <span class="overlay-titulo termos">TERMOS DE USO DO SITE</span>
            @elseif(Tools::routeIs('politica-de-privacidade'))
            <div class="sem-img-menu-cinza"></div>
            <span class="overlay-titulo termos">POLÍTICA DE PRIVACIDADE</span>
            @elseif(Tools::routeIs(['area-do-cliente*', 'admin.*']))
            <div class="sem-img-menu"></div>
            <span class="overlay-titulo">
                ÁREA DO CLIENTE
                @if(auth('admin')->check())
                <p class="usuario">Olá {{ auth('admin')->user()->name }} <span class="tipo-usuario">[ {{ auth('admin')->user()->getTipoAttribute() }} ]</span></p>
                <a href="{{ route('admin.logout') }}" class="link-logout">[ SAIR ]</a>
                @endif

                @if(auth('clientes')->check())
                @php $empresa = App\Models\AreaDoCliente\Empresa::where('id', auth('clientes')->user()->empresa_id)->first()->nome; @endphp
                <p class="usuario">Olá {{ auth('clientes')->user()->nome }} <span class="tipo-usuario">[ {{ auth('clientes')->user()->gerente == 1 ? 'Gerente' : 'Usuário' }} | {{ $empresa }} ]</span></p>
                <a href="{{ route('area-do-cliente.logout') }}" class="link-logout">[ SAIR ]</a>
                @endif
            </span>
            @else
            @foreach($menu as $dado)
            <img src="{{ asset('assets/img/capas-menus/'.$dado->imagem) }}" class="img-menu" alt="{{ $dado->titulo }}">
            <div class="overlay-escuro"></div>
            <span class="overlay-titulo">{{ $dado->titulo }}</span>
            @endforeach
            @endif
        </div>
    </div>
    @endif

</header>