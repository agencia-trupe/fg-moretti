<footer>
    <div class="footer">
        <div class="dados-empresa">
            <a href="{{ route('home') }}" class="logo-footer"><img src="{{ asset('assets/img/layout/marca-fg-moretti-engenharia.svg') }}" alt="" class="img-logo-footer"></a>
            @php $whatsapp = "+55".str_replace(" ", "", $contato->celular) @endphp
            <a href="https://api.whatsapp.com/send?phone={{ $whatsapp }}" class="link-celular" target="_blank"><img src="{{ asset('assets/img/layout/ico-whatsapp.svg') }}" alt="" class="img-celular">{{ $contato->celular }}</a>
            @php $telefone = "+55".str_replace(" ", "", $contato->telefone); @endphp
            <a href="tel:{{ $telefone }}" class="link-telefone"><img src="{{ asset('assets/img/layout/ico-telefone.svg') }}" alt="" class="img-telefone">{{ $contato->telefone }}</a>
            <div class="endereco">{{ $contato->endereco }}</div>
            <div class="redes-sociais">
                <a href="{{ $contato->facebook }}" target="_blank" class="facebook"><img src="{{ asset('assets/img/layout/ico-facebook.svg') }}" alt=""></a>
                <a href="{{ $contato->linkedin }}" target="_blank" class="linkedin"><img src="{{ asset('assets/img/layout/ico-linkedin.svg') }}" alt=""></a>
                <a href="{{ $contato->instagram }}" target="_blank" class="instagram"><img src="{{ asset('assets/img/layout/ico-instagram.svg') }}" alt=""></a>
            </div>
        </div>

        <div class="areas-aplicacoes">
            <div class="areas-de-negocio">
                <h4 class="titulo-areas"><span>-</span>ÁREAS DE NEGÓCIO</h4>
                @foreach($areas as $area)
                <a href="{{ route('areas-de-negocio', ['area' => $area->slug]) }}" class="area" id="area{{$area->id}}">» {{ $area->titulo }}</a>
                @endforeach
            </div>
            <div class="aplicacoes">
                <h4 class="titulo-aplicacoes"><span>-</span>APLICAÇÕES</h4>
                @foreach($aplicacoes as $aplicacao)
                <a href="{{ route('aplicacoes', ['aplicacao' => $aplicacao->slug]) }}" class="aplicacao" id="aplicacao{{$aplicacao->id}}">» {{ $aplicacao->titulo }}</a>
                @endforeach
            </div>
        </div>

        <div class="servicos">
            <h4 class="titulo-servicos"><span>-</span>SERVIÇOS</h4>
            @foreach($grupos as $grupo)
            <a href="#" class="grupo" id="grupo{{$grupo->id}}">» {{ $grupo->titulo }}</a>
            @endforeach
        </div>

        <div class="nav-footer">
            <div class="menus-principais">
                <a href="{{ route('home') }}" @if(Tools::routeIs('home*')) class="link-footer active" @endif class="link-footer"><span>-</span>HOME</a>
                <a href="{{ route('empresa', ['submenu' => 'historia']) }}" @if(Tools::routeIs('empresa*')) class="link-footer active" @endif class="link-footer"><span>-</span>EMPRESA</a>
                <a href="{{ route('galeria-de-imagens.index') }}" @if(Tools::routeIs('galeria-de-imagens*')) class="link-footer active" @endif class="link-footer"><span>-</span>GALERIA DE IMAGENS</a>
                <a href="{{ route('artigos-tecnicos') }}" @if(Tools::routeIs('artigos-tecnicos*')) class="link-footer active" @endif class="link-footer"><span>-</span>ARTIGOS TÉCNICOS</a>
                <a href="{{ route('clientes') }}" @if(Tools::routeIs('clientes*')) class="link-footer active" @endif class="link-footer"><span>-</span>CLIENTES</a>
                <a href="{{ route('contato') }}" @if(Tools::routeIs('contato*')) class="link-footer active" @endif class="link-footer"><span>-</span>CONTATO</a>
            </div>
            <div class="menus-secundarios">
                <a href="{{ route('area-do-cliente.login') }}" @if(Tools::routeIs('area-do-cliente*')) class="link-footer active" @endif class="link-footer"><span>-</span><img src="{{ asset('assets/img/layout/ico-cadeado-cinza.svg') }}" alt="" class="img-cadeado">ÁREA DO CLIENTE</a>
                <a href="{{ route('termos-de-uso') }}" @if(Tools::routeIs('termos-de-uso*')) class="link-footer active" @endif class="link-footer"><span>-</span>Termos de uso do site</a>
                <a href="{{ route('politica-de-privacidade') }}" @if(Tools::routeIs('politica-de-privacidade*')) class="link-footer active" @endif class="link-footer"><span>-</span>Política de privacidade</a>
            </div>
        </div>
    </div>

    <hr class="linha-footer">

    <div class="direitos">© {{ date('Y') }} {{ $config->title }} {{ $config->{trans('banco.description')} }} &middot; • Todos os direitos reservados | Criação de sites: <a href="http://www.trupe.net" target="_blank" class="link-trupe">Trupe Agência Criativa</a></div>
</footer>