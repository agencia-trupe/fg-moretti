@extends('frontend.common.template')

@section('content')

<section class="informacoes-obra">
    <div class="centralizado">
        <h2 class="titulo-aplicacao">{{ $obraDados->aplicacao_titulo }}</h2>
        <h4 class="titulo-obra">{{ $obraDados->titulo }}</h4>
        <div class="areas-servicos">
            <div class="areas-obra">
                @foreach($areasObra as $area)
                <a href="{{ route('areas-de-negocio', $area->area_slug) }}" class="link-area" id="area{{$area->area_id}}">{{ $area->area_titulo }}</a>
                @endforeach
            </div>
            <div class="servicos-obra">
                <p class="titulo-servicos">SERVIÇOS REALIZADOS:</p>
                @foreach($servicosObra as $servico)
                <a href="{{ route('servicos.show', $servico->servico_id) }}" class="link-servico" id="grupo{{$servico->grupo_id}}" servico="{{$servico->servico_id}}">» {{ $servico->servico_titulo }}</a>
                @endforeach
            </div>
        </div>
        <div class="texto-obra">{!! $obraDados->texto !!}</div>
        <div class="imagens-obra">
            @foreach($imagens as $imagem)
            <img src="{{ asset('assets/img/obras/imagens/'.$imagem->imagem) }}" alt="" class="img-obra">
            @endforeach
        </div>
        <a href="{{ route('galeria-de-imagens.index') }}" class="btn-voltar-obras">« VOLTAR</a>
    </div>

    @include('frontend.filtro')

</section>

@endsection