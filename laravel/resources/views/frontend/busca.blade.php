@extends('frontend.common.template')

@section('content')

<section class="resultados-busca">
    <div class="centralizado">
        <h2 class="titulo">RESULTADO DA BUSCA</h2>

        <div class="texto-input" style="visibility: hidden; display: none;">{{$termo}}</div>
        @if(count($resultados))
        <div class="resultados">
            @foreach($resultados as $secoes)
            @foreach($secoes as $resultado)
            <div class="resultado">
                <div class="secao"><span>//</span>{{ $resultado['secao'] }}</div>
                
                @if ($resultado['secao'] == "aplicações")
                <a href="{{ $resultado['link'] }}" title="{{ $resultado['secao'] . ' - ' . $resultado['titulo'] }}" class="link-resultado aplicacao" id="aplicacao{{ $resultado['id'] }}">{!! $resultado['titulo'] !!} • {!! Tools::str_words(strip_tags($resultado['texto'], '<span>'), 30) !!}
                </a>
                
                @elseif ($resultado['secao'] == "áreas de negócio")
                <a href="{{ $resultado['link'] }}" title="{{ $resultado['secao'] . ' - ' . $resultado['titulo'] }}" class="link-resultado area" id="area{{ $resultado['id'] }}">{!! $resultado['titulo'] !!} • {!! Tools::str_words(strip_tags($resultado['texto'], '<span>'), 30) !!}
                </a>
                
                @elseif ($resultado['secao'] == "servicos")
                <a href="{{ $resultado['link'] }}" title="{{ $resultado['secao'] . ' - ' . $resultado['titulo'] }}" class="link-resultado servico" id="grupo{{ $resultado['grupo'] }}" servico="{{ $resultado['id'] }}">{!! $resultado['titulo'] !!} • {!! Tools::str_words(strip_tags($resultado['texto'], '<span>'), 30) !!}
                </a>
                
                @else 
                <a href="{{ $resultado['link'] }}" title="{{ $resultado['secao'] . ' - ' . $resultado['titulo'] }}" class="link-resultado">{!! $resultado['titulo'] !!} • {!! Tools::str_words(strip_tags($resultado['texto'], '<span>'), 30) !!}
                </a>
                @endif
            </div>
            @endforeach
            @endforeach
        </div>

        @else
        <div class="nenhum-resultado">
            Nenhum resultado encontrado para a busca: <strong>{{$termo}}</strong>.
        </div>
        @endif

    </div>

    @include('frontend.filtro')

</section>

@endsection