@extends('frontend.common.template')

@section('content')

<section class="galeria-de-imagens">

    <div class="filtro-galeria">
        <div class="centralizado">
            <div class="filtro">
                <span class="label">FILTRAR POR:</span>
                <select name="area_id" class="areas" id="filtroGaleriaAreas">
                    <option value="" selected>Área de Negócio</option>
                    @foreach($areas as $area)
                    <option value="{{ $area->id }}">{{ $area->titulo }}</option>
                    @endforeach
                </select>
                <select name="aplicacao_id" class="aplicacoes" id="filtroGaleriaAplicacoes">
                    <option value="" selected>Tipo de Obra • Aplicação</option>
                    @foreach($aplicacoes as $aplicacao)
                    <option value="{{ $aplicacao->id }}">{{ $aplicacao->titulo }}</option>
                    @endforeach
                </select>
                <select name="servico_id" class="servicos" id="filtroGaleriaServicos">
                    <option value="" selected>Serviço (digite ou selecione)</option>
                    @foreach($servicos as $servico)
                    <option value="{{ $servico->id }}">{{ $servico->titulo }}</option>
                    @endforeach
                </select>
                <a href="#" class="btn-filtro" id="btnFiltroGaleria">FILTRAR <img src="{{ asset('assets/img/layout/ico-busca-vinho.svg') }}" alt="" class="img-lupa"></a>
            </div>
        </div>
    </div>

    <div class="galeria-obras">
        <div class="centralizado" id="todasObrasGaleria">
            @foreach($obras as $obra)
            <a href="{{ route('galeria-de-imagens.show', $obra->slug) }}" class="galeria-obra" id="obra{{$obra->id}}">
                <div class="capa-obra">
                    <img src="{{ asset('assets/img/obras/'.$obra->capa) }}" alt="" class="img-obra">
                    <span class="overlay-aplicacao">{{ $obra->aplicacao_titulo }}</span>
                </div>
                <p class="titulo-obra">{{ $obra->titulo }}</p>
                <div class="areas-obra" id="areasObra{{$obra->id}}">
                    @foreach($areasObra as $area)
                    @if($area->obra_id == $obra->id)
                    <div class="obra-area" id="area{{ $area->area_id }}">{{ $area->area_titulo }}</div>
                    @endif
                    @endforeach
                </div>
            </a>
            @endforeach
        </div>
        <button class="btn-mais-obras">VER MAIS +</button>
    </div>

    @include('frontend.filtro')

</section>

@endsection