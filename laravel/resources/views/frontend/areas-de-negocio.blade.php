@extends('frontend.common.template')

@section('content')

<section class="areas-de-negocio">
    <div class="areas">
        @foreach($areas as $area)
        <a href="{{ route('getDadosAreasDeNegocio', ['area' => $area->slug]) }}" class="link-area" id="area{{ $area->id }}">
            <div class="conteudo-link">
                <h2 class="titulo">{{ $area->titulo }}</h2>
                <img src="{{ asset('assets/img/layout/seta-dupla-abaixo.svg') }}" alt="" class="img-areas">
            </div>
        </a>
        @endforeach
    </div>

    <div class="conteudo" id="conteudoAreaDeNegocio">
        <div class="img-titulo" id="area{{$areaInicial->id}}">
            <img src="{{ asset('assets/img/areas-de-negocio/'.$areaInicial->imagem) }}" class="img-area">
            <h4 class="titulo">{{ $areaInicial->titulo }}</h4>
        </div>
        <div class="texto-pt1">
            {!! $areaInicial->texto_inicial !!}
        </div>
        <div class="aplicacoes">
            <h6 class="titulo" id="aplicacaoArea{{$areaInicial->id}}">APLICAÇÕES</h6>
            @foreach($aplicacoes as $aplicacao)
            <a href="{{ route('aplicacoes', ['aplicacao' => $aplicacao->slug]) }}" class="aplicacao" id="aplicacao{{$aplicacao->id}}"><span>»</span>{{ $aplicacao->titulo }}</a>
            @endforeach
        </div>
        <div class="texto-pt2">
            {!! $areaInicial->texto_servicos !!}
        </div>
        <div class="servicos">
            <h6 class="titulo" id="servicoArea{{$areaInicial->id}}">SERVIÇOS</h6>
            @foreach($servicos as $servico)
            <a href="{{ route('servicos.show', $servico->id) }}" class="servico" id="grupo{{$servico->grupo_id}}" servico="{{$servico->id}}">
                <p class="nome"><span>•</span>{{ $servico->titulo }}</p>
                <img src="{{ asset('assets/img/layout/setinha-dupla-vinho.svg') }}" alt="" class="img-seta">
            </a>
            @endforeach
        </div>
    </div>

    @include('frontend.filtro')

</section>

@endsection