<div class="filtro-geral">
    <div class="centralizado">
        <h2 class="titulo-filtro">ENCONTRE NOSSOS SERVIÇOS</h2>
        <div class="filtro">
            <span class="label">FILTRAR POR:</span>
            <select name="area_id" class="areas-filtro" id="filtroGeralAreas">
                <option value="" selected>Área de Negócio</option>
                @foreach($areas as $area)
                <option value="{{ $area->id }}">{{ $area->titulo }}</option>
                @endforeach
            </select>
            <select name="aplicacao_id" class="aplicacoes-filtro" id="filtroGeralAplicacoes">
                <option value="" selected>Tipo de Obra • Aplicação</option>
                @foreach($aplicacoes as $aplicacao)
                <option value="{{ $aplicacao->id }}">{{ $aplicacao->titulo }}</option>
                @endforeach
            </select>
            <select name="servico_id" class="servicos-filtro" id="filtroGeralServicos">
                <option value="" selected>Serviço (digite ou selecione)</option>
                @foreach($servicos as $servico)
                <option value="{{ $servico->id }}">{{ $servico->titulo }}</option>
                @endforeach
            </select>
            <a href="#" class="btn-filtro" id="btnFiltroGeral">FILTRAR <img src="{{ asset('assets/img/layout/ico-busca-vinho.svg') }}" alt="" class="img-lupa"></a>
        </div>
    </div>
</div>