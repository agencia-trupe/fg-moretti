@extends('frontend.common.template')

@section('content')

<section class="artigos-tecnicos">

    <div class="centralizado">
        <h2 class="titulo">ARTIGOS TÉCNICOS</h2>

        <div class="artigos">
            @foreach($artigos as $artigo)
            <a href="{{ route('artigos-tecnicos.pdf', $artigo->slug) }}" target="_blank" class="artigo" id="artigo{{$artigo->id}}">
                <div class="img-pdf">
                    <img src="{{ asset('assets/img/layout/ico-pdf.svg') }}" alt="">
                </div>
                <p class="titulo-artigo">{{ $artigo->titulo }}</p>
            </a>
            @endforeach
        </div>
    </div>

    @include('frontend.filtro')

</section>

@endsection