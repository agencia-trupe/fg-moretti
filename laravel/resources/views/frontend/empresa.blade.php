@extends('frontend.common.template')

@section('content')

<section class="empresa">
    <div class="centralizado">
        <div class="submenu">
            <h4 class="titulo-menu">EMPRESA</h4>
            <div class="itens-submenu">
                <a href="{{ route('getDadosEmpresa', ['submenu' => 'historia']) }}" class="link-submenu active">
                    <div class="nome-link">
                        <span>•</span>história
                    </div>
                    <span class="link-hover">»</span>
                </a>
                <a href="{{ route('getDadosEmpresa', ['submenu' => 'nossa-otica']) }}" class="link-submenu">
                    <div class="nome-link">
                        <span>•</span>nossa ótica
                    </div>
                    <span class="link-hover">»</span>
                </a>
                <a href="{{ route('getDadosEmpresa', ['submenu' => 'corpo-tecnico']) }}" class="link-submenu">
                    <div class="nome-link">
                        <span>•</span>corpo técnico
                    </div>
                    <span class="link-hover">»</span>
                </a>
            </div>
        </div>

        <div class="conteudo-submenu" id="conteudoSubmenuEmpresa">
            <div class="img-empresa">
                <img src="{{ asset('assets/img/layout/ico-historia.svg') }}" alt="" class="img-empresa">
            </div>
            <div class="textos-empresa">
                <h3 class="titulo">HISTÓRIA</h3>
                <div class="texto">{!! $historia->texto !!}</div>
            </div>
            <!-- troca de submenus via AJAX -->
        </div>
    </div>

    @include('frontend.filtro')

</section>

@endsection