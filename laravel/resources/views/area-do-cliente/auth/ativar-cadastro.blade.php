@extends('frontend.common.template')

@section('content')

<section class="ac-clientes">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - ATUALIZAR E ATIVAR CADASTRO</h2>

        <form action="{{ route('area-do-cliente.usuarios.cadastro.post', [$email, $token]) }}" class="form-ativar-cadastro" method="POST">
            {!! csrf_field() !!}

            <input type="hidden" name="token" value="{{ $token }}">
            <input type="hidden" name="email" value="{{ $email }}" required>

            <label for="nome">Nome:</label>
            <input type="text" name="nome" value="{{ old('nome') }}">
            <label for="telefone">Telefone/Celular:</label>
            <input type="text" name="telefone" class="telefone-usuario" value="{{ old('telefone') }}">
            <label for="email">E-mail:</label>
            <input type="email" name="email" value="{{ $email }}" readonly>

            <label for="password">Senha atual:</label>
            <input type="password" name="password" required>

            <label for="password">Nova senha:</label>
            <input type="password" name="nova_senha" required>
            <label for="password">Confirme a nova senha:</label>
            <input type="password" name="nova_senha_confirmation" required>

            <button type="submit">SALVAR</button>

            @include('area-do-cliente._flash')
        </form>

    </div>
</section>

@endsection