@extends('frontend.common.template')

@section('content')

<div class="ac-login">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - REDEFINIR SENHA</h2>

        <form action="{{ route('area-do-cliente.password.reset') }}" class="form-redefinir" method="POST">
            {{ csrf_field() }}

            <div class="inputs-form">
                <input type="hidden" name="token" value="{{ $token }}">
                <input type="hidden" name="email" value="{{ $email }}">

                <label for="password">Nova senha:</label>
                <input type="password" name="password" required>
                <label for="password">Confirme a nova senha:</label>
                <input type="password" name="password_confirmation" required>

                <input type="submit" value="REDEFINIR SENHA" class="btn btn-primary">
            </div>
        </form>

        @include('area-do-cliente._flash')
    </div>
</div>
@endsection