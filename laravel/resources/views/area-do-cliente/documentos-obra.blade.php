@extends('frontend.common.template')

@section('content')

<section class="ac-clientes">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - DOCUMENTOS DA OBRA</h2>

        @include('area-do-cliente.clientes-btns')

        @include('area-do-cliente.busca')

        @include('area-do-cliente._flash')

        <div class="lista-documentos-versoes">
            <h4 class="subtitulo">{{ $obra->titulo }} ∙ {{ $obra->localizacao }}</h4>

            <a href="{{ route('area-do-cliente.clientes.obra.comunicacao', $obra->slug) }}" class="link-comunicacao">COMUNICAÇÃO COM A FG MORETTI</a>

            @foreach($documentos as $documento)
            <div class="documento-versoes">
                <div class="documento">
                    <div class="img-pdf">
                        <img src="{{ asset('assets/img/layout/areacliente-icone-PDF.svg') }}" alt="" class="icone-pdf">
                    </div>
                    <div class="dados-documento">
                        <p class="titulo-doc">{{ $documento->titulo }}</p>
                        <div class="texto-doc">{!! $documento->descricao !!}</div>
                    </div>
                </div>
                @php
                $total = 0;
                $versoesDoc = [];
                foreach($versoes as $v) {
                    if($v->documento_id == $documento->id) {
                        $total += 1;
                        $versoesDoc[] = $v;
                    }
                }
                @endphp
                @foreach($versoesDoc as $v)
                <div class="versoes">
                    <p class="total-documentos">TOTAL DE DOCUMENTOS: {{ $total }}</p>
                    <hr class="linha-versoes">
                    <p class="ultimo-doc">ÚLTIMO DOCUMENTO ADICIONADO: {{ strftime("%d/%m/%Y ∙ %H:%M", strtotime($v->updated_at)) }}</p>
                    <p class="doc">{{ $v->titulo }} | {{ $v->versao }} | {{ strftime("%d/%m/%Y ∙ %H:%M", strtotime($v->updated_at)) }}</p>
                </div>
                <a href="{{ route('area-do-cliente.clientes.documentos.download', $v->id) }}" class="link-download-versao">
                    DOWNLOAD DA ÚLTIMA VERSÃO ADICIONADA
                    <img src="{{ asset('assets/img/layout/areacliente-icone-download.svg') }}" alt="" class="icone-download">
                </a>
                <a href="{{ route('area-do-cliente.clientes.documentos.obra.versoes', [$obra->slug, $documento->id]) }}" class="link-documento-versoes">
                    <img src="{{ asset('assets/img/layout/areacliente-icone-mais.svg') }}" alt="" class="icone-mais">
                </a>
                @break
                @endforeach
            </div>
            @endforeach
        </div>

    </div>
</section>

@endsection