@extends('frontend.common.template')

@section('content')

<section class="ac-clientes">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - DOCUMENTOS DA OBRA</h2>

        @include('area-do-cliente.clientes-btns')

        @include('area-do-cliente._flash')

        <div class="lista-comunicacoes">
            <div class="header-comunicacoes">
                <a href="{{ route('area-do-cliente.clientes.documentos.obra', $obra->slug) }}" class="link-voltar">« VOLTAR</a>
                <h4 class="subtitulo"><span>COMUNICAÇÃO: </span>{{ $obra->titulo }} ∙ {{ $obra->localizacao }}</h4>
            </div>

            <form action="{{ route('area-do-cliente.clientes.obra.comunicacao.post', $obra->slug) }}" method="post" class="form-comunicacao">
                {!! csrf_field() !!}
                <input type="hidden" name="empresa_id" class="input-empresa" value="{{ $usuarioLogado->empresa_id }}">
                <input type="hidden" name="usuario_id" class="input-usuario" value="{{ $usuarioLogado->id }}">
                <input type="hidden" name="admin_id" class="input-admin" value="null">
                <textarea name="mensagem" id="mensagem" placeholder="[mensagem]" value="{{ old('mensagem') }}"></textarea>
                <button type="submit">ENVIAR</button>
            </form>

            <div class="comunicacoes">
                @foreach($comunicacoes as $comunicacao)
                @php
                foreach($empresas as $e) {
                    if($e->id == $comunicacao->empresa_id) {
                    $empresa = $e;
                    }
                    break;
                }
                @endphp
                @if($comunicacao->admin_id == null)
                <div class="comunicacao left">
                    <p class="empresa">{{ $empresa->nome }}</p>
                    <div class="texto-msg">{!! $comunicacao->mensagem !!}</div>
                    <p class="data-msg">ENVIADA EM {{ strftime("%d/%m/%Y ∙ %H:%M", strtotime($comunicacao->created_at)) }}</p>
                    @if($comunicacao->respondido == 0)
                    <a href="{{ route('area-do-cliente.clientes.obra.comunicacao.destroy', [$obra->slug, $comunicacao->id]) }}" class="link-excluir-comunicacao">
                        <img src="{{ asset('assets/img/layout/areacliente-icone-lixeira.svg') }}" alt="Excluir" class="img-excluir">
                    </a>
                    @endif
                </div>
                @endif
                @if($comunicacao->admin_id != null)
                <div class="comunicacao right">
                    <div class="texto-msg">{!! $comunicacao->mensagem !!}</div>
                    <p class="data-msg">RECEBIDA EM {{ strftime("%d/%m/%Y ∙ %H:%M", strtotime($comunicacao->created_at)) }}</p>
                </div>
                @endif
                @endforeach
            </div>

        </div>
    </div>
</section>

@endsection