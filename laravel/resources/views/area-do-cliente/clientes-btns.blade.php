<div class="btns-clientes">
    <a href="{{ route('area-do-cliente.clientes.identificacao') }}" @if(Tools::routeIs(['area-do-cliente.clientes.identificacao*'])) class="link-identificacao active" @endif class="link-identificacao">IDENTIFICAÇÃO</a>
    <a href="{{ route('area-do-cliente.clientes.documentos') }}" @if(Tools::routeIs(['area-do-cliente.clientes.documentos*'])) class="link-documentos active" @endif class="link-documentos">DOCUMENTOS</a>
</div>