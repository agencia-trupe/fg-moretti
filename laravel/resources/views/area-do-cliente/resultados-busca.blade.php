@extends('frontend.common.template')

@section('content')

<section class="ac-clientes">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - RESULTADOS DA BUSCA</h2>

        @include('area-do-cliente.clientes-btns')

        @include('area-do-cliente.busca')

        @include('area-do-cliente._flash')

        <div class="lista-resultados-busca">
            <h4 class="subtitulo">BUSCA: {{ $palavra }}</h4>

            @foreach($resultados as $resultado)
            <div class="resultado">
                <div class="documento">
                    <div class="img-pdf">
                        <img src="{{ asset('assets/img/layout/areacliente-icone-PDF.svg') }}" alt="" class="icone-pdf">
                    </div>
                    <p class="titulo-doc">{{ $resultado->doc_titulo }}</p>
                    <div class="texto-doc">{!! $resultado->descricao !!}</div>
                    <a href="{{ route('area-do-cliente.clientes.documentos.obra.versoes', [$resultado->obra_slug, $resultado->id]) }}" class="link-versoes">VER DOCUMENTOS</a>
                </div>
                <div class="versao">
                    <p class="data">{{ strftime("%d/%m/%Y ∙ %H:%M", strtotime($resultado->updated_at)) }}</p>
                    <p class="nome">{{ $resultado->versao_titulo }} | {{ $resultado->versao }}</p>
                    <a href="{{ route('area-do-cliente.clientes.documentos.download', $resultado->id) }}" class="link-download-versao">
                        FAZER DOWNLOAD<img src="{{ asset('assets/img/layout/areacliente-icone-download.svg') }}" alt="" class="icone-download">
                    </a>
                </div>
            </div>
            @endforeach
        </div>

    </div>
</section>

@endsection