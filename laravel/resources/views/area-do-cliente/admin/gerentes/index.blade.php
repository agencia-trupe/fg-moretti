@extends('frontend.common.template')

@section('content')

<section class="ac-admin">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - EMPRESA: {{ $empresa->nome }} - GERENTES</h2>

        @include('area-do-cliente.admin-btns')

        <div class="ac-gerentes">

            @include('area-do-cliente._flash')

            <a href="{{ route('area-do-cliente.gerentes.create', $empresa->slug) }}" class="link-adicionar">
                <img src="{{ asset('assets/img/layout/areacliente-icone-mais.svg') }}" alt="Adicionar" class="img-mais">
                ADICIONAR GERENTE
            </a>
            <div class="lista-resultados">
                @if(count($gerentes) > 0)
                @foreach($gerentes as $gerente)
                <div class="gerente">
                    <p class="nome-gerente">{{ $gerente->nome }}</p>
                    <p class="telefone">{{ $gerente->telefone }}</p>
                    @if($gerente->ativo == 1)
                    <p class="ativo">[ ATIVO ]</p>
                    @else
                    <p class="ativo">[ PENDENTE ]</p>
                    @endif
                    <div class="btns-gerenciamentos">
                        <a href="{{ route('area-do-cliente.gerentes.obras', [$empresa->slug, $gerente->slug]) }}" class="link-gerentes-obras">OBRAS</a>
                        <a href="{{ route('area-do-cliente.gerentes.usuarios', [$empresa->slug, $gerente->slug]) }}" class="link-gerentes-usuarios">USUÁRIOS</a>
                        <div class="btns-icones">
                            <a href="{{ route('area-do-cliente.gerentes.edit', [$empresa->slug, $gerente->slug]) }}" class="link-editar">
                                <img src="{{ asset('assets/img/layout/editar-branco.svg') }}" alt="Editar" class="img-editar">
                            </a>
                            <a href="{{ route('area-do-cliente.gerentes.destroy', [$empresa->slug, $gerente->slug]) }}" class="link-excluir" onclick="return confirm('Tem certeza que deseja excluir o gerente: {{ $gerente->nome }}?')">
                                <img src="{{ asset('assets/img/layout/areacliente-icone-lixeira-branco.svg') }}" alt="Excluir" class="img-excluir">
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <p class="nenhum-resultado">Nenhum gerente cadastrado.</p>
                @endif
            </div>
        </div>

    </div>
</section>

@endsection