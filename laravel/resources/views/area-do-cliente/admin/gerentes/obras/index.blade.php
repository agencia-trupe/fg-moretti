@extends('frontend.common.template')

@section('content')

<section class="ac-admin">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - GERENTE: {{ $gerente->nome }} - OBRAS</h2>

        @include('area-do-cliente.admin-btns')

        <div class="ac-gerentes">

            @include('area-do-cliente._flash')

            <div class="voltar-e-adicionar">
                <a href="{{ route('area-do-cliente.gerentes', $empresa->slug) }}" class="link-voltar">
                    <img src="{{ asset('assets/img/layout/setinha-dupla.svg') }}" alt="Voltar" class="img-voltar">
                    VOLTAR PARA GERENTES
                </a>
                <a href="{{ route('area-do-cliente.gerentes.obras.create', [$empresa->slug, $gerente->slug]) }}" class="link-adicionar">
                    <img src="{{ asset('assets/img/layout/areacliente-icone-mais.svg') }}" alt="Adicionar" class="img-mais">
                    ADICIONAR OBRA
                </a>
            </div>
            <div class="lista-resultados">
                @if(count($obrasGerente) > 0)
                @foreach($obrasGerente as $obra)
                <div class="gerente-obras">
                    <p class="titulo-obra-g">{{ $obra->titulo }}</p>
                    <p class="localizacao-g">[ {{ $obra->localizacao }} ]</p>
                    <p class="codigo-obra">JOB: {{ $obra->codigo }}</p>
                    <div class="btns-gerenciamentos">
                        <div class="btns-icones">
                            <a href="{{ route('area-do-cliente.gerentes.obras.destroy', [$empresa->slug, $gerente->slug, $obra->slug]) }}" class="link-excluir" onclick="return confirm('Tem certeza que deseja excluir a obra: {{ $obra->titulo }} da lista do gerente: {{ $gerente->nome }}?')">
                                <img src="{{ asset('assets/img/layout/areacliente-icone-lixeira-branco.svg') }}" alt="Excluir" class="img-excluir">
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <p class="nenhum-resultado">Nenhuma obra cadastrada para este gerente.</p>
                @endif
            </div>
        </div>

    </div>
</section>

@endsection