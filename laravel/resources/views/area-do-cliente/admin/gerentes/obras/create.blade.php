@extends('frontend.common.template')

@section('content')

<section class="ac-admin">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - GERENTE: {{ $gerente->nome }} - ADICIONAR OBRA</h2>

        @include('area-do-cliente.admin-btns')

        <div class="ac-gerentes">
            <a href="{{ route('area-do-cliente.gerentes.obras', [$empresa->slug, $gerente->slug]) }}" class="link-voltar">
                <img src="{{ asset('assets/img/layout/setinha-dupla.svg') }}" alt="Voltar" class="img-voltar">
                VOLTAR
            </a>
            <form action="{{ route('area-do-cliente.gerentes.obras.store', [$empresa->slug, $gerente->slug]) }}" class="form-gerente" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <label for="obras">Obras:</label>
                <select name="obra_id" id="obras" class="select-obras">
                    <option value="" selected>Selecione</option>
                    @foreach($obras as $obra)
                    <option value="{{ $obra->id }}">{{ $obra->titulo }}</option>
                    @endforeach
                </select>

                <button type="submit">ADICIONAR</button>

                @include('area-do-cliente._flash')
            </form>
        </div>

    </div>
</section>

@endsection