@extends('frontend.common.template')

@section('content')

<section class="ac-admin">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - EMPRESA: {{ $empresa->nome }} - GERENTE: {{ $gerente->nome }} - USUÁRIOS</h2>

        @include('area-do-cliente.admin-btns')

        <div class="ac-gerentes">

            @include('area-do-cliente._flash')

            <div class="lista-resultados">
                @if(count($usuarios) > 0)
                @foreach($usuarios as $usuario)
                <div class="usuario">
                    <p class="email-usuario">{{ $usuario->email }}</p>
                    @if($usuario->ativo == 1)
                    <p class="ativo">[ ATIVO ]</p>
                    @else
                    <p class="ativo">[ PENDENTE ]</p>
                    @endif
                    <div class="btns-gerenciamentos">
                        <div class="btns-icones">
                            <a href="{{ route('area-do-cliente.gerentes.usuarios.destroy', [$empresa->slug, $gerente->slug, $usuario->id]) }}" class="link-excluir" onclick="return confirm('Tem certeza que deseja excluir o usuario: {{ $usuario->email }}?')">
                                <img src="{{ asset('assets/img/layout/areacliente-icone-lixeira-branco.svg') }}" alt="Excluir" class="img-excluir">
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <p class="nenhum-resultado">Nenhum usuário cadastrado.</p>
                @endif
            </div>
        </div>

    </div>
</section>

@endsection