@extends('frontend.common.template')

@section('content')

<section class="ac-admin">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - EMPRESA: {{ $empresa->nome }} - EDITAR GERENTE</h2>

        @include('area-do-cliente.admin-btns')

        <div class="ac-gerentes">
            <a href="{{ route('area-do-cliente.gerentes', $empresa->slug) }}" class="link-voltar">
                <img src="{{ asset('assets/img/layout/setinha-dupla.svg') }}" alt="Voltar" class="img-voltar">
                VOLTAR
            </a>
            <form action="{{ route('area-do-cliente.gerentes.update', [$empresa->slug, $gerente->slug]) }}" class="form-gerente" method="POST">
                {!! csrf_field() !!}
                <label for="nome">Nome do Gerente:</label>
                <input type="text" name="nome" value="{{ $gerente->nome }}">
                <label for="telefone">Telefone/Celular:</label>
                <input type="text" name="telefone" class="telefone-usuario" value="{{ $gerente->telefone }}">
                <label for="email">E-mail:</label>
                <input type="email" name="email" value="{{ $gerente->email }}">

                <div class="info-senha">
                    Senha padrão (deve ser alterada pelo usuário para ativar o cadastro):
                    <span>alterar#123</span>
                </div>
                
                <button type="submit">ATUALIZAR</button>

                @include('area-do-cliente._flash')
            </form>
        </div>

    </div>
</section>

@endsection