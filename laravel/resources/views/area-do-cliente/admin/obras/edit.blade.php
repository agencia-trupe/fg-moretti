@extends('frontend.common.template')

@section('content')

<section class="ac-admin">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - EDITAR OBRA</h2>

        @include('area-do-cliente.admin-btns')

        <div class="ac-obras">
            <a href="{{ route('area-do-cliente.obras') }}" class="link-voltar">
                <img src="{{ asset('assets/img/layout/setinha-dupla.svg') }}" alt="Voltar" class="img-voltar">
                VOLTAR
            </a>
            <form action="{{ route('area-do-cliente.obras.update', $obra->slug) }}" class="form-obra" method="POST">
                {!! csrf_field() !!}
                <label for="titulo">Título da Obra:</label>
                <input type="text" name="titulo" value="{{ $obra->titulo }}">
                <label for="localizacao">Localização:</label>
                <input type="text" name="localizacao" value="{{ $obra->localizacao }}">
                <label for="codigo">Job/Código:</label>
                <input type="text" name="codigo" value="{{ $obra->codigo }}">
                <button type="submit">ATUALIZAR</button>

                @include('area-do-cliente._flash')
            </form>
        </div>

    </div>
</section>

@endsection