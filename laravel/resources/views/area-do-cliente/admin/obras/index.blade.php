@extends('frontend.common.template')

@section('content')

<section class="ac-admin">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - OBRAS</h2>

        @include('area-do-cliente.admin-btns')

        <div class="ac-obras">

            @include('area-do-cliente._flash')

            <a href="{{ route('area-do-cliente.obras.create') }}" class="link-adicionar">
                <img src="{{ asset('assets/img/layout/areacliente-icone-mais.svg') }}" alt="Adicionar" class="img-mais">
                ADICIONAR OBRA
            </a>
            <div class="lista-resultados">
                @if(count($obras) > 0)
                @foreach($obras as $obra)
                <div class="obra">
                    <img src="{{ asset('assets/img/layout/areacliente-icone-obras.svg') }}" alt="" class="icone-local">
                    <p class="titulo-obra">{{ $obra->titulo }}</p>
                    <p class="localizacao">[ {{ $obra->localizacao }} ]</p>
                    <p class="codigo-obra">JOB: {{ $obra->codigo }}</p>
                    <div class="btns-gerenciamentos">
                        <a href="{{ route('area-do-cliente.documentos', $obra->slug) }}" class="link-documentos-obra">DOCUMENTOS</a>
                        <a href="{{ route('area-do-cliente.obras.comunicacao', $obra->slug) }}" class="link-comunicacao-obra">COMUNICAÇÃO</a>
                        <div class="btns-icones">
                            <a href="{{ route('area-do-cliente.obras.edit', $obra->slug) }}" class="link-editar">
                                <img src="{{ asset('assets/img/layout/editar-branco.svg') }}" alt="Editar" class="img-editar">
                            </a>
                            <a href="{{ route('area-do-cliente.obras.destroy', $obra->slug) }}" class="link-excluir" onclick="return confirm('Tem certeza que deseja excluir a obra: {{ $obra->titulo }}?')">
                                <img src="{{ asset('assets/img/layout/areacliente-icone-lixeira-branco.svg') }}" alt="Excluir" class="img-excluir">
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <p class="nenhum-resultado">Nenhuma obra cadastrada.</p>
                @endif
            </div>
        </div>

    </div>
</section>

@endsection