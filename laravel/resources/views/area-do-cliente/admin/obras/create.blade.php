@extends('frontend.common.template')

@section('content')

<section class="ac-admin">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - ADICIONAR OBRA</h2>

        @include('area-do-cliente.admin-btns')

        <div class="ac-obras">
            <a href="{{ route('area-do-cliente.obras') }}" class="link-voltar">
                <img src="{{ asset('assets/img/layout/setinha-dupla.svg') }}" alt="Voltar" class="img-voltar">
                VOLTAR
            </a>
            <form action="{{ route('area-do-cliente.obras.store') }}" class="form-obra" method="POST">
                {!! csrf_field() !!}
                <label for="titulo">Título da Obra:</label>
                <input type="text" name="titulo" value="{{ old('titulo') }}">
                <label for="localizacao">Localização:</label>
                <input type="text" name="localizacao" value="{{ old('localizacao') }}">
                <label for="codigo">Job/Código:</label>
                <input type="text" name="codigo" value="{{ old('codigo') }}">
                <button type="submit">ADICIONAR</button>

                @include('area-do-cliente._flash')
            </form>
        </div>

    </div>
</section>

@endsection