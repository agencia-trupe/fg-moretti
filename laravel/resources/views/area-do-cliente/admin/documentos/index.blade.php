@extends('frontend.common.template')

@section('content')

<section class="ac-admin">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - OBRA: {{ $obra->titulo }} ({{$obra->codigo}}) - DOCUMENTOS</h2>

        @include('area-do-cliente.admin-btns')

        <div class="ac-documentos">

            @include('area-do-cliente._flash')

            <a href="{{ route('area-do-cliente.documentos.create', $obra->slug) }}" class="link-adicionar">
                <img src="{{ asset('assets/img/layout/areacliente-icone-mais.svg') }}" alt="Adicionar" class="img-mais">
                ADICIONAR DOCUMENTO
            </a>
            <div class="lista-resultados">
                @if(count($documentos) > 0)
                <table class="table table-sortable" data-table="ac_documentos" data-order-url="{{ route('documentos.order') }}" style="width: 100%;">
                    <tbody>
                        @foreach($documentos as $documento)
                        <tr class="tr-row" id="{{ $documento->id }}" style="display:flex;align-items:center;">
                            <td class="handle-ordem">
                                <a href="#" class="btn-ordem" title="ordenar">icone ordem</a>
                            </td>
                            <td class="documento">
                                <div class="icone-pdf"><img src="{{ asset('assets/img/layout/areacliente-icone-PDF.svg') }}" alt="" class="img-pdf"></div>
                                <p class="titulo-documento">{{ $documento->titulo }}</p>
                                <p class="descricao-documento">{!! $documento->descricao !!}</p>
                                <div class="btns-gerenciamentos">
                                    <a href="{{ route('area-do-cliente.versoes', [$obra->slug, $documento->id]) }}" class="link-versoes-obra">VERSÕES</a>
                                    <div class="btns-icones">
                                        <a href="{{ route('area-do-cliente.documentos.edit', [$obra->slug, $documento->id]) }}" class="link-editar">
                                            <img src="{{ asset('assets/img/layout/editar-branco.svg') }}" alt="Editar" class="img-editar">
                                        </a>
                                        <a href="{{ route('area-do-cliente.documentos.destroy', [$obra->slug, $documento->id]) }}" class="link-excluir" onclick="return confirm('Tem certeza que deseja excluir o documento: {{ $documento->titulo }}?')">
                                            <img src="{{ asset('assets/img/layout/areacliente-icone-lixeira-branco.svg') }}" alt="Excluir" class="img-excluir">
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                <p class="nenhum-resultado">Nenhum documento cadastrado.</p>
                @endif
            </div>
        </div>

    </div>
</section>

@endsection