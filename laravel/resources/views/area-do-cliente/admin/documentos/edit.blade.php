@extends('frontend.common.template')

@section('content')

<section class="ac-admin">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - OBRA: {{ $obra->titulo }} ({{$obra->codigo}}) - EDITAR DOCUMENTO</h2>

        @include('area-do-cliente.admin-btns')

        <div class="ac-documentos">
            <a href="{{ route('area-do-cliente.documentos', $obra->slug) }}" class="link-voltar">
                <img src="{{ asset('assets/img/layout/setinha-dupla.svg') }}" alt="Voltar" class="img-voltar">
                VOLTAR
            </a>
            <form action="{{ route('area-do-cliente.documentos.update', [$obra->slug, $documento->id]) }}" class="form-doc" method="POST">
                {!! csrf_field() !!}
                <label for="titulo">Título do Documento:</label>
                <input type="text" name="titulo" value="{{ $documento->titulo }}">
                <label for="descricao">Descrição do Documento:</label>
                <textarea name="descricao" id="descricao" value="{!! $documento->descricao !!}">{!! $documento->descricao !!}</textarea>
                <button type="submit">ATUALIZAR</button>

                @include('area-do-cliente._flash')
            </form>
        </div>

    </div>
</section>

@endsection