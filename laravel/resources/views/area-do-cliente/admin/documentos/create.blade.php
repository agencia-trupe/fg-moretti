@extends('frontend.common.template')

@section('content')

<section class="ac-admin">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - OBRA: {{ $obra->titulo }} ({{$obra->codigo}}) - ADICIONAR DOCUMENTO</h2>

        @include('area-do-cliente.admin-btns')

        <div class="ac-documentos">
            <a href="{{ route('area-do-cliente.documentos', $obra->slug) }}" class="link-voltar">
                <img src="{{ asset('assets/img/layout/setinha-dupla.svg') }}" alt="Voltar" class="img-voltar">
                VOLTAR
            </a>
            <form action="{{ route('area-do-cliente.documentos.store', $obra->slug) }}" class="form-doc" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <label for="titulo">Título do Documento:</label>
                <input type="text" name="titulo" value="{{ old('titulo') }}">
                <label for="descricao">Descrição do Documento:</label>
                <textarea name="descricao" id="descricao" value="{{ old('descricao') }}"></textarea>
                <label for="arquivo">Arquivo:</label>
                <input type="file" name="arquivo" id="arquivo" value="{{ old('arquivo') }}">
                <button type="submit">ADICIONAR</button>

                @include('area-do-cliente._flash')
            </form>
        </div>

    </div>
</section>

@endsection