@extends('frontend.common.template')

@section('content')

<section class="ac-admin">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - OBRA: {{ $obra->titulo }} ({{$obra->codigo}}) - DOCUMENTO: {{ $documento->titulo }} - ADICIONAR VERSÃO</h2>

        @include('area-do-cliente.admin-btns')

        <div class="ac-versoes">
            <a href="{{ route('area-do-cliente.versoes', [$obra->slug, $documento->id]) }}" class="link-voltar">
                <img src="{{ asset('assets/img/layout/setinha-dupla.svg') }}" alt="Voltar" class="img-voltar">
                VOLTAR
            </a>
            <form action="{{ route('area-do-cliente.versoes.store', [$obra->slug, $documento->id]) }}" class="form-versao" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <label for="versao">Número da Versão:</label>
                <input type="text" name="versao" value="{{ old('versao') }}">
                <label for="titulo">Título da Versão:</label>
                <input type="text" name="titulo" value="{{ old('titulo') }}">
                <label for="arquivo">Arquivo:</label>
                <input type="file" name="arquivo" id="arquivo" value="{{ old('arquivo') }}">
                <button type="submit">ADICIONAR</button>

                @include('area-do-cliente._flash')
            </form>
        </div>

    </div>
</section>

@endsection