@extends('frontend.common.template')

@section('content')

<section class="ac-admin">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - OBRA: {{ $obra->titulo }} ({{$obra->codigo}}) - DOCUMENTO: {{ $documento->titulo }} - EDITAR VERSÃO</h2>

        @include('area-do-cliente.admin-btns')

        <div class="ac-versoes">
            <a href="{{ route('area-do-cliente.versoes', [$obra->slug, $documento->id]) }}" class="link-voltar">
                <img src="{{ asset('assets/img/layout/setinha-dupla.svg') }}" alt="Voltar" class="img-voltar">
                VOLTAR
            </a>
            <form action="{{ route('area-do-cliente.versoes.update', [$obra->slug, $documento->id, $versao->id]) }}" class="form-versao" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <label for="versao">Número da Versão:</label>
                <input type="text" name="versao" value="{{ $versao->versao }}">
                <label for="titulo">Título da Versão:</label>
                <input type="text" name="titulo" value="{{ $versao->titulo }}">
                <label for="arquivo">Arquivo:</label>
                <a href="{{ route('area-do-cliente.versoes.arquivo', [$obra->slug, $documento->id, $versao->id]) }}" target="_blank" class="link-arquivo" onclick="return confirm('Deseja fazer o download do documento: {{ $versao->arquivo }}?')">{{ $versao->arquivo }}</a>
                <input type="file" name="arquivo" id="arquivo" value="{{ $versao->arquivo }}">
                <button type="submit">ATUALIZAR</button>

                @include('area-do-cliente._flash')
            </form>
        </div>

    </div>
</section>

@endsection