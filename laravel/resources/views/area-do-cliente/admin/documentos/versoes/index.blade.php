@extends('frontend.common.template')

@section('content')

<section class="ac-admin">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - OBRA: {{ $obra->titulo }} ({{$obra->codigo}}) - DOCUMENTO: {{ $documento->titulo }} - VERSÕES</h2>

        @include('area-do-cliente.admin-btns')

        <div class="ac-versoes">

            @include('area-do-cliente._flash')

            <div class="voltar-e-adicionar">
                <a href="{{ route('area-do-cliente.documentos', $obra->slug) }}" class="link-voltar">
                    <img src="{{ asset('assets/img/layout/setinha-dupla.svg') }}" alt="Voltar" class="img-voltar">
                    VOLTAR PARA DOCUMENTOS
                </a>
                <a href="{{ route('area-do-cliente.versoes.create', [$obra->slug, $documento->id]) }}" class="link-adicionar">
                    <img src="{{ asset('assets/img/layout/areacliente-icone-mais.svg') }}" alt="Adicionar" class="img-mais">
                    ADICIONAR VERSÃO
                </a>
            </div>

            <div class="lista-resultados">
                @if(count($versoes) > 0)
                @foreach($versoes as $versao)
                <div class="versao">
                    <div class="icone-pdf"><img src="{{ asset('assets/img/layout/areacliente-icone-PDF.svg') }}" alt="" class="img-pdf"></div>
                    <p class="titulo-versao">{{ $versao->titulo }}</p>
                    <p class="n-versao">[{{ $versao->versao }}]</p>
                    <a href="{{ route('area-do-cliente.versoes.arquivo', [$obra->slug, $documento->id, $versao->id]) }}" class="link-arquivo" target="_blank" onclick="return confirm('Deseja fazer o download do documento: {{ $versao->arquivo }}?')">{{ $versao->arquivo }}</a>
                    <div class="btns-gerenciamentos">
                        <div class="btns-icones">
                            <a href="{{ route('area-do-cliente.versoes.edit', [$obra->slug, $documento->id, $versao->id]) }}" class="link-editar">
                                <img src="{{ asset('assets/img/layout/editar-branco.svg') }}" alt="Editar" class="img-editar">
                            </a>
                            <a href="{{ route('area-do-cliente.versoes.destroy', [$obra->slug, $documento->id, $versao->id]) }}" class="link-excluir" onclick="return confirm('Tem certeza que deseja excluir o documento: {{ $documento->titulo }}?')">
                                <img src="{{ asset('assets/img/layout/areacliente-icone-lixeira-branco.svg') }}" alt="Excluir" class="img-excluir">
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <p class="nenhum-resultado">Nenhuma versão cadastrada.</p>
                @endif
            </div>
        </div>

    </div>
</section>

@endsection