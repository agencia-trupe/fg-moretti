@extends('frontend.common.template')

@section('content')

<section class="ac-admin">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - CLIENTES/EMPRESAS</h2>

        @include('area-do-cliente.admin-btns')

        <div class="ac-empresas">

            @include('area-do-cliente._flash')

            <a href="{{ route('area-do-cliente.empresas.create') }}" class="link-adicionar">
                <img src="{{ asset('assets/img/layout/areacliente-icone-mais.svg') }}" alt="Adicionar" class="img-mais">
                ADICIONAR EMPRESA
            </a>

            <div class="lista-resultados">
                @if(count($empresas) > 0)
                @foreach($empresas as $empresa)
                <div class="empresa">
                    <p class="nome-empresa">{{ $empresa->nome }}</p>
                    <p class="cnpj">{{ $empresa->cnpj }}</p>
                    <div class="btns-gerenciamentos">
                        <a href="{{ route('area-do-cliente.gerentes', $empresa->slug) }}" class="link-gerentes">GERENTES</a>
                        <div class="btns-icones">
                            <a href="{{ route('area-do-cliente.empresas.edit', $empresa->slug) }}" class="link-editar">
                                <img src="{{ asset('assets/img/layout/editar-branco.svg') }}" alt="Editar" class="img-editar">
                            </a>
                            <a href="{{ route('area-do-cliente.empresas.destroy', $empresa->slug) }}" class="link-excluir" onclick="return confirm('Tem certeza que deseja excluir a empresa: {{ $empresa->nome }}?')">
                                <img src="{{ asset('assets/img/layout/areacliente-icone-lixeira-branco.svg') }}" alt="Excluir" class="img-excluir">
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <p class="nenhum-resultado">Nenhuma empresa cadastrada.</p>
                @endif
            </div>
        </div>

    </div>
</section>

@endsection