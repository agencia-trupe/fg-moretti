@extends('frontend.common.template')

@section('content')

<section class="ac-admin">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - EDITAR EMPRESA</h2>

        @include('area-do-cliente.admin-btns')

        <div class="ac-empresas">
            <a href="{{ route('area-do-cliente.empresas') }}" class="link-voltar">
                <img src="{{ asset('assets/img/layout/setinha-dupla.svg') }}" alt="Voltar" class="img-voltar">
                VOLTAR
            </a>
            <form action="{{ route('area-do-cliente.empresas.update', $empresa->slug) }}" class="form-empresa" method="POST">
                {!! csrf_field() !!}
                <label for="nome">Nome da Empresa:</label>
                <input type="text" name="nome" value="{{ $empresa->nome }}">
                <label for="cnpj">CNPJ:</label>
                <input type="text" name="cnpj" class="cnpj" value="{{ $empresa->cnpj }}">
                <button type="submit">ATUALIZAR</button>

                @include('area-do-cliente._flash')
            </form>
        </div>

    </div>
</section>

@endsection