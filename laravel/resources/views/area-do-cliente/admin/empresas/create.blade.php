@extends('frontend.common.template')

@section('content')

<section class="ac-admin">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - ADICIONAR EMPRESA</h2>

        @include('area-do-cliente.admin-btns')

        <div class="ac-empresas">
            <a href="{{ route('area-do-cliente.empresas') }}" class="link-voltar">
                <img src="{{ asset('assets/img/layout/setinha-dupla.svg') }}" alt="Voltar" class="img-voltar">
                VOLTAR
            </a>
            <form action="{{ route('area-do-cliente.empresas.store') }}" class="form-empresa" method="POST">
                {!! csrf_field() !!}
                <label for="nome">Nome da Empresa:</label>
                <input type="text" name="nome" value="{{ old('nome') }}">
                <label for="cnpj">CNPJ:</label>
                <input type="text" name="cnpj" class="cnpj" value="{{ old('cnpj') }}">
                <button type="submit">ADICIONAR</button>

                @include('area-do-cliente._flash')
            </form>
        </div>

    </div>
</section>

@endsection