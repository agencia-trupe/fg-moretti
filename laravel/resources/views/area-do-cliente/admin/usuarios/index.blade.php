@extends('frontend.common.template')

@section('content')

<section class="ac-admin">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - USUÁRIOS FG MORETTI</h2>

        @include('area-do-cliente.admin-btns')

        <div class="ac-usuarios">

            @include('area-do-cliente._flash')

            <a href="{{ route('area-do-cliente.usuarios.create') }}" class="link-adicionar">
                <img src="{{ asset('assets/img/layout/areacliente-icone-mais.svg') }}" alt="Adicionar" class="img-mais">
                ADICIONAR USUÁRIO
            </a>

            <div class="lista-resultados">
                @if(count($usuarios) > 0)
                @foreach($usuarios as $usuario)
                <div class="usuario">
                    <p class="nome-usuario">{{ $usuario->name }}</p>
                    <p class="email">{{ $usuario->email }}</p>
                    @if($usuario->ativo == 1)
                    <p class="ativo">[ ATIVO ]</p>
                    @else
                    <p class="ativo">[ PENDENTE ]</p>
                    @endif
                    @if($usuario->email != 'trupe@trupe.net')
                    <div class="btns-gerenciamentos">
                        <div class="btns-icones">
                            <a href="{{ route('area-do-cliente.usuarios.edit', $usuario->id) }}" class="link-editar">
                                <img src="{{ asset('assets/img/layout/editar-branco.svg') }}" alt="Editar" class="img-editar">
                            </a>
                            <a href="{{ route('area-do-cliente.usuarios.destroy', $usuario->id) }}" class="link-excluir" onclick="return confirm('Tem certeza que deseja excluir o usuário: {{ $usuario->name }}?')">
                                <img src="{{ asset('assets/img/layout/areacliente-icone-lixeira-branco.svg') }}" alt="Excluir" class="img-excluir">
                            </a>
                        </div>
                    </div>
                    @endif
                </div>
                @endforeach
                @else
                <p class="nenhum-resultado">Nenhum usuário cadastrado.</p>
                @endif
            </div>
        </div>

    </div>
</section>

@endsection