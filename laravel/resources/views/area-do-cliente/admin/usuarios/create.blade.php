@extends('frontend.common.template')

@section('content')

<section class="ac-admin">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - ADICIONAR USUÁRIO FG MORETTI</h2>

        @include('area-do-cliente.admin-btns')

        <div class="ac-usuarios">
            <a href="{{ route('area-do-cliente.usuarios') }}" class="link-voltar">
                <img src="{{ asset('assets/img/layout/setinha-dupla.svg') }}" alt="Voltar" class="img-voltar">
                VOLTAR
            </a>
            <form action="{{ route('area-do-cliente.usuarios.store') }}" class="form-usuario" method="POST">
                {!! csrf_field() !!}
                <label for="name">Nome do Usuário:</label>
                <input type="text" name="name" value="{{ old('name') }}">
                <label for="email">E-mail:</label>
                <input type="email" name="email" value="{{ old('email') }}">

                <div class="info-senha">
                    Senha padrão (deve ser alterada pelo usuário para ativar o cadastro):
                    <span>alterar#123</span>
                </div>

                <button type="submit">ADICIONAR</button>

                @include('area-do-cliente._flash')
            </form>
        </div>

    </div>
</section>

@endsection