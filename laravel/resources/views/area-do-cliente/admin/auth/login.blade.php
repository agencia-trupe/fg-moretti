@extends('frontend.common.template')

@section('content')

<main class="ac-login">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE [Área Administrativa] - LOGIN</h2>

        <form action="{{ route('admin.login') }}" class="form-login" method="POST">
            {!! csrf_field() !!}
            <div class="caixa-identifique-se">
                <p class="txt-identifique-se">IDENTIFIQUE-SE PARA ACESSAR CONTEÚDO RESTRITO</p>
                <img src="{{ asset('assets/img/layout/areacliente-seta-gde.svg') }}" alt="" class="img-seta">
            </div>

            <div class="inputs-form">
                <input type="email" name="email" value="{{ old('email') }}" placeholder="login (e-mail)" required>
                <input type="password" name="password" placeholder="senha" required>

                <input type="submit" value="ENTRAR" class="btn btn-primary">

                <a href="{{ route('admin.password.linkRequest') }}" class="link-esqueci-senha">esqueci minha senha »</a>
            </div>
        </form>

        @include('area-do-cliente._flash')

    </div>
</main>

@endsection