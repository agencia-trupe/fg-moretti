@extends('frontend.common.template')

@section('content')

<section class="ac-login">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - ALTERAR SENHA E ATIVAR CADASTRO</h2>

        <form action="{{ route('admin.alterar-senha.post', [$email, $token]) }}" class="form-alterar" method="POST">
            {!! csrf_field() !!}

            <div class="inputs-form">
                <input type="hidden" name="token" value="{{ $token }}">
                <input type="hidden" name="email" value="{{ $email }}" required>

                <label for="password">Senha atual:</label>
                <input type="password" name="password" required>

                <label for="password">Nova senha:</label>
                <input type="password" name="nova_senha" required>
                <label for="password">Confirme a nova senha:</label>
                <input type="password" name="nova_senha_confirmation" required>

                <button type="submit">SALVAR</button>
            </div>
        </form>
        
        @include('area-do-cliente._flash')

    </div>
</section>

@endsection