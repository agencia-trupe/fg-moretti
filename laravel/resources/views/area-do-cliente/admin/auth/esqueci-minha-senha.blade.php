@extends('frontend.common.template')

@section('content')

<div class="ac-login">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - ESQUECI MINHA SENHA</h2>

        <form action="{{ route('admin.password.sendResetLink') }}" class="form-esqueci" method="POST">
            {{ csrf_field() }}

            <div class="inputs-form">
                <input type="email" name="email" value="{{ old('email') }}" placeholder="login (e-mail)" required>
                <input type="submit" value="SOLICITAR REDEFINIÇÃO DE SENHA" class="btn btn-primary">
            </div>
        </form>

        <a href="{{ route('admin.login') }}" class="link-voltar">« VOLTAR</a>

        @include('area-do-cliente._flash')

    </div>

</div>


@endsection