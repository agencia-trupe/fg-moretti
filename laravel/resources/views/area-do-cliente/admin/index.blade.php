@extends('frontend.common.template')

@section('content')

<section class="ac-admin">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - ÁREA ADMINISTRATIVA</h2>

        <div class="btns-home-admin">
            <a href="{{ route('area-do-cliente.obras') }}" class="link-obras">OBRAS</a>
            <a href="{{ route('area-do-cliente.empresas') }}" class="link-clientes">CLIENTES/EMPRESAS</a>
            <a href="{{ route('area-do-cliente.usuarios') }}" class="link-usuarios">USUÁRIOS FG MORETTI</a>
        </div>

    </div>
</section>

@endsection