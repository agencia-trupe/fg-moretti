<div class="btns-admin">
    <a href="{{ route('area-do-cliente.obras') }}" @if(Tools::routeIs(['area-do-cliente.obras*', 'area-do-cliente.documentos*' , 'area-do-cliente.versoes*' ])) class="link-obras active" @endif class="link-obras">OBRAS</a>
    <a href="{{ route('area-do-cliente.empresas') }}" @if(Tools::routeIs(['area-do-cliente.empresas*', 'area-do-cliente.gerentes*'])) class="link-clientes active" @endif class="link-clientes">CLIENTES/EMPRESAS</a>
    <a href="{{ route('area-do-cliente.usuarios') }}" @if(Tools::routeIs(['area-do-cliente.usuarios*'])) class="link-usuarios active" @endif class="link-usuarios">USUÁRIOS FG MORETTI</a>
</div>