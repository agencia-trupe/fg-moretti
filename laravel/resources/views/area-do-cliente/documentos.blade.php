@extends('frontend.common.template')

@section('content')

<section class="ac-clientes">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - OBRAS E DOCUMENTOS</h2>

        @include('area-do-cliente.clientes-btns')

        @include('area-do-cliente._flash')

        <div class="lista-obras-documentos">
            <h4 class="subtitulo">OBRAS</h4>

            @foreach($obras as $obra)
            <div class="obra-documentos">
                <div class="obra">
                    <div class="img-obra">
                        <img src="{{ asset('assets/img/layout/areacliente-icone-obras.svg') }}" alt="" class="icone-local">
                    </div>
                    <div class="dados-obra">
                        <p class="titulo-obra">{{ $obra->titulo }}</p>
                        <p class="localizacao">{{ $obra->localizacao }}</p>
                    </div>
                </div>
                <div class="documentos">
                    @php
                    $documentosObra = [];
                    foreach($documentos as $doc) {
                        if($doc->obra_id == $obra->id) {
                            if(count($documentosObra) < 3) { $documentosObra[]=$doc; } 
                        } 
                    }
                    @endphp 

                    @foreach($documentosObra as $doc)
                    <p class="ultimo-doc">ÚLTIMO DOCUMENTO ADICIONADO: {{ strftime("%d/%m/%Y ∙ %H:%M", strtotime($doc->updated_at)) }}</p>
                    @break
                    @endforeach

                    @foreach($documentosObra as $doc)
                    <p class="documento">{{ $doc->titulo }} | {{ $doc->versao }} | {{ strftime("%d/%m/%Y ∙ %H:%M", strtotime($doc->updated_at)) }}</p>
                    @endforeach

                </div>
                <a href="{{ route('area-do-cliente.clientes.documentos.obra', $obra->slug) }}" class="link-ver-docs">
                    <img src="{{ asset('assets/img/layout/areacliente-seta-gde-branco.svg') }}" alt="" class="icone-seta">
                </a>
            </div>
            @endforeach
        </div>

    </div>
</section>

@endsection