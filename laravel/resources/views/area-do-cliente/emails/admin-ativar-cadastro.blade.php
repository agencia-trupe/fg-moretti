<!DOCTYPE html>
<html>

<head>
    <title>[ATIVAR CADASTRO] GRUPO FG MORETTI</title>
    <meta charset="utf-8">
</head>

<body>
    <p style="color:#86174C;font-size:12px;font-family:Calibri,Verdana,Arial;line-height:1.5;margin:10px 0;">GRUPO FG MORETTI - <span style="font-style:italic;">Aviso de sistema</span></p>

    <hr style="width:20px;margin:0 auto 0 0">

    <p style='color:#0D0C0C;font-size:12px;font-family:Calibri,Verdana,Arial;line-height:1.5;margin:10px 0;'>Olá {{ $nome }},</p>
    <p style='color:#0D0C0C;font-size:12px;font-family:Calibri,Verdana,Arial;line-height:1.5;margin:0 0 10px 0;'>Você foi cadastrado para acessar a área do cliente do <span style="font-weight:600;">Grupo FG MORETTI</span>.</p>

    <p style='color:#0D0C0C;font-size:12px;font-family:Calibri,Verdana,Arial;line-height:1.5;margin:0 0 10px 0;'>Para ativar seu cadastro você deve alterar sua senha. A senha padrão é: <span style="font-weight:600;">alterar#123</span> – e você deve informá-la como senha atual para cadastrar uma nova senha</p>

    <p style='color:#0D0C0C;font-size:12px;font-family:Calibri,Verdana,Arial;line-height:1.5;margin:0 0 10px 30px;'>As senhas na área do cliente FG Moretti são criptografadas e seguras.</p>
    <p style='color:#0D0C0C;font-size:12px;font-family:Calibri,Verdana,Arial;line-height:1.5;margin:0 0 10px 30px;'>Crie sua nova senha acessando o link: <a href="{{ route('admin.alterar-senha', [urlencode($email), $token]) }}" style='color:#FFF;font-size:12px;font-weight:600;font-family:Calibri,Verdana,Arial;line-height:1.5;padding:3px;background-color:#0000CD;'>» ALTERAR SENHA E ATIVAR CADASTRO</a></p>

    <p style='color:#0D0C0C;font-size:12px;font-family:Calibri,Verdana,Arial;line-height:1.5;margin:20px 0;'>Gratos,</p>
    
    <p style='color:#0D0C0C;font-size:12px;font-family:Calibri,Verdana,Arial;line-height:1.5;margin:0;'>GRUPO FG MORETTI · ÁREA DO CLIENTE</p>
    <a href="https://www.fgmoretti.com.br/area-do-cliente" style='color:#0000CD;font-size:12px;font-family:Calibri,Verdana,Arial;line-height:1.5;'>www.fgmoretti.com.br/area-do-cliente</a>

    <p style='color:#8395d4;font-size:10px;font-family:Calibri,Verdana,Arial;line-height:1.5;margin:20px 0 0 0;'>PARA DÚVIDAS E OUTRAS INFORMAÇÕES ENTRE EM CONTATO DIRETAMENTE COM A FG MORETTI</p>
    <p style='color:#8395d4;font-size:10px;font-family:Calibri,Verdana,Arial;line-height:1.5;margin:0;'>DADOS DE CONTATO NO SITE: <a href="https://www.fgmoretti.com.br/" style='color:#8395d4;font-size:10px;font-family:Calibri,Verdana,Arial;line-height:1.5;'>www.fgmoretti.com.br</a></p>

</body>

</html>