<form action="{{ route('area-do-cliente.busca') }}" method="GET" class="form-busca-documentos">
    <input type="text" name="palavraChave" value="" placeholder="BUSCA" class="input-busca">
    <button type="submit" class="btn-buscar"><img src="{{ asset('assets/img/layout/ico-busca-vinho.svg') }}" alt="" class="img-lupa"></button>
</form>