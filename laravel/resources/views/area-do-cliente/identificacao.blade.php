@extends('frontend.common.template')

@section('content')

<section class="ac-clientes">
    <div class="centralizado">

        @if($usuario->ativo == 0)
        <h2 class="titulo" style="color:red;">ATENÇÃO!</h2>
        <div class="gerentes">
            <h2 class="titulo">Verificamos que seu cadastro não está ativo. Entre no link enviado para o seu e-mail e ative seu cadastro.</h2>
        </div>
        @else

        @if($usuario->gerente == 1)

        <h2 class="titulo">ÁREA DO CLIENTE - IDENTIFICAÇÃO E CRIAÇÃO DE GRUPO</h2>

        @include('area-do-cliente.clientes-btns')

        @include('area-do-cliente._flash')

        <div class="gerentes">
            <form action="{{ route('area-do-cliente.clientes.update', $usuario->slug) }}" class="form-edit-gerente" method="POST">
                {!! csrf_field() !!}
                <input type="hidden" name="gerente_id" class="input-gerente" value="{{ $usuario->id }}">
                <div class="label-input">
                    <label for="empresa">empresa</label>
                    <input type="text" name="empresa" value="{{ $empresa->nome }}" readonly>
                </div>
                <div class="label-input">
                    <label for="nome">nome do gerente</label>
                    <input type="text" name="nome" value="{{ $usuario->nome }}">
                </div>
                <div class="label-input">
                    <label for="email">e-mail</label>
                    <input type="email" name="email" value="{{ $usuario->email }}">
                </div>
                <div class="label-input">
                    <label for="telefone">telefone</label>
                    <input type="text" name="telefone" class="telefone-usuario" value="{{ $usuario->telefone }}">
                </div>

                <hr class="linha-form-cliente">
                <h4 class="alterar-senha">ALTERAR SENHA</h4>

                <div class="label-input">
                    <label for="password">senha</label>
                    <input type="password" name="password">
                </div>
                <div class="label-input">
                    <label for="password">repetir senha</label>
                    <input type="password" name="password_confirmation">
                </div>

                <button type="submit" class="btn-salvar">SALVAR ALTERAÇÕES</button>
            </form>
            <div class="gerente-usuarios">
                <h4 class="titulo-cadastro">VOCÊ É O CADASTRO PRINCIPAL DA SUA EMPRESA</h4>
                <p class="descricao-cadastro">Permite a criação de logins adicionais para o mesmo grupo/empresa que terão o mesmo acesso restrito aos projetos da empresa com a FG Moretti</p>
                <select name="obra_id" class="obras-cadastro" id="obrasGerente">
                    <option value="" selected>Selecione a OBRA para visualizar/adicionar logins</option>
                    @foreach($obras as $obra)
                    <option value="{{ $obra->id }}">{{ $obra->titulo }}</option>
                    @endforeach
                </select>
                <form action="{{ route('area-do-cliente.clientes.adicionar-usuario') }}" class="form-add-usuario" method="POST">
                    {!! csrf_field() !!}
                    <input type="hidden" name="empresa_id" value="{{ $empresa->id }}">
                    <input type="hidden" name="gerente_id" value="{{ $usuario->id }}">
                    <input type="hidden" name="obra_id" class="input-obra" value="">
                    <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    <button type="submit" class="btn-adicionar">ADICIONAR</button>
                </form>
                <div class="usuarios-gerente-obra">
                    <!-- AJAX -->
                </div>
            </div>
        </div>

        @else

        <h2 class="titulo">ÁREA DO CLIENTE - IDENTIFICAÇÃO</h2>

        @include('area-do-cliente.clientes-btns')

        @include('area-do-cliente._flash')

        <div class="usuarios">
            <form action="{{ route('area-do-cliente.clientes.update', $usuario->slug) }}" class="form-edit-usuario" method="POST">
                {!! csrf_field() !!}
                <div class="label-input">
                    <label for="empresa">empresa</label>
                    <input type="text" name="empresa" value="{{ $empresa->nome }}" readonly>
                </div>
                <div class="label-input">
                    <label for="gerente">nome do gerente</label>
                    <input type="text" name="gerente" value="{{ $gerente->nome }}" readonly>
                </div>
                <div class="label-input">
                    <label for="nome">seu nome</label>
                    <input type="text" name="nome" value="{{ $usuario->nome }}">
                </div>
                <div class="label-input">
                    <label for="email">e-mail</label>
                    <input type="email" name="email" value="{{ $usuario->email }}">
                </div>
                <div class="label-input">
                    <label for="telefone">telefone</label>
                    <input type="text" name="telefone" class="telefone-usuario" value="{{ $usuario->telefone }}">
                </div>

                <hr class="linha-form-cliente">
                <h4 class="alterar-senha">ALTERAR SENHA</h4>

                <div class="label-input">
                    <label for="password">senha</label>
                    <input type="password" name="password">
                </div>
                <div class="label-input">
                    <label for="password">repetir senha</label>
                    <input type="password" name="password_confirmation">
                </div>

                <button type="submit" class="btn-salvar">SALVAR ALTERAÇÕES</button>
            </form>

            <div class="usuario-obras">
                <h4 class="titulo-cadastro">VOCÊ FAZ PARTE DA EQUIPE DE <strong>{{ $gerente->nome }}</strong></h4>
                <p class="descricao-cadastro">Você recebe comunicados sobre novos documentos na área restrita juntamente com os demais integrantes da sua equipe.</p>
                <h4 class="lista-obras">NAS SEGUINTES OBRAS:</h4>
                @foreach($obras as $obra)
                <p class="titulo-obra">» {{ $obra->titulo }}</p>
                @endforeach
            </div>

            @endif
            @endif

        </div>
</section>

@endsection