@extends('frontend.common.template')

@section('content')

<section class="ac-clientes">
    <div class="centralizado">

        <h2 class="titulo">ÁREA DO CLIENTE - DOCUMENTOS DA OBRA</h2>

        @include('area-do-cliente.clientes-btns')

        @include('area-do-cliente.busca')

        @include('area-do-cliente._flash')

        <div class="lista-versoes-documento">
            <h4 class="subtitulo">{{ $obra->titulo }} ∙ {{ $obra->localizacao }}</h4>

            <div class="doc-versoes">
                <div class="left-documento">
                    <a href="{{ route('area-do-cliente.clientes.documentos.obra', $obra->slug) }}" class="link-voltar-documentos">« VOLTAR</a>
                    <div class="documento">
                        <div class="img-pdf">
                            <img src="{{ asset('assets/img/layout/areacliente-icone-PDF.svg') }}" alt="" class="icone-pdf">
                        </div>
                        <div class="dados-documento">
                            <p class="titulo-doc">{{ $documento->titulo }}</p>
                            <div class="texto-doc">{!! $documento->descricao !!}</div>
                        </div>
                    </div>
                    <p class="total-documentos">TOTAL DE DOCUMENTOS: {{ count($versoes) }}</p>
                </div>

                <div class="right-versoes">
                    @foreach($versoes as $versao)
                    <div class="versao">
                        <div class="dados-versao">
                            <p class="data">{{ strftime("%d/%m/%Y ∙ %H:%M", strtotime($versao->updated_at)) }}</p>
                            <p class="nome">{{ $versao->titulo }} | {{ $versao->versao }}</p>
                        </div>
                        <a href="{{ route('area-do-cliente.clientes.documentos.download', $versao->id) }}" class="link-download-versao">
                            <img src="{{ asset('assets/img/layout/areacliente-icone-download.svg') }}" alt="" class="icone-download">
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

@endsection