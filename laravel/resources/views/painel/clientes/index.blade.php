@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Clientes
        <a href="{{ route('painel.clientes.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Cliente</a>
    </h2>
</legend>

@if(!count($clientes))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="clientes">
    <thead>
        <tr>
            <th>Imagem</th>
            <th>Nome</th>
            <th>Link</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($clientes as $cliente)
        <tr class="tr-row" id="{{ $cliente->id }}">
            <td><img src="{{ asset('assets/img/clientes/'.$cliente->imagem) }}" style="width: 100%; max-width:100px;" alt=""></td>
            <td>{{ $cliente->nome }}</td>
            <td>{{ $cliente->link }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.clientes.destroy', $cliente->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.clientes.edit', $cliente->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

{!! $clientes->appends($_GET)->links() !!}

@endif

@endsection