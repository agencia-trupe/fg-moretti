@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Área do Cliente | Empresas
        <a href="{{ route('painel.area-do-cliente.empresas.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Empresa</a>
    </h2>
</legend>


@if(!count($empresas))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="area_clientes">
    <thead>
        <tr>
            <th>Empresa</th>
            <th>Nome</th>
            <th>E-mail</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($empresas as $empresa)
        <tr class="tr-row" id="{{ $empresa->id }}">
            <td>{{ $empresa->empresa }}</td>
            <td>{{ $empresa->nome }}</td>
            <td>
                <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $empresa->email }}" style="margin-right:5px;border:0;transition:background .3s">
                    <span class="glyphicon glyphicon-copy"></span>
                </button>
                {{ $empresa->email }}
            </td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.area-do-cliente.empresas.destroy', $empresa->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.area-do-cliente.empresas.edit', $empresa->id) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>


@endif

@endsection