@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Área do Cliente |</small> Editar Empresa</h2>
</legend>

{!! Form::model($empresa, [
'route' => ['painel.area-do-cliente.empresas.update', $empresa->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.area-do-cliente.empresas.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection