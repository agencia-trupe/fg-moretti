@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Área do Cliente |</small> Adicionar Empresa</h2>
</legend>

{!! Form::open(['route' => 'painel.area-do-cliente.empresas.store', 'files' => true]) !!}

@include('painel.area-do-cliente.empresas.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection