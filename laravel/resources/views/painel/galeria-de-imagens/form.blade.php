@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('aplicacao_id', 'Aplicações') !!}
    {!! Form::select('aplicacao_id', $aplicacoes , old('aplicacao_id'), ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título da obra') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto sobre a obra') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textEdit']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Imagem') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/obras/'.$obra->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.galeria-de-imagens.index') }}" class="btn btn-default btn-voltar">Voltar</a>