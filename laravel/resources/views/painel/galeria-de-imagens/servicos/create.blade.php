@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Obra: {{ $obra->titulo }} |</small> Adicionar Serviço</h2>
</legend>

{!! Form::model($servicos, [
'route' => ['painel.galeria-de-imagens.servicos.store', $obra->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.galeria-de-imagens.servicos.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection