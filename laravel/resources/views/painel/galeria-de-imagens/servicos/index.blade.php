@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<a href="{{ route('painel.galeria-de-imagens.index') }}" title="Voltar para Obras" class="btn btn-sm btn-default">&larr; Voltar para Obras</a>

<legend>
    <h2>
        <small>Obra: {{ $obra->titulo }} |</small> Serviços

        <a href="{{ route('painel.galeria-de-imagens.servicos.create', $obra->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Serviço</a>
    </h2>
</legend>

@if(!count($servicos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="obras_servicos">
    <thead>
        <tr>
            <th>Serviços</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($servicos as $servico)
        <tr class="tr-row" id="{{ $servico->id }}">
            <td>{{ $servico->servico_titulo }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['galeria.servicos.destroy', $obra->id, $servico->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection