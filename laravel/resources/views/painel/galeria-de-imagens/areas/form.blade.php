@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('area_id', 'Áreas de Negócio') !!}
    {!! Form::select('area_id', $areas , old('area_id'), ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.galeria-de-imagens.areas.index', $obra->id) }}" class="btn btn-default btn-voltar">Voltar</a>