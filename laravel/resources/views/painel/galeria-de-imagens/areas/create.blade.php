@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Obra: {{ $obra->titulo }} |</small> Adicionar Área de Negócio</h2>
</legend>

{!! Form::model($areas, [
'route' => ['painel.galeria-de-imagens.areas.store', $obra->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.galeria-de-imagens.areas.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection