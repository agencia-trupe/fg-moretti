@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<a href="{{ route('painel.galeria-de-imagens.index') }}" title="Voltar para Obras" class="btn btn-sm btn-default">&larr; Voltar para Obras</a>

<legend>
    <h2>
        <small>Obra: {{ $obra->titulo }} |</small> Áreas de Negócio

        <a href="{{ route('painel.galeria-de-imagens.areas.create', $obra->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Área de Negócio</a>
    </h2>
</legend>

@if(!count($areas))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="obras_areas">
    <thead>
        <tr>
            <th>Área de Negócio</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($areas as $area)
        <tr class="tr-row" id="{{ $area->id }}">
            <td>{{ $area->area_titulo }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.galeria-de-imagens.areas.destroy', $obra->id, $area->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection