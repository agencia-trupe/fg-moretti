@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Galeria de Imagens | Obras |</small> Adicionar Obra</h2>
</legend>

{!! Form::open(['route' => 'painel.galeria-de-imagens.store', 'files' => true]) !!}

@include('painel.galeria-de-imagens.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection