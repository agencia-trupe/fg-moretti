@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Galeria de Imagens | Obras |</small> Editar Obra</h2>
</legend>

{!! Form::model($obra, [
'route' => ['painel.galeria-de-imagens.update', $obra->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.galeria-de-imagens.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection