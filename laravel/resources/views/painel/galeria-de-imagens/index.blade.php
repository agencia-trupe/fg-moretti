@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Galeria de Imagens | Obras
        <a href="{{ route('painel.galeria-de-imagens.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Obra</a>
    </h2>
</legend>

@if(!count($obras))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="obras">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Titulo</th>
            <th>Aplicação</th>
            <th>Gerenciar</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($obras as $obra)
        <tr class="tr-row tabela-obras" id="{{ $obra->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            <td>{{ $obra->titulo }}</td>
            <td>{{ $obra->aplicacao_titulo }}</td>
            <td>
                <a href="{{ route('painel.galeria-de-imagens.areas.index', $obra->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Áreas de Negócio
                </a>
                <a href="{{ route('painel.galeria-de-imagens.servicos.index', $obra->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Serviços
                </a>
                <a href="{{ route('painel.galeria-de-imagens.imagens.index', $obra->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Imagens
                </a>
            </td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.galeria-de-imagens.destroy', $obra->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.galeria-de-imagens.edit', $obra->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

{!! $obras->appends($_GET)->links() !!}

@endif

@endsection