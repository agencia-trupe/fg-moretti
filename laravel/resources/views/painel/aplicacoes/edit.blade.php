@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Aplicações |</small> Editar Aplicação</h2>
</legend>

{!! Form::model($aplicacao, [
'route' => ['painel.aplicacoes.update', $aplicacao->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.aplicacoes.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection