@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Aplicação: {{ $aplicacao->titulo }} | Áreas de Negócio |</small> Editar Área</h2>
</legend>

{!! Form::model($aplicacaoArea, [
'route' => ['painel.aplicacoes.areas.update', $aplicacao->id, $aplicacaoArea->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.aplicacoes.areas.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection