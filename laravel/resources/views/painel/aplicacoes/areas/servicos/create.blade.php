@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Aplicação: {{ $aplicacao->titulo }} | Área: {{ $areaDeNegocio->titulo }} | Serviços |</small> Adicionar Serviço</h2>
</legend>

{!! Form::model($servicos, [
'route' => ['painel.aplicacoes.areas.servicos.store', $aplicacao->id, $area],
'method' => 'post',
'files' => true])
!!}

@include('painel.aplicacoes.areas.servicos.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection