@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('servico_id', 'Serviços') !!}
    {!! Form::select('servico_id', $servicos , old('servico_id'), ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.aplicacoes.areas.servicos.index', [$aplicacao->id, $area]) }}" class="btn btn-default btn-voltar">Voltar</a>