@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('area_id', 'Áreas de Negócio') !!}
    {!! Form::select('area_id', $areas , old('area_id'), ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_area', 'Texto sobre a área de negócio nesta aplicação') !!}
    {!! Form::textarea('texto_area', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textEdit']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.aplicacoes.areas.index', $aplicacao->id) }}" class="btn btn-default btn-voltar">Voltar</a>