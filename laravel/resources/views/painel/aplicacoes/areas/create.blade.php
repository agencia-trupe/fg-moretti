@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Aplicação: {{ $aplicacao->titulo }} | Áreas de Negócio |</small> Adicionar Área</h2>
</legend>

{!! Form::model($aplicacao, [
'route' => ['painel.aplicacoes.areas.store', $aplicacao->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.aplicacoes.areas.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection