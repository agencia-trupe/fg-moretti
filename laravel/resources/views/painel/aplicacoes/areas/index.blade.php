@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<a href="{{ route('painel.aplicacoes.index') }}" title="Voltar para Aplicações" class="btn btn-sm btn-default">&larr; Voltar para Aplicações</a>

<legend>
    <h2>
        Aplicação: {{ $aplicacao->titulo }} | Áreas de Negócio
        <a href="{{ route('painel.aplicacoes.areas.create', $aplicacao->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Área</a>
    </h2>
</legend>

@if(!count($areas))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="aplicacoes_areas">
    <thead>
        <tr>
            <th>Área de Negocio</th>
            <th>Gerenciar</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($areas as $area)
        <tr class="tr-row" id="{{ $area->id }}">
            <td>{{ $area->area_titulo }}</td>
            <td><a href="{{ route('painel.aplicacoes.areas.servicos.index', [$aplicacao->id, $area->id]) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Serviços
                </a></td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.aplicacoes.areas.destroy', $aplicacao->id, $area->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.aplicacoes.areas.edit', [$aplicacao->id, $area->id] ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection