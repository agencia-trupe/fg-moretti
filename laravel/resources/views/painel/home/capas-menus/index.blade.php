@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Home | Capas dos menus
    </h2>
</legend>


<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="capas_menus">
    <thead>
        <tr>
            <th>Titulo</th>
            <th>Imagem</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($capas as $capa)
        <tr class="tr-row" id="{{ $capa->id }}">
            <td>{{ $capa->titulo }}</td>
            <td><img src="{{ asset('assets/img/capas-menus/'.$capa->imagem) }}" style="width: 100%; max-width:150px;" alt=""></td>
            <td class="crud-actions">
                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.capas-menus.edit', $capa->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection