@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Home | Capas dos menus |</small> Editar capa: {{ $capa->titulo }}</h2>
</legend>

{!! Form::model($capa, [
'route' => ['painel.capas-menus.update', $capa->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.home.capas-menus.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection