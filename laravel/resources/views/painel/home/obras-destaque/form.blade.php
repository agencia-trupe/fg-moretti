@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('obra_id', 'Obras') !!}
    {!! Form::select('obra_id', $obras , old('obra_id'), ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.obras-destaque.index') }}" class="btn btn-default btn-voltar">Voltar</a>