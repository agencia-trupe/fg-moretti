@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Home | Obras em Destaque |</small> Adicionar Obra</h2>
</legend>

{!! Form::model($obras, [
'route' => ['painel.obras-destaque.store'],
'method' => 'post',
'files' => true])
!!}

@include('painel.home.obras-destaque.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection