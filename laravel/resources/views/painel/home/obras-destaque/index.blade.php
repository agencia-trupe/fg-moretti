@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Home | Obras em Destaque
        <a href="{{ route('painel.obras-destaque.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Obra</a>
    </h2>
</legend>

@if(!count($destaques))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="obras_destaque">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Imagem</th>
            <th>Titulo</th>
            <th>Áreas de negócio</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($destaques as $obra)
        <tr class="tr-row" id="{{ $obra->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            <td><img src="{{ asset('assets/img/obras/'.$obra->capa) }}" style="width: 100%; max-width:100px;" alt=""></td>
            <td>{{ $obra->titulo }}</td>

            @php
            $areas = App\Models\ObraArea::where('obra_id', $obra->obra_id)->join('areas_de_negocio', 'areas_de_negocio.id', '=', 'obras_areas.area_id')->select('obras_areas.obra_id as obra_id', 'areas_de_negocio.id as area_id', 'areas_de_negocio.titulo as titulo')->orderBy('area_id', 'ASC')->pluck('titulo');

            $arrayAreas = [];
            foreach($areas as $area) {
            $arrayAreas[] = $area;
            }
            @endphp
            <td>{{ implode("|", $arrayAreas) }}</td>

            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.obras-destaque.destroy', $obra->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection