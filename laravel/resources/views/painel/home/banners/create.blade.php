@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Home | Banners cabeçalho |</small> Adicionar Banner</h2>
</legend>

{!! Form::open(['route' => 'painel.banners-header.store', 'files' => true]) !!}

@include('painel.home.banners.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection