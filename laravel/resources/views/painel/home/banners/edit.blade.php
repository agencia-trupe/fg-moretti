@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Home | Banners cabeçalho |</small> Editar Banner</h2>
</legend>

{!! Form::model($banner, [
'route' => ['painel.banners-header.update', $banner->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.home.banners.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection