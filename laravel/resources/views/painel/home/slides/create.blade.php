@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Home | Slides |</small> Adicionar Slide</h2>
</legend>

{!! Form::open(['route' => 'painel.slides-home.store', 'files' => true]) !!}

@include('painel.home.slides.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection