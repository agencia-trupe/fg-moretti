@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Home | Slides |</small> Editar Slide</h2>
</legend>

{!! Form::model($slide, [
'route' => ['painel.slides-home.update', $slide->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.home.slides.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection