@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Serviços |</small> Editar Serviço</h2>
</legend>

{!! Form::model($servico, [
'route' => ['painel.servicos.update', $servico->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.servicos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection