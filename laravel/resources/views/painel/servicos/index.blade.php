@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Serviços
        <a href="{{ route('painel.servicos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Serviço</a>
    </h2>
</legend>

@if(!count($servicos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="servicos">
    <thead>
        <tr>
            <th>Titulo</th>
            <th>Grupo</th>
            <th>Gerenciar</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($servicos as $servico)
        <tr class="tr-row" id="{{ $servico->id }}">
            <td>{{ $servico->titulo }}</td>
            <td>{{ $servico->grupo_titulo }}</td>
            <td>
                <a href="{{ route('painel.servicos.imagens.index', $servico->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Imagens
                </a>
            </td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.servicos.destroy', $servico->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.servicos.edit', $servico->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

{!! $servicos->appends($_GET)->links() !!}

@endif

@endsection