@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('grupo_id', 'Grupos') !!}
    {!! Form::select('grupo_id', $grupos , old('grupo_id'), ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título do Serviço') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto sobre o serviço') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textEdit']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.servicos.index') }}" class="btn btn-default btn-voltar">Voltar</a>