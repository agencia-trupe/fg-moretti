@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Grupos de Serviços |</small> Editar Grupo</h2>
</legend>

{!! Form::model($grupo, [
'route' => ['painel.servicos-grupos.update', $grupo->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.servicos.grupos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection