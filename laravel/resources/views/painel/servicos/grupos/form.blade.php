@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título do Grupo') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.servicos-grupos.index') }}" class="btn btn-default btn-voltar">Voltar</a>