@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Grupos de Serviços |</small> Adicionar Grupo</h2>
</legend>

{!! Form::open(['route' => 'painel.servicos-grupos.store', 'files' => true]) !!}

@include('painel.servicos.grupos.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection