@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Grupos de Serviços
        <a href="{{ route('painel.servicos-grupos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Grupo</a>
    </h2>
</legend>

@if(!count($grupos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="servicos_grupos">
    <thead>
        <tr>
            <th>Titulo</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($grupos as $grupo)
        <tr class="tr-row" id="{{ $grupo->id }}">
            <td>{{ $grupo->titulo }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.servicos-grupos.destroy', $grupo->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.servicos-grupos.edit', $grupo->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

{!! $grupos->appends($_GET)->links() !!}

@endif

@endsection