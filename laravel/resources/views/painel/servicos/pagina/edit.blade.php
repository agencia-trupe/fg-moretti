@extends('painel.common.template')

@section('content')

<legend>
    <h2>Serviços | Texto da Página</h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.servicos-pagina.update', $registro->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.servicos.pagina.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection