@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto', 'Texto inicial da página de serviços') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textEdit']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}