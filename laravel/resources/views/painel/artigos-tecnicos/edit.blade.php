@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Artigos Técnicos |</small> Editar Artigo</h2>
</legend>

{!! Form::model($artigo, [
'route' => ['painel.artigos-tecnicos.update', $artigo->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.artigos-tecnicos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection