@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Artigos Técnicos
        <a href="{{ route('painel.artigos-tecnicos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Artigo</a>
    </h2>
</legend>


@if(!count($artigos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="artigos_tecnicos">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Titulo</th>
            <th>Arquivo</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($artigos as $artigo)
        <tr class="tr-row" id="{{ $artigo->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            <td>{{ $artigo->titulo }}</td>
            <td>
                <a href="{{ route('artigos-tecnicos.pdf', $artigo->slug) }}" target="_blank">{{ $artigo->arquivo }}</a>
            </td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.artigos-tecnicos.destroy', $artigo->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.artigos-tecnicos.edit', $artigo->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

{!! $artigos->appends($_GET)->links() !!}

@endif

@endsection