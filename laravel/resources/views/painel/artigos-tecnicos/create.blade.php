@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Artigos Técnicos |</small> Adicionar Artigo</h2>
</legend>

{!! Form::open(['route' => 'painel.artigos-tecnicos.store', 'files' => true]) !!}

@include('painel.artigos-tecnicos.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection