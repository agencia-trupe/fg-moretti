@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Titulo do Artigo') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('arquivo', 'Arquivo') !!}
    @if($submitText == 'Alterar')
    @if($artigo->arquivo)
    <a href="{{ route('artigos-tecnicos.pdf', $artigo->slug) }}" target="_blank" style="display:block; margin-bottom: 10px; max-width: 100%;">{{ $artigo->arquivo }}</a>
    @endif
    @endif
    {!! Form::file('arquivo', ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.artigos-tecnicos.index') }}" class="btn btn-default btn-voltar">Voltar</a>