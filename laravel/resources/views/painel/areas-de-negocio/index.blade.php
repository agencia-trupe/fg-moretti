@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Áreas de Negócio
    </h2>
</legend>


<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="areas_de_negocio">
    <thead>
        <tr>
            <th>Titulo</th>
            <th>Imagem</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($areas as $area)
        <tr class="tr-row" id="{{ $area->id }}">
            <td>{{ $area->titulo }}</td>
            <td><img src="{{ asset('assets/img/areas-de-negocio/'.$area->imagem) }}" style="width: 100%; max-width:150px;" alt=""></td>
            <td class="crud-actions">
                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.areas-de-negocio.edit', $area->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection