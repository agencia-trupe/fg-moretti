@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Nome da área de negócio') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_inicial', 'Texto 1 (inicial, sobre a área de negócio)') !!}
    {!! Form::textarea('texto_inicial', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textEdit']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_servicos', 'Texto 2 (entre aplicações e serviços) - Opcional') !!}
    {!! Form::textarea('texto_servicos', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textEdit']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/areas-de-negocio/'.$area->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.areas-de-negocio.index') }}" class="btn btn-default btn-voltar">Voltar</a>