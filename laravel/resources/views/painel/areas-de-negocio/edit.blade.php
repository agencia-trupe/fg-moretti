@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Áreas de Negócio |</small> Editar Área: {{ $area->titulo }}</h2>
</legend>

{!! Form::model($area, [
'route' => ['painel.areas-de-negocio.update', $area->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.areas-de-negocio.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection