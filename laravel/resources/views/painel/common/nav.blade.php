<ul class="nav navbar-nav">

    <li class="dropdown @if(Tools::routeIs(['painel.home*', 'painel.capas-menus*', 'painel.banners-header*', 'painel.banners-home*', 'painel.obras-destaque*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Home<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.capas-menus*')) class="active" @endif>
                <a href="{{ route('painel.capas-menus.index') }}">Capas dos Menus</a>
            </li>
            <li @if(Tools::routeIs('painel.banners-header*')) class="active" @endif>
                <a href="{{ route('painel.banners-header.index') }}">Home - 1.Banners</a>
            </li>
            <li @if(Tools::routeIs('painel.slides-home*')) class="active" @endif>
                <a href="{{ route('painel.slides-home.index') }}">Home - 2.Slides</a>
            </li>
            <li @if(Tools::routeIs('painel.home*')) class="active" @endif>
                <a href="{{ route('painel.home.index') }}">Home - 3.Faixa de texto</a>
            </li>
            <li @if(Tools::routeIs('painel.obras-destaque*')) class="active" @endif>
                <a href="{{ route('painel.obras-destaque.index') }}">Home - 4.Obras em destaque</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(Tools::routeIs(['painel.empresa-historia*', 'painel.empresa-nossa-otica*', 'painel.empresa-corpo-tecnico*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Empresa<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.empresa-historia*')) class="active" @endif>
                <a href="{{ route('painel.empresa-historia.index') }}">História</a>
            </li>
            <li @if(Tools::routeIs('painel.empresa-nossa-otica*')) class="active" @endif>
                <a href="{{ route('painel.empresa-nossa-otica.index') }}">Nossa Ótica</a>
            </li>
            <li @if(Tools::routeIs('painel.empresa-corpo-tecnico*')) class="active" @endif>
                <a href="{{ route('painel.empresa-corpo-tecnico.index') }}">Corpo Técnico</a>
            </li>
        </ul>
    </li>

    <li @if(Tools::routeIs('painel.areas-de-negocio*')) class="active" @endif>
        <a href="{{ route('painel.areas-de-negocio.index') }}">Áreas de Negócio</a>
    </li>

    <li @if(Tools::routeIs('painel.aplicacoes*')) class="active" @endif>
        <a href="{{ route('painel.aplicacoes.index') }}">Aplicações</a>
    </li>

    <li class="dropdown @if(Tools::routeIs(['painel.servicos-pagina.*', 'painel.servicos-grupos.*', 'painel.servicos.*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Serviços<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.servicos-pagina.index')) class="active" @endif>
                <a href="{{ route('painel.servicos-pagina.index') }}">Texto da Página</a>
            </li>
            <li @if(Tools::routeIs('painel.servicos-grupos.*')) class="active" @endif>
                <a href="{{ route('painel.servicos-grupos.index') }}">Grupos de Serviços</a>
            </li>
            <li @if(Tools::routeIs('painel.servicos.*')) class="active" @endif>
                <a href="{{ route('painel.servicos.index') }}">Serviços</a>
            </li>
        </ul>
    </li>

    <li @if(Tools::routeIs('painel.galeria-de-imagens*')) class="active" @endif>
        <a href="{{ route('painel.galeria-de-imagens.index') }}">Galeria/Obras</a>
    </li>

    <li class="dropdown @if(Tools::routeIs(['painel.artigos-tecnicos*', 'painel.clientes*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Artigos/Clientes<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.artigos-tecnicos*')) class="active" @endif>
                <a href="{{ route('painel.artigos-tecnicos.index') }}">Artigos Técnicos</a>
            </li>
            <li @if(Tools::routeIs('painel.clientes*')) class="active" @endif>
                <a href="{{ route('painel.clientes.index') }}">Clientes</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(Tools::routeIs('painel.contato*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>

</ul>