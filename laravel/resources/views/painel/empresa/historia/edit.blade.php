@extends('painel.common.template')

@section('content')

<legend>
    <h2>Empresa | História</h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.empresa-historia.update', $registro->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.empresa.historia.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection