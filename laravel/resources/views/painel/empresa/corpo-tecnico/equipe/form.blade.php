@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_sobre', 'Texto Sobre') !!}
    {!! Form::textarea('texto_sobre', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
</div>

<div class="well form-group">
    {!! Form::label('foto', 'Foto') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/corpo-tecnico-equipe/'.$equipe->foto) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('foto', ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.empresa-corpo-tecnico.equipe.index', $registro->id) }}" class="btn btn-default btn-voltar">Voltar</a>