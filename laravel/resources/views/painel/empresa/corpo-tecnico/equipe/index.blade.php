@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<a href="{{ route('painel.empresa-corpo-tecnico.index') }}" title="Voltar para Corpo Técnico" class="btn btn-sm btn-default">&larr; Voltar para Corpo Técnico</a>

<legend>
    <h2>
        Empresa | Corpo Técnico | Equipe
        <a href="{{ route('painel.empresa-corpo-tecnico.equipe.create', $registro->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Membro</a>
    </h2>
</legend>

@if(!count($equipe))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="empresa_corpo_tecnico_equipe">
    <thead>
        <tr>
            <th>Ordem</th>
            <th>Foto</th>
            <th>Nome</th>
            <th>Sobre</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($equipe as $membro)
        <tr class="tr-row" id="{{ $membro->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            <td><img src="{{ asset('assets/img/corpo-tecnico-equipe/'.$membro->foto) }}" style="width: 100%; max-width:100px;" alt=""></td>
            <td>{{ $membro->nome }}</td>
            <td>{!! $membro->texto_sobre !!}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.empresa-corpo-tecnico.equipe.destroy', $registro->id, $membro->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.empresa-corpo-tecnico.equipe.edit', [$registro->id, $membro->id] ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection