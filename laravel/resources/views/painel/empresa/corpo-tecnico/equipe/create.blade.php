@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Empresa | Corpo Técnico | Equipe |</small> Adicionar Membro</h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.empresa-corpo-tecnico.equipe.store', $registro->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.empresa.corpo-tecnico.equipe.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection