@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Empresa | Corpo Técnico | Equipe |</small> Editar Membro</h2>
</legend>

{!! Form::model($equipe, [
'route' => ['painel.empresa-corpo-tecnico.equipe.update', $registro->id, $equipe->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.empresa.corpo-tecnico.equipe.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection