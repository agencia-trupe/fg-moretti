@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Empresa | Corpo Técnico
        <a href="{{ route('painel.empresa-corpo-tecnico.equipe.index', $registro->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Gerenciar Equipe</a>
    </h2>
</legend>

<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="empresa_corpo_tecnico">
    <thead>
        <tr>
            <th>Texto sobre corpo técnico</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        <tr class="tr-row" id="{{ $registro->id }}">
            <td>{!! $registro->texto !!}</td>
            <td class="crud-actions">
                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.empresa-corpo-tecnico.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar Texto
                    </a>
                </div>
            </td>
        </tr>
    </tbody>
</table>

@endsection