@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Empresa | Corpo Técnico |</small> Editar Texto</h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.empresa-corpo-tecnico.update', $registro->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.empresa.corpo-tecnico.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection