@extends('painel.common.template')

@section('content')

<legend>
    <h2>Empresa | Nossa Ótica</h2>
</legend>

{!! Form::model($registro, [
'route' => ['painel.empresa-nossa-otica.update', $registro->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.empresa.nossa-otica.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection