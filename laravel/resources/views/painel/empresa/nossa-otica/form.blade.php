@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto', 'Nossa Ótica') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textEdit']) !!}
</div>

<div class="form-group">
    {!! Form::label('visao', 'Visão') !!}
    {!! Form::textarea('visao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textEdit']) !!}
</div>

<div class="form-group">
    {!! Form::label('missao', 'Missão') !!}
    {!! Form::textarea('missao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textEdit']) !!}
</div>

<div class="form-group">
    {!! Form::label('valores', 'Valores') !!}
    {!! Form::textarea('valores', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textEdit']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}