<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class CapaMenu extends Model
{
    protected $table = 'capas_menus';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 1200,
            'height' => 300,
            'path'   => 'assets/img/capas-menus/'
        ]);
    }
}
