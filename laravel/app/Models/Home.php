<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    protected $table = 'home';

    protected $guarded = ['id'];

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo', 'LIKE', '%' . $termo . '%')
            ->orWhere('texto', 'LIKE', '%' . $termo . '%');
    }
}
