<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServicosGrupo extends Model
{
    protected $table = 'servicos_grupos';

    protected $guarded = ['id'];

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo', 'LIKE', '%' . $termo . '%');
    }
}
