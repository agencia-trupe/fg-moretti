<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class ServicoImagem extends Model
{
    protected $table = 'servicos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeServico($query, $id)
    {
        return $query->where('servico_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 150,
                'height'  => null,
                'path'    => 'assets/img/servicos/thumbs/'
            ],
            [
                'width'  => null,
                'height' => 240,
                'upsize'  => true,
                'path'    => 'assets/img/servicos/'
            ]
        ]);
    }
}
