<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Servico extends Model
{
    protected $table = 'servicos';

    protected $guarded = ['id'];

    public function scopeGrupos($query, $id)
    {
        return $query->where('grupo_id', $id);
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ServicoImagem', 'servico_id')->ordenados();
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo', 'LIKE', '%' . $termo . '%')
            ->orWhere('texto', 'LIKE', '%' . $termo . '%');
    }
}
