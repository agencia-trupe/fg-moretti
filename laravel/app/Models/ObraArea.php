<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ObraArea extends Model
{
    protected $table = 'obras_areas';

    protected $guarded = ['id'];

    public function scopeObra($query, $id)
    {
        return $query->where('obra_id', $id);
    }

    public function areas()
    {
        return $this->hasMany('App\Models\AreaDeNegocio', 'area_id');
    }
}
