<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TermosDeUso extends Model
{
    protected $table = 'termos_de_uso';

    protected $guarded = ['id'];

    public function scopeBusca($query, $termo)
    {
        return $query->where('texto', 'LIKE', '%' . $termo . '%');
    }
}
