<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AplicacaoAreaServico extends Model
{
    protected $table = 'aplicacoes_areas_servicos';

    protected $guarded = ['id'];

    public function servicos()
    {
        return $this->hasMany('App\Models\Servico', 'servico_id');
    }

    public function scopeApparea($query, $id)
    {
        return $query->where('aplicacao_area_id', $id);
    }
}
