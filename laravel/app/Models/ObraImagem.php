<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class ObraImagem extends Model
{
    protected $table = 'obras_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeObra($query, $id)
    {
        return $query->where('obra_id', $id);
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 1300,
            'height' => null,
            'path'   => 'assets/img/obras/imagens/'
        ]);
    }
}
