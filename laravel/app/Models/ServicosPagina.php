<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServicosPagina extends Model
{
    protected $table = 'servicos_pagina';

    protected $guarded = ['id'];

    public function scopeBusca($query, $termo)
    {
        return $query->where('texto', 'LIKE', '%' . $termo . '%');
    }
}
