<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AplicacaoArea extends Model
{
    protected $table = 'aplicacoes_areas';

    protected $guarded = ['id'];

    public function scopeAplicacao($query, $id)
    {
        return $query->where('aplicacao_id', $id);
    }

    public function scopeArea($query, $id)
    {
        return $query->where('area_id', $id);
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('texto_area', 'LIKE', '%' . $termo . '%');
    }
}
