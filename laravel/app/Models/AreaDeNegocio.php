<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class AreaDeNegocio extends Model
{
    protected $table = 'areas_de_negocio';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/areas-de-negocio/'
        ]);
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo', 'LIKE', '%' . $termo . '%')
            ->orWhere('texto_inicial', 'LIKE', '%' . $termo . '%')
            ->orWhere('texto_servicos', 'LIKE', '%' . $termo . '%');
    }
}
