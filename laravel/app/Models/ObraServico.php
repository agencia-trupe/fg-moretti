<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ObraServico extends Model
{
    protected $table = 'obras_servicos';

    protected $guarded = ['id'];

    public function scopeObra($query, $id)
    {
        return $query->where('obra_id', $id);
    }

    public function servicos()
    {
        return $this->hasMany('App\Models\Servico', 'servico_id');
    }
}
