<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Aplicacao extends Model
{
    protected $table = 'aplicacoes';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 300,
            'height' => 300,
            'path'   => 'assets/img/aplicacoes/'
        ]);
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo', 'LIKE', '%' . $termo . '%')
            ->orWhere('texto', 'LIKE', '%' . $termo . '%');
    }
}
