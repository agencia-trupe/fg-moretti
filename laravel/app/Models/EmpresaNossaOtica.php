<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmpresaNossaOtica extends Model
{
    protected $table = 'empresa_nossa_otica';

    protected $guarded = ['id'];

    public function scopeBusca($query, $termo)
    {
        return $query->where('texto', 'LIKE', '%' . $termo . '%')
            ->orWhere('visao', 'LIKE', '%' . $termo . '%')
            ->orWhere('missao', 'LIKE', '%' . $termo . '%')
            ->orWhere('valores', 'LIKE', '%' . $termo . '%');
    }
}
