<?php

namespace App\Models\AreaDoCliente;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'ac_empresas';

    protected $guarded = ['id'];

    public function gerentes()
    {
        return $this->hasMany('App\Models\AreaDoCliente\Gerente', 'empresa_id');
    }
}
