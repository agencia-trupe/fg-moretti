<?php

namespace App\Models\AreaDoCliente;

use Illuminate\Database\Eloquent\Model;

class ObraComunicacao extends Model
{
    protected $table = 'ac_obras_comunicacoes';

    protected $guarded = ['id'];

    public function scopeGerente($query, $id)
    {
        return $query->where('gerente_id', $id);
    }

    public function scopeObra($query, $id)
    {
        return $query->where('obra_id', $id);
    }

    public function scopeUsuario($query, $id)
    {
        return $query->where('usuario_id', $id);
    }

    public function scopeAdmin($query, $id)
    {
        return $query->where('admin_id', $id);
    }
}
