<?php

namespace App\Models\AreaDoCliente;

use Illuminate\Database\Eloquent\Model;

class GerenteObra extends Model
{
    protected $table = 'ac_gerentes_obras';

    protected $guarded = ['id'];

    public function scopeGerente($query, $id)
    {
        return $query->where('usuario_id', $id);
    }

    public function scopeObra($query, $id)
    {
        return $query->where('obra_id', $id);
    }
}
