<?php

namespace App\Models\AreaDoCliente;

use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
    protected $table = 'ac_documentos';

    protected $guarded = ['id'];

    public function scopeObra($query, $id)
    {
        return $query->where('obra_id', $id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function versoes()
    {
        return $this->hasMany('App\Models\AreaDoCliente\DocumentoVersao', 'documento_id');
    }
}
