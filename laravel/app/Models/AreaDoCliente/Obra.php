<?php

namespace App\Models\AreaDoCliente;

use Illuminate\Database\Eloquent\Model;

class Obra extends Model
{
    protected $table = 'ac_obras';

    protected $guarded = ['id'];

    public function documentos()
    {
        return $this->hasMany('App\Models\AreaDoCliente\Documento', 'obra_id');
    }

    public function gerentes()
    {
        return $this->hasMany('App\Models\AreaDoCliente\GerenteObra', 'obra_id');
    }
}
