<?php

namespace App\Models\AreaDoCliente;

use Illuminate\Database\Eloquent\Model;

class DocumentoVersao extends Model
{
    protected $table = 'ac_documentos_versoes';

    protected $guarded = ['id'];

    public function scopeDocumento($query, $id)
    {
        return $query->where('documento_id', $id);
    }
}
