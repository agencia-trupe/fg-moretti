<?php

namespace App\Models\AreaDoCliente;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class ClienteUsuario extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    protected $table = 'ac_clientes_usuarios';

    protected $guard = 'clientes';

    protected $guarded = ['id'];

    protected $fillable = ['empresa_id', 'slug', 'nome', 'telefone', 'email', 'password', 'gerente'];

    protected $hidden = ['password', 'remember_token'];

    public function scopeAtivos($query)
    {
        return $query->where('ativo', '=', 1);
    }

    public function scopeGerente($query)
    {
        return $query->where('gerente', '=', 1);
    }

    public function scopeEmpresa($query, $id)
    {
        return $query->where('empresa_id', $id);
    }

    public function obras()
    {
        return $this->hasMany('App\Models\AreaDoCliente\GerenteObra', 'gerente_id');
    }
}
