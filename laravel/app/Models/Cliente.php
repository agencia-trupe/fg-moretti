<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'clientes';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 300,
            'height' => null,
            'path'   => 'assets/img/clientes/'
        ]);
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('nome', 'LIKE', '%' . $termo . '%');
    }
}
