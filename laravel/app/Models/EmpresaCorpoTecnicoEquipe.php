<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class EmpresaCorpoTecnicoEquipe extends Model
{
    protected $table = 'empresa_corpo_tecnico_equipe';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeCorpoTecnico($query, $id)
    {
        return $query->where('corpo_tecnico_id', $id);
    }

    public static function upload_foto()
    {
        return CropImage::make('foto', [
            'width'  => 200,
            'height' => 200,
            'path'   => 'assets/img/corpo-tecnico-equipe/'
        ]);
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('nome', 'LIKE', '%' . $termo . '%')
            ->orWhere('texto_sobre', 'LIKE', '%' . $termo . '%');
    }
}
