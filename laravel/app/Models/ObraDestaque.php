<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ObraDestaque extends Model
{
    protected $table = 'obras_destaque';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
