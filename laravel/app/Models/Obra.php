<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Obra extends Model
{
    protected $table = 'obras';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeAplicacao($query, $id)
    {
        return $query->where('aplicacao_id', $id);
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 600,
            'height' => null,
            'path'   => 'assets/img/obras/'
        ]);
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo', 'LIKE', '%' . $termo . '%')
            ->orWhere('texto', 'LIKE', '%' . $termo . '%');
    }
}
