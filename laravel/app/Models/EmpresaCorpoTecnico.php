<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmpresaCorpoTecnico extends Model
{
    protected $table = 'empresa_corpo_tecnico';

    protected $guarded = ['id'];

    public function scopeBusca($query, $termo)
    {
        return $query->where('texto', 'LIKE', '%' . $termo . '%');
    }
}
