<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmpresaHistoria extends Model
{
    protected $table = 'empresa_historia';

    protected $guarded = ['id'];

    public function scopeBusca($query, $termo)
    {
        return $query->where('texto', 'LIKE', '%' . $termo . '%');
    }
}
