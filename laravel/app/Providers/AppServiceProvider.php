<?php

namespace App\Providers;

use App\Models\Aplicacao;
use App\Models\AreaDeNegocio;
use App\Models\CapaMenu;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*', function ($view) {
            $view->with('config', \App\Models\Configuracao::first());
        });

        view()->composer('frontend.common.*', function ($view) {
            $view->with('contato', \App\Models\Contato::first());
        });

        view()->composer('frontend.common.header', function ($view) {
            $url = explode('/', $_SERVER['REQUEST_URI']);
            $menu = CapaMenu::where('slug', $url[1])->get(); // local ou final
            // $menu = CapaMenu::where('slug', $url[2])->get(); // com url prévia
            $view->with('menu', $menu);
        });

        view()->composer('frontend.common.*', function ($view) {
            $area1 = AreaDeNegocio::first();
            $view->with('area1', $area1);

            $aplicacao1 = Aplicacao::first();
            $view->with('aplicacao1', $aplicacao1);
        });

        view()->composer('frontend.common.footer*', function ($view) {
            $view->with('areas', \App\Models\AreaDeNegocio::orderBy('id', 'ASC')->get());
            $view->with('aplicacoes', \App\Models\Aplicacao::orderBy('titulo', 'ASC')->get());
            $view->with('grupos', \App\Models\ServicosGrupo::orderBy('titulo', 'ASC')->get());
        });

        view()->composer('painel.common.*', function ($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
        });

        view()->composer('frontend.filtro*', function ($view) {
            $view->with('areas', \App\Models\AreaDeNegocio::orderBy('id', 'ASC')->get());
            $view->with('aplicacoes', \App\Models\Aplicacao::orderBy('titulo', 'ASC')->get());
            $view->with('servicos', \App\Models\Servico::orderBy('titulo', 'ASC')->get());
        });
    }

    public function register()
    {
        //
    }
}
