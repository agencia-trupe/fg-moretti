<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Http\Controllers';

    public function boot(Router $router)
    {
        $router->model('home', 'App\Models\Home');
        $router->model('capas_menus', 'App\Models\CapaMenu');
        $router->model('banners_header', 'App\Models\BannerHeader');
        $router->model('slides_home', 'App\Models\SlideHome');
        $router->model('obras_destaque', 'App\Models\ObraDestaque');
        $router->model('empresa_historia', 'App\Models\EmpresaHistoria');
        $router->model('empresa_nossa_otica', 'App\Models\EmpresaNossaOtica');
        $router->model('empresa_corpo_tecnico', 'App\Models\EmpresaCorpoTecnico');
        $router->model('empresa_corpo_tecnico_equipe', 'App\Models\EmpresaCorpoTecnicoEquipe');
        $router->model('areas_de_negocio', 'App\Models\AreaDeNegocio');
        $router->model('servicos_pagina', 'App\Models\ServicosPagina');
        $router->model('servicos_grupos', 'App\Models\ServicosGrupo');
        $router->model('servicos', 'App\Models\Servico');
        $router->model('servicos_imagens', 'App\Models\ServicoImagem');
        $router->model('aplicacoes', 'App\Models\Aplicacao');
        $router->model('aplicacoes_areas', 'App\Models\AplicacaoArea');
        $router->model('aplicacoes_areas_servicos', 'App\Models\AplicacaoAreaServico');
        $router->model('obras', 'App\Models\Obra');
        $router->model('obras_areas', 'App\Models\ObraArea');
        $router->model('obras_servicos', 'App\Models\ObraServico');
        $router->model('obras_imagens', 'App\Models\ObraImagem');
        $router->model('artigos_tecnicos', 'App\Models\ArtigoTecnico');
        $router->model('clientes', 'App\Models\Cliente');
        $router->model('contatos_recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('configuracoes', 'App\Models\Configuracao');
        $router->model('usuarios', 'App\Models\User');
        $router->model('termos-de-uso', 'App\Models\TermosDeUso');
        $router->model('politica-de-privacidade', 'App\Models\PoliticaDePrivacidade');
        $router->model('aceite_de_cookies', 'App\Models\AceiteDeCookies');

        parent::boot($router);
    }

    public function map(Router $router)
    {
        $router->group([
            'namespace'  => $this->namespace,
            'middleware' => ['web']
        ], function ($router) {
            require app_path('Http/routes.php');
            require app_path('Http/routesAdmin.php');
            require app_path('Http/routesClientes.php');
        });
    }
}
