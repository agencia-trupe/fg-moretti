<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClientesUsuariosAuthenticate
{
    public function handle(Request $request, Closure $next, $guard = 'clientes')
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('area-do-cliente/login');
            }
        }

        return $next($request);
    }
}
