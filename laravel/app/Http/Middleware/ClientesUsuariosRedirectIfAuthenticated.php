<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ClientesUsuariosRedirectIfAuthenticated
{
    public function handle($request, Closure $next, $guard = 'clientes')
    {
        if (Auth::guard($guard)->check()) {
            return redirect()->route('area-do-cliente');
        }

        return $next($request);
    }
}
