<?php

use Illuminate\Support\Facades\Route;

// Auth - ÁREA DO CLIENTE
Route::group([
    'prefix' => 'area-do-cliente',
    'namespace' => 'AreaDoCliente\Auth',
], function () {
    Route::get('login', 'AuthController@showLoginForm')->name('area-do-cliente.login');
    Route::post('login', 'AuthController@login')->name('area-do-cliente.login.post');
    Route::get('logout', 'AuthController@logout')->name('area-do-cliente.logout');
    Route::get('esqueci-minha-senha', 'PasswordController@showLinkRequestForm')->name('area-do-cliente.password.linkRequest');
    Route::post('esqueci-minha-senha', 'PasswordController@sendResetLinkEmail')->name('area-do-cliente.password.sendResetLink');
    Route::get('redefinir-senha/{token}', 'PasswordController@showResetForm')->name('area-do-cliente.password.resetForm');
    Route::post('redefinir-senha', 'PasswordController@reset')->name('area-do-cliente.password.reset');
    // SENHAS GERENTES
    Route::get('redefinir-senha/{email}/{token}/ativar-cadastro', 'SenhaController@edit')->name('area-do-cliente.alterar-senha');
    Route::post('redefinir-senha/{email}/{token}/ativar-cadastro', 'SenhaController@update')->name('area-do-cliente.alterar-senha.post');
    // SENHAS USUARIOS
    Route::get('cadastro/{email}/{token}/ativar-cadastro', 'SenhaController@editUsuario')->name('area-do-cliente.usuarios.cadastro');
    Route::post('cadastro/{email}/{token}/ativar-cadastro', 'SenhaController@updateUsuario')->name('area-do-cliente.usuarios.cadastro.post');
});

// ÁREA DO CLIENTE
Route::group([
    'prefix'     => 'area-do-cliente',
    'namespace'  => 'AreaDoCliente',
    'middleware' => ['clientes.auth']
], function () {
    Route::get('/', function () {
        return redirect()->route('area-do-cliente.clientes.identificacao');
    })->name('area-do-cliente');
    // // IDENTIFICAÇÃO
    Route::get('identificacao', 'IdentificacaoController@index')->name('area-do-cliente.clientes.identificacao');
    Route::post('identificacao/adicionar', 'IdentificacaoController@store')->name('area-do-cliente.clientes.adicionar-usuario');
    Route::get('identificacao/list/{obra}/{gerente}', 'IdentificacaoController@getUsuariosObra')->name('getUsuariosObra');
    Route::get('identificacao/excluir/{gerente}/{obra}/{usuario}', 'IdentificacaoController@destroy')->name('area-do-cliente.clientes.excluir-usuario');
    Route::post('identificacao/{usuario}/editar', 'IdentificacaoController@update')->name('area-do-cliente.clientes.update');
    // // DOCUMENTOS
    Route::get('documentos', 'DocumentosController@index')->name('area-do-cliente.clientes.documentos');
    Route::get('documentos/{obra}', 'DocumentosController@showObraDocs')->name('area-do-cliente.clientes.documentos.obra');
    Route::get('documentos/{documento}/download', 'DocumentosController@download')->name('area-do-cliente.clientes.documentos.download');
    Route::get('documentos/{obra}/{documento}/versoes', 'DocumentosController@showObraDocsVersoes')->name('area-do-cliente.clientes.documentos.obra.versoes');
    Route::get('documentos/{obra}/comunicacao', 'DocumentosController@showComunicacaoObra')->name('area-do-cliente.clientes.obra.comunicacao');
    Route::post('documentos/{obra}/comunicacao', 'DocumentosController@postComunicacaoObra')->name('area-do-cliente.clientes.obra.comunicacao.post');
    Route::get('documentos/{obra}/{comunicacao}/excluir', 'DocumentosController@destroy')->name('area-do-cliente.clientes.obra.comunicacao.destroy');
    Route::get('busca', 'DocumentosController@buscar')->name('area-do-cliente.busca');
});
