<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('resultados-filtro/{area}/{aplicacao}/{servico}', 'FiltroController@index')->name('filtro');
    Route::get('filtro/{area}/{aplicacao}/{servico}', 'FiltroController@filtroGeral')->name('filtroGeral');
    Route::get('empresa/{submenu}', 'EmpresaController@index')->name('empresa');
    Route::get('empresa/list/{submenu}', 'EmpresaController@getDadosEmpresa')->name('getDadosEmpresa');
    Route::get('areas-de-negocio/{area}', 'AreasDeNegocioController@index')->name('areas-de-negocio');
    Route::get('areas-de-negocio/list/{area}', 'AreasDeNegocioController@getDadosAreasDeNegocio')->name('getDadosAreasDeNegocio');
    Route::get('aplicacoes/{aplicacao}', 'AplicacoesController@index')->name('aplicacoes');
    Route::get('aplicacoes/list/{aplicacao}', 'AplicacoesController@getDadosAplicacoes')->name('getDadosAplicacoes');
    Route::get('servicos', 'ServicosController@index')->name('servicos');
    Route::get('servicos/{servico}', 'ServicosController@show')->name('servicos.show');
    Route::get('servicos/grupo/{grupo}', 'ServicosController@showServicosGrupo')->name('showServicosGrupo');
    Route::get('servicos/list/{servico}', 'ServicosController@getDadosServicos')->name('getDadosServicos');
    Route::get('galeria-de-imagens', 'GaleriaDeImagensController@index')->name('galeria-de-imagens.index');
    Route::get('galeria-de-imagens/{obra}', 'GaleriaDeImagensController@show')->name('galeria-de-imagens.show');
    Route::get('galeria-de-imagens/filtro/{area}/{aplicacao}/{servico}', 'GaleriaDeImagensController@filtroGaleria')->name('filtroGaleria');
    Route::get('artigos-tecnicos', 'ArtigosTecnicosController@index')->name('artigos-tecnicos');
    Route::get('artigos-tecnicos/{artigo}', 'ArtigosTecnicosController@openPdf')->name('artigos-tecnicos.pdf');
    Route::get('clientes', 'ClientesController@index')->name('clientes');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::post('busca', 'BuscaController@post')->name('busca');
    Route::get('termos-de-uso', 'TermosDeUsoController@index')->name('termos-de-uso');
    Route::get('politica-de-privacidade', 'PoliticaDePrivacidadeController@index')->name('politica-de-privacidade');
    Route::post('aceite-de-cookies', 'HomeController@postCookies')->name('aceite-de-cookies.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function () {
        Route::get('/', 'PainelController@index')->name('painel');
        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
        Route::resource('home', 'HomeController', ['only' => ['index', 'update']]);
        Route::resource('capas-menus', 'CapasMenusController', ['only' => ['index', 'edit', 'update']]);
        Route::resource('banners-header', 'BannersHeaderController');
        Route::resource('slides-home', 'SlidesHomeController');
        Route::resource('obras-destaque', 'ObrasDestaqueController', ['only' => ['index', 'create', 'store', 'destroy']]);
        Route::resource('empresa-historia', 'EmpresaHistoriaController', ['only' => ['index', 'update']]);
        Route::resource('empresa-nossa-otica', 'EmpresaNossaOticaController', ['only' => ['index', 'update']]);
        Route::resource('empresa-corpo-tecnico', 'EmpresaCorpoTecnicoController', ['only' => ['index', 'edit', 'update']]);
        Route::resource('empresa-corpo-tecnico.equipe', 'EmpresaCorpoTecnicoEquipeController');
        Route::resource('areas-de-negocio', 'AreasDeNegocioController', ['only' => ['index', 'edit', 'update']]);
        Route::resource('servicos-pagina', 'ServicosPaginaController', ['only' => ['index', 'update']]);
        Route::resource('servicos-grupos', 'ServicosGruposController');
        Route::resource('servicos', 'ServicosController', ['except' => 'show']);
        Route::get('servicos/{servico}/imagens/clear', [
            'as'   => 'painel.servicos.imagens.clear',
            'uses' => 'ServicosImagensController@clear'
        ]);
        Route::resource('servicos.imagens', 'ServicosImagensController', ['parameters' => ['imagens' => 'servicos_imagens']]);
        Route::resource('aplicacoes', 'AplicacoesController');
        Route::resource('aplicacoes.areas', 'AplicacoesAreasController');
        Route::resource('aplicacoes.areas.servicos', 'AplicacoesAreasServicosController', ['only' => ['index', 'create', 'store', 'destroy']]);
        Route::resource('galeria-de-imagens', 'ObrasController');
        Route::resource('galeria-de-imagens.areas', 'ObrasAreasController', ['only' => ['index', 'create', 'store', 'destroy']]);
        Route::resource('galeria-de-imagens.servicos', 'ObrasServicosController', ['only' => ['index', 'create', 'store']]);
        Route::delete('galeria-de-imagens/{obra}/servicos/{servico}', 'ObrasServicosController@destroy')->name('galeria.servicos.destroy');
        Route::resource('galeria-de-imagens.imagens', 'ObrasImagensController');
        Route::resource('artigos-tecnicos', 'ArtigosTecnicosController');
        Route::resource('clientes', 'ClientesController');
        Route::get('contato/recebidos/{recebidos}/pdf', 'ContatosRecebidosController@openPdf')->name('painel.contato.arquivos.pdf');
        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
        Route::resource('usuarios', 'UsuariosController');
        Route::resource('termos-de-uso', 'TermosDeUsoController', ['only' => ['index', 'update']]);
        Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
        Route::get('aceite-de-cookies', 'AceiteDeCookiesController@index')->name('painel.aceite-de-cookies');

        // Limpar caches
        Route::get('clear-cache', function () {
            $exitCode = Artisan::call('config:clear');
            $exitCode = Artisan::call('cache:clear');
            $exitCode = Artisan::call('route:clear');
            $exitCode = Artisan::call('view:clear');
            $exitCode = Artisan::call('config:cache');

            return 'DONE';
        });
    });

    // Auth - PAINEL
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function () {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
        Route::get('esqueci-minha-senha', 'PasswordController@sendEmail')->name('esqueci-minha-senha');
        Route::post('esqueci-minha-senha', 'PasswordController@sendResetLinkEmail')->name('esqueci-minha-senha.post');
        Route::get('reset/{token}', 'PasswordController@showResetForm')->name('password-reset');
        Route::post('reset', 'PasswordController@reset')->name('password-reset.post');
    });
});
