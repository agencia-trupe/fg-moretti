<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ArtigosTecnicosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return  [
            'titulo'       => 'required',
            // 'arquivo'      => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => trans('frontend.request-required'),
            'image'    => trans('frontend.request-image'),
        ];
    }
}
