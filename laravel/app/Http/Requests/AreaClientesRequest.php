<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AreaClientesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'empresa'  => 'required',
            'nome'     => 'required',
            'telefone' => 'required',
            'email'    => 'required|email|max:255|unique:area_clientes',
            'senha'    => 'required|confirmed',
        ];

        if ($this->method() != 'POST') {
            $rules['email'] = 'required|email,' . auth('cliente')->user()->id;
            $rules['senha'] = 'confirmed';
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'required'  => "Preencha todos os campos corretamente.",
            'email'     => "Preencha o campo com um e-mail válido.",
            'confirmed' => "A senha e a confirmação devem ser a mesma.",
        ];
    }
}
