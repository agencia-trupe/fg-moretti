<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ObrasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'texto'  => 'required',
            'capa'   => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
