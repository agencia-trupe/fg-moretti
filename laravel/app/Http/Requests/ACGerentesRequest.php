<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ACGerentesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'     => 'required',
            'telefone' => 'required',
            'email'    => 'required|email|max:255',
            'password' => 'confirmed|min:6',
        ];
    }

    public function messages()
    {
        return [
            'required'  => "Preencha todos os campos corretamente.",
            'unique'    => "Já existe um cadastro com este e-mail.",
            'email'     => "Insira um e-mail válido.",
            'confirmed' => "As senhas devem ser iguais.",
            'min'       => "A senha deve ter no mínimo 6 caracteres.",
        ];
    }
}
