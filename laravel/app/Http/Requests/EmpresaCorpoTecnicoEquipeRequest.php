<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EmpresaCorpoTecnicoEquipeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome'        => 'required',
            'texto_sobre' => 'required',
            'foto'        => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['foto'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
