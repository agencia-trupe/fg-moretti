<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AreasDeNegocioRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo'         => 'required',
            'texto_inicial'  => 'required',
            'imagem'         => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
        ];
    }
}
