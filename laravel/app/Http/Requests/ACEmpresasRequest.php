<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ACEmpresasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'cnpj' => 'required|min:14',
            'nome' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
            'unique'   => "CNPJ já possui cadastro.",
            'min'      => "O CNPJ deve ter 14 números."
        ];
    }
}
