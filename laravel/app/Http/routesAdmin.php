<?php

use Illuminate\Support\Facades\Route;

// Auth - ÁREA DO CLIENTE
Route::group([
    'prefix' => 'area-do-cliente',
    'namespace' => 'AreaDoCliente\Admin\Auth',
], function () {
    Route::get('admin/login', 'AuthController@showLoginForm')->name('admin.login');
    Route::post('admin/login', 'AuthController@login')->name('admin.login');
    Route::get('admin/logout', 'AuthController@logout')->name('admin.logout');
    Route::get('admin/esqueci-minha-senha', 'PasswordController@showLinkRequestForm')->name('admin.password.linkRequest');
    Route::post('admin/esqueci-minha-senha', 'PasswordController@sendResetLinkEmail')->name('admin.password.sendResetLink');
    Route::get('admin/redefinir-senha/{token}', 'PasswordController@showResetForm')->name('admin.password.resetForm');
    Route::post('admin/redefinir-senha', 'PasswordController@reset')->name('admin.password.reset');
    // SENHAS
    Route::get('admin/redefinir-senha/{email}/{token}/ativar-cadastro', 'SenhaController@edit')->name('admin.alterar-senha');
    Route::post('admin/redefinir-senha/{email}/{token}/ativar-cadastro', 'SenhaController@update')->name('admin.alterar-senha.post');
});

// ÁREA DO CLIENTE - ADMIN
Route::group([
    'prefix'     => 'area-do-cliente/admin',
    'namespace'  => 'AreaDoCliente\Admin',
    'middleware' => ['admin.auth']
], function () {
    Route::get('/', function () {
        return view('area-do-cliente.admin.index');
    })->name('area-do-cliente.admin');
    // OBRAS
    Route::get('obras', 'ObrasController@index')->name('area-do-cliente.obras');
    Route::get('obras/adicionar', 'ObrasController@create')->name('area-do-cliente.obras.create');
    Route::post('obras/adicionar', 'ObrasController@store')->name('area-do-cliente.obras.store');
    Route::get('obras/{obra}/editar', 'ObrasController@edit')->name('area-do-cliente.obras.edit');
    Route::post('obras/{obra}/editar', 'ObrasController@update')->name('area-do-cliente.obras.update');
    Route::get('obras/{obra}/excluir', 'ObrasController@destroy')->name('area-do-cliente.obras.destroy');
    // OBRAS - COMUNICAÇÕES
    Route::get('obras/{obra}/comunicacao', 'ObrasComunicacoesController@index')->name('area-do-cliente.obras.comunicacao');
    Route::post('obras/{obra}/comunicacao', 'ObrasComunicacoesController@post')->name('area-do-cliente.obras.comunicacao.post');
    Route::get('obras/{obra}/{comunicacao}/excluir', 'ObrasComunicacoesController@destroy')->name('area-do-cliente.obras.comunicacao.destroy');
    // DOCUMENTOS
    Route::get('{obra}/documentos', 'DocumentosController@index')->name('area-do-cliente.documentos');
    Route::get('{obra}/documentos/adicionar', 'DocumentosController@create')->name('area-do-cliente.documentos.create');
    Route::post('{obra}/documentos/adicionar', 'DocumentosController@store')->name('area-do-cliente.documentos.store');
    Route::get('{obra}/documentos/{documento}/editar', 'DocumentosController@edit')->name('area-do-cliente.documentos.edit');
    Route::post('{obra}/documentos/{documento}/editar', 'DocumentosController@update')->name('area-do-cliente.documentos.update');
    Route::get('{obra}/documentos/{documento}/excluir', 'DocumentosController@destroy')->name('area-do-cliente.documentos.destroy');
    Route::post('order', 'DocumentosController@order')->name('documentos.order');
    // VERSOES
    Route::get('{obra}/{documento}/versoes', 'DocumentosVersoesController@index')->name('area-do-cliente.versoes');
    Route::get('{obra}/{documento}/versoes/adicionar', 'DocumentosVersoesController@create')->name('area-do-cliente.versoes.create');
    Route::post('{obra}/{documento}/versoes/adicionar', 'DocumentosVersoesController@store')->name('area-do-cliente.versoes.store');
    Route::get('{obra}/{documento}/versoes/{versao}/editar', 'DocumentosVersoesController@edit')->name('area-do-cliente.versoes.edit');
    Route::post('{obra}/{documento}/versoes/{versao}/editar', 'DocumentosVersoesController@update')->name('area-do-cliente.versoes.update');
    Route::get('{obra}/{documento}/versoes/{versao}/excluir', 'DocumentosVersoesController@destroy')->name('area-do-cliente.versoes.destroy');
    Route::get('{obra}/{documento}/{versao}/arquivo', 'DocumentosVersoesController@openPDF')->name('area-do-cliente.versoes.arquivo');
    // CLIENTES/EMPRESAS
    Route::get('empresas', 'EmpresasController@index')->name('area-do-cliente.empresas');
    Route::get('empresas/adicionar', 'EmpresasController@create')->name('area-do-cliente.empresas.create');
    Route::post('empresas/adicionar', 'EmpresasController@store')->name('area-do-cliente.empresas.store');
    Route::get('empresas/{empresa}/editar', 'EmpresasController@edit')->name('area-do-cliente.empresas.edit');
    Route::post('empresas/{empresa}/editar', 'EmpresasController@update')->name('area-do-cliente.empresas.update');
    Route::get('empresas/{empresa}/excluir', 'EmpresasController@destroy')->name('area-do-cliente.empresas.destroy');
    // GERENTES
    Route::get('{empresa}/gerentes', 'GerentesController@index')->name('area-do-cliente.gerentes');
    Route::get('{empresa}/gerentes/adicionar', 'GerentesController@create')->name('area-do-cliente.gerentes.create');
    Route::post('{empresa}/gerentes/adicionar', 'GerentesController@store')->name('area-do-cliente.gerentes.store');
    Route::get('{empresa}/gerentes/{gerente}/editar', 'GerentesController@edit')->name('area-do-cliente.gerentes.edit');
    Route::post('{empresa}/gerentes/{gerente}/editar', 'GerentesController@update')->name('area-do-cliente.gerentes.update');
    Route::get('{empresa}/gerentes/{gerente}/excluir', 'GerentesController@destroy')->name('area-do-cliente.gerentes.destroy');
    // GERENTES-USUÁRIOS - somente listar e excluir
    Route::get('{empresa}/{gerente}/usuarios', 'GerentesUsuariosController@index')->name('area-do-cliente.gerentes.usuarios');
    Route::get('{empresa}/{gerente}/usuarios/{usuario}/excluir', 'GerentesUsuariosController@destroy')->name('area-do-cliente.gerentes.usuarios.destroy');
    // GERENTES-OBRAS
    Route::get('{empresa}/{gerente}/obras', 'GerentesObrasController@index')->name('area-do-cliente.gerentes.obras');
    Route::get('{empresa}/{gerente}/obras/adicionar', 'GerentesObrasController@create')->name('area-do-cliente.gerentes.obras.create');
    Route::post('{empresa}/{gerente}/obras/adicionar', 'GerentesObrasController@store')->name('area-do-cliente.gerentes.obras.store');
    Route::get('{empresa}/{gerente}/obras/{obra}/excluir', 'GerentesObrasController@destroy')->name('area-do-cliente.gerentes.obras.destroy');
    // USUÁRIOS FG MORETTI
    Route::get('usuarios', 'UsuariosController@index')->name('area-do-cliente.usuarios');
    Route::get('usuarios/adicionar', 'UsuariosController@create')->name('area-do-cliente.usuarios.create');
    Route::post('usuarios/adicionar', 'UsuariosController@store')->name('area-do-cliente.usuarios.store');
    Route::get('usuarios/{usuario}/editar', 'UsuariosController@edit')->name('area-do-cliente.usuarios.edit');
    Route::post('usuarios/{usuario}/editar', 'UsuariosController@update')->name('area-do-cliente.usuarios.update');
    Route::get('usuarios/{usuario}/excluir', 'UsuariosController@destroy')->name('area-do-cliente.usuarios.destroy');
});
