<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Aplicacao;
use App\Models\AplicacaoArea;
use App\Models\AreaDeNegocio;
use App\Models\ArtigoTecnico;
use App\Models\Cliente;
use App\Models\EmpresaCorpoTecnico;
use App\Models\EmpresaCorpoTecnicoEquipe;
use App\Models\EmpresaHistoria;
use App\Models\EmpresaNossaOtica;
use App\Models\Home;
use App\Models\Obra;
use App\Models\PoliticaDePrivacidade;
use App\Models\Servico;
use App\Models\ServicosGrupo;
use App\Models\TermosDeUso;

class BuscaController extends Controller
{
    public function destacar_palavras($conteudo, $palavras)
    {
        $palavrasBusca = explode(" ", $palavras);
        // loop de palavras buscadas (se tiver mais de 1)
        foreach ($palavrasBusca as $palavra) {
            // dd($palavra);
            $replace = '<span style="font-weight: bold;">' . $palavra . '</span>'; // inclui palavra buscada dentro do span
            $conteudo = str_ireplace($palavra, $replace, $conteudo); // replace no conteudo
        }

        return $conteudo; // retorna conteudo com as palavras em destaque
    }

    public function post(Request $request)
    {
        $termo = $request->busca;
        $resultados = [];

        if (!empty($termo)) {

            $busca_aplicacoes = Aplicacao::busca($termo)->get();
            foreach ($busca_aplicacoes as $dados) {
                $aplicacao_area = AplicacaoArea::where('aplicacao_id', $dados->id)->first();
                $resultados['aplicacoes'][$dados->id]['link']   = route('aplicacoes', $dados->slug);
                $resultados['aplicacoes'][$dados->id]['secao']  = "aplicações";
                $resultados['aplicacoes'][$dados->id]['id']     = $dados->id;
                $resultados['aplicacoes'][$dados->id]['titulo'] = $this->destacar_palavras($dados->titulo, $termo);
                $resultados['aplicacoes'][$dados->id]['texto']  = $this->destacar_palavras($dados->texto . $aplicacao_area->texto_area, $termo);
            }

            $busca_areas = AreaDeNegocio::busca($termo)->get();
            foreach ($busca_areas as $dados) {
                $resultados['areas-de-negocio'][$dados->id]['link']   = route('areas-de-negocio', $dados->slug);
                $resultados['areas-de-negocio'][$dados->id]['secao']  = "áreas de negócio";
                $resultados['areas-de-negocio'][$dados->id]['id']     = $dados->id;
                $resultados['areas-de-negocio'][$dados->id]['titulo'] = $this->destacar_palavras($dados->titulo, $termo);
                $resultados['areas-de-negocio'][$dados->id]['texto']  = $this->destacar_palavras($dados->texto_inicial . $dados->texto_servicos, $termo);
            }

            $busca_artigos = ArtigoTecnico::busca($termo)->get();
            foreach ($busca_artigos as $dados) {
                $resultados['artigos-tecnicos'][$dados->id]['link']   = route('artigos-tecnicos') . "#artigo" . $dados->id;
                $resultados['artigos-tecnicos'][$dados->id]['secao']  = "artigos técnicos";
                $resultados['artigos-tecnicos'][$dados->id]['titulo'] = $this->destacar_palavras($dados->titulo, $termo);
                $resultados['artigos-tecnicos'][$dados->id]['texto']  = $this->destacar_palavras("Arquivo: " . $dados->arquivo . " • Clique e saiba mais...", $termo);
            }

            $busca_clientes = Cliente::busca($termo)->get();
            foreach ($busca_clientes as $dados) {
                $resultados['clientes'][$dados->id]['link']   = route('clientes') . "#cliente" . $dados->id;
                $resultados['clientes'][$dados->id]['secao']  = "clientes";
                $resultados['clientes'][$dados->id]['titulo'] = $this->destacar_palavras($dados->nome, $termo);
                $resultados['clientes'][$dados->id]['texto']  = $this->destacar_palavras("Esse é um dos nossos clientes: " . $dados->nome . " • Clique e veja todos os nossos clientes...", $termo);
            }

            $busca_corpo_tecnico = EmpresaCorpoTecnico::busca($termo)->get();
            foreach ($busca_corpo_tecnico as $dados) {
                $resultados['corpo-tecnico'][$dados->id]['link']   = route('empresa', "corpo-tecnico");
                $resultados['corpo-tecnico'][$dados->id]['secao']  = "empresa";
                $resultados['corpo-tecnico'][$dados->id]['titulo'] = $this->destacar_palavras("Corpo Técnico", $termo);
                $resultados['corpo-tecnico'][$dados->id]['texto']  = $this->destacar_palavras($dados->texto, $termo);
            }

            $busca_equipe = EmpresaCorpoTecnicoEquipe::busca($termo)->get();
            foreach ($busca_equipe as $dados) {
                $resultados['corpo-tecnico'][$dados->id]['link']   = route('empresa', "corpo-tecnico");
                $resultados['corpo-tecnico'][$dados->id]['secao']  = "empresa - corpo técnico";
                $resultados['corpo-tecnico'][$dados->id]['titulo'] = $this->destacar_palavras($dados->nome, $termo);
                $resultados['corpo-tecnico'][$dados->id]['texto']  = $this->destacar_palavras($dados->texto_sobre, $termo);
            }

            $busca_historia = EmpresaHistoria::busca($termo)->get();
            foreach ($busca_historia as $dados) {
                $resultados['historia'][$dados->id]['link']   = route('empresa', "historia");
                $resultados['historia'][$dados->id]['secao']  = "empresa";
                $resultados['historia'][$dados->id]['titulo'] = $this->destacar_palavras("História", $termo);
                $resultados['historia'][$dados->id]['texto']  = $this->destacar_palavras($dados->texto, $termo);
            }

            $busca_nossa_otica = EmpresaNossaOtica::busca($termo)->get();
            foreach ($busca_nossa_otica as $dados) {
                $resultados['nossa-otica'][$dados->id]['link']   = route('empresa', "nossa-otica");
                $resultados['nossa-otica'][$dados->id]['secao']  = "empresa";
                $resultados['nossa-otica'][$dados->id]['titulo'] = $this->destacar_palavras("Nossa Ótica", $termo);
                $resultados['nossa-otica'][$dados->id]['texto']  = $this->destacar_palavras($dados->texto . $dados->visao . $dados->missao . $dados->valores, $termo);
            }

            $busca_home = Home::busca($termo)->get();
            foreach ($busca_home as $dados) {
                $resultados['home'][$dados->id]['link']   = route('home') . "#faixaTexto";
                $resultados['home'][$dados->id]['secao']  = "home";
                $resultados['home'][$dados->id]['titulo'] = $this->destacar_palavras($dados->titulo, $termo);
                $resultados['home'][$dados->id]['texto']  = $this->destacar_palavras($dados->texto, $termo);
            }

            $busca_obras = Obra::busca($termo)->get();
            foreach ($busca_obras as $dados) {
                $resultados['obras'][$dados->id]['link']   = route('galeria-de-imagens.show', $dados->slug);
                $resultados['obras'][$dados->id]['secao']  = "galeria de imagens";
                $resultados['obras'][$dados->id]['titulo'] = $this->destacar_palavras($dados->titulo, $termo);
                $resultados['obras'][$dados->id]['texto']  = $this->destacar_palavras($dados->texto, $termo);
            }

            $busca_servicos = Servico::busca($termo)->get();
            foreach ($busca_servicos as $dados) {
                $resultados['servicos'][$dados->id]['link']   = route('servicos.show', $dados->id);
                $resultados['servicos'][$dados->id]['secao']  = "servicos";
                $resultados['servicos'][$dados->id]['grupo']  = $dados->grupo_id;
                $resultados['servicos'][$dados->id]['id']     = $dados->id;
                $resultados['servicos'][$dados->id]['titulo'] = $this->destacar_palavras("Serviço: " . $dados->titulo, $termo);
                $resultados['servicos'][$dados->id]['texto']  = $this->destacar_palavras($dados->texto, $termo);
            }

            $busca_grupos_servicos = ServicosGrupo::busca($termo)->get();
            foreach ($busca_grupos_servicos as $dados) {
                $resultados['servicos'][$dados->id]['link']   = route('servicos');
                $resultados['servicos'][$dados->id]['secao']  = "servicos";
                $resultados['servicos'][$dados->id]['grupo']  = $dados->id;
                $resultados['servicos'][$dados->id]['id']     = null;
                $resultados['servicos'][$dados->id]['titulo'] = $this->destacar_palavras("Grupo: " . $dados->titulo, $termo);
                $resultados['servicos'][$dados->id]['texto']  = $this->destacar_palavras("Veja todos os serviços relacionados ao grupo que você procura. Clique e saiba mais...", $termo);
            }

            $busca_termos_de_uso = TermosDeUso::busca($termo)->get();
            foreach ($busca_termos_de_uso as $dados) {
                $resultados['termos-de-uso'][$dados->id]['link']   = route('termos-de-uso');
                $resultados['termos-de-uso'][$dados->id]['secao']  = "termos de uso do site";
                $resultados['termos-de-uso'][$dados->id]['titulo'] = $this->destacar_palavras("Leia nossos termos de uso", $termo);
                $resultados['termos-de-uso'][$dados->id]['texto']  = $this->destacar_palavras($dados->texto, $termo);
            }

            $busca_politica_de_privacidade = PoliticaDePrivacidade::busca($termo)->get();
            foreach ($busca_politica_de_privacidade as $dados) {
                $resultados['politica-de-privacidade'][$dados->id]['link']   = route('politica-de-privacidade');
                $resultados['politica-de-privacidade'][$dados->id]['secao']  = "política de privacidade";
                $resultados['politica-de-privacidade'][$dados->id]['titulo'] = $this->destacar_palavras("Leia nossa política de privacidade", $termo);
                $resultados['politica-de-privacidade'][$dados->id]['texto']  = $this->destacar_palavras($dados->texto, $termo);
            }
        } else {
            $resultados = [];
        }
        // dd($resultados);

        return view('frontend.busca', compact('resultados', 'termo'));
    }
}
