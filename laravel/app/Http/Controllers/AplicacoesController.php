<?php

namespace App\Http\Controllers;

use App\Models\Aplicacao;
use App\Models\AreaDeNegocio;
use App\Models\Servico;

class AplicacoesController extends Controller
{
    public function index($aplicacao)
    {
        $aplicacoes = Aplicacao::orderBy('titulo', 'ASC')->get();

        $aplicacaoInicial = $aplicacoes->where('slug', $aplicacao)->first();

        $aplicacoesAreas = AreaDeNegocio::join('aplicacoes_areas', 'aplicacoes_areas.area_id', '=', 'areas_de_negocio.id')
            ->join('aplicacoes', 'aplicacoes.id', '=', 'aplicacoes_areas.aplicacao_id')
            ->select('areas_de_negocio.id as area_id', 'areas_de_negocio.slug as area_slug', 'areas_de_negocio.titulo as area_titulo', 'aplicacoes_areas.texto_area as texto_area')
            ->where('aplicacoes.slug', $aplicacao)
            ->distinct()->orderBy('areas_de_negocio.id', 'ASC')->get();

        $aplicacoesServicos = Servico::join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.servico_id', '=', 'servicos.id')
            ->join('aplicacoes_areas', 'aplicacoes_areas.id', '=', 'aplicacoes_areas_servicos.aplicacao_area_id')
            ->join('aplicacoes', 'aplicacoes.id', '=', 'aplicacoes_areas.aplicacao_id')
            ->join('areas_de_negocio', 'areas_de_negocio.id', '=', 'aplicacoes_areas.area_id')
            ->select('aplicacoes.id as aplicacao_id', 'aplicacoes.slug as aplicacao_slug', 'aplicacoes.titulo as aplicacao_titulo', 'areas_de_negocio.id as area_id', 'areas_de_negocio.slug as area_slug', 'areas_de_negocio.titulo as area_titulo', 'servicos.id as servico_id', 'servicos.slug as servico_slug', 'servicos.titulo as servico_titulo', 'servicos.grupo_id as grupo_id')
            ->where('aplicacoes.id', $aplicacaoInicial->id)
            ->orderBy('servicos.titulo', 'ASC')->get();

        // dd($aplicacoesServicos);

        return view('frontend.aplicacoes', compact('aplicacoes', 'aplicacaoInicial', 'aplicacoesAreas', 'aplicacoesServicos'));
    }

    public function getDadosAplicacoes($aplicacao)
    {
        $aplicacoes = Aplicacao::where('slug', $aplicacao)->first();

        $aplicacoesAreas = AreaDeNegocio::join('aplicacoes_areas', 'aplicacoes_areas.area_id', '=', 'areas_de_negocio.id')
            ->join('aplicacoes', 'aplicacoes.id', '=', 'aplicacoes_areas.aplicacao_id')
            ->select('areas_de_negocio.id as area_id', 'areas_de_negocio.slug as area_slug', 'areas_de_negocio.titulo as area_titulo', 'aplicacoes_areas.texto_area as texto_area')
            ->where('aplicacoes.slug', $aplicacao)
            ->distinct()->orderBy('areas_de_negocio.id', 'ASC')->get();

        $aplicacoesServicos = Servico::join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.servico_id', '=', 'servicos.id')
            ->join('aplicacoes_areas', 'aplicacoes_areas.id', '=', 'aplicacoes_areas_servicos.aplicacao_area_id')
            ->join('aplicacoes', 'aplicacoes.id', '=', 'aplicacoes_areas.aplicacao_id')
            ->join('areas_de_negocio', 'areas_de_negocio.id', '=', 'aplicacoes_areas.area_id')
            ->select('aplicacoes.id as aplicacao_id', 'aplicacoes.slug as aplicacao_slug', 'aplicacoes.titulo as aplicacao_titulo', 'areas_de_negocio.id as area_id', 'areas_de_negocio.slug as area_slug', 'areas_de_negocio.titulo as area_titulo', 'servicos.id as servico_id', 'servicos.slug as servico_slug', 'servicos.titulo as servico_titulo', 'servicos.grupo_id as grupo_id')
            ->where('aplicacoes.id', $aplicacoes->id)
            ->orderBy('servicos.titulo', 'ASC')->get();

        return response()->json(['aplicacoes' => $aplicacoes, 'aplicacoesAreas' => $aplicacoesAreas, 'aplicacoesServicos' => $aplicacoesServicos]);
    }
}
