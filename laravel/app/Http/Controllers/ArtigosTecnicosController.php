<?php

namespace App\Http\Controllers;

use App\Models\ArtigoTecnico;

class ArtigosTecnicosController extends Controller
{
    public function index()
    {
        $artigos = ArtigoTecnico::ordenados()->get();

        return view('frontend.artigos-tecnicos', compact('artigos'));
    }

    public function openPdf($artigo)
    {
        $arquivo = ArtigoTecnico::where('slug', $artigo)->select('arquivo')->first();
        
        $arquivoPath = public_path() . "/assets/artigos/" . $arquivo->arquivo;

        return response()->file($arquivoPath);
    }
}
