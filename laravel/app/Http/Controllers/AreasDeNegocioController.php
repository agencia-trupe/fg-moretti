<?php

namespace App\Http\Controllers;

use App\Models\AreaDeNegocio;

class AreasDeNegocioController extends Controller
{
    public function index($area)
    {
        $areas = AreaDeNegocio::orderBy('id', 'ASC')->get();
        $areaInicial = $areas->where('slug', $area)->first();

        $aplicacoes = AreaDeNegocio::join('aplicacoes_areas', 'aplicacoes_areas.area_id', '=', 'areas_de_negocio.id')
            ->join('aplicacoes', 'aplicacoes.id', '=', 'aplicacoes_areas.aplicacao_id')
            ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
            ->where('areas_de_negocio.slug', $area)
            ->orderBy('aplicacoes.titulo', 'ASC')->get();
        
        $servicos = AreaDeNegocio::join('aplicacoes_areas', 'aplicacoes_areas.area_id', '=', 'areas_de_negocio.id')
            ->join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.aplicacao_area_id', '=', 'aplicacoes_areas.id')
            ->join('servicos', 'servicos.id', '=', 'aplicacoes_areas_servicos.servico_id')
            ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo', 'servicos.grupo_id as grupo_id')
            ->where('areas_de_negocio.slug', $area)
            ->distinct()->orderBy('servicos.titulo', 'ASC')->get();

        return view('frontend.areas-de-negocio', compact('areaInicial', 'areas', 'aplicacoes', 'servicos'));
    }

    public function getDadosAreasDeNegocio($area)
    {
        $areas = AreaDeNegocio::where('slug', $area)->first();

        $aplicacoes = AreaDeNegocio::join('aplicacoes_areas', 'aplicacoes_areas.area_id', '=', 'areas_de_negocio.id')
            ->join('aplicacoes', 'aplicacoes.id', '=', 'aplicacoes_areas.aplicacao_id')
            ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
            ->where('areas_de_negocio.slug', $area)
            ->orderBy('aplicacoes.titulo', 'ASC')->get();

        $servicos = AreaDeNegocio::join('aplicacoes_areas', 'aplicacoes_areas.area_id', '=', 'areas_de_negocio.id')
            ->join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.aplicacao_area_id', '=', 'aplicacoes_areas.id')
            ->join('servicos', 'servicos.id', '=', 'aplicacoes_areas_servicos.servico_id')
            ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo', 'servicos.grupo_id as grupo_id')
            ->where('areas_de_negocio.slug', $area)
            ->distinct()->orderBy('servicos.titulo', 'ASC')->get();

        return response()->json(['areas' => $areas, 'aplicacoes' => $aplicacoes, 'servicos' => $servicos]);
    }
}
