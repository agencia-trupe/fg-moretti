<?php

namespace App\Http\Controllers;

use App\Models\AceiteDeCookies;
use App\Models\AreaDeNegocio;
use App\Models\BannerHeader;
use App\Models\Home;
use App\Models\ObraDestaque;
use App\Models\SlideHome;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $banners = BannerHeader::ordenados()->get();

        $areas = AreaDeNegocio::orderBy('id', 'ASC')->get();

        $slides = SlideHome::ordenados()->get();

        $texto = Home::first();

        $destaques = ObraDestaque::join('obras', 'obras.id', '=', 'obras_destaque.obra_id')
            ->select('obras_destaque.id as id', 'obras_destaque.ordem as ordem', 'obras.id as obra_id', 'obras.titulo as obra_titulo', 'obras.slug as obra_slug', 'obras.capa as obra_capa')
            ->ordenados()->get();

        $verificacao = AceiteDeCookies::where('ip', $request->ip())->first();

        return view('frontend.home', compact('banners', 'areas', 'slides', 'texto', 'destaques', 'verificacao'));
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }
}
