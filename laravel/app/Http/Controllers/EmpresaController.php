<?php

namespace App\Http\Controllers;

use App\Models\EmpresaCorpoTecnico;
use App\Models\EmpresaCorpoTecnicoEquipe;
use App\Models\EmpresaHistoria;
use App\Models\EmpresaNossaOtica;

class EmpresaController extends Controller
{
    public function index()
    {
        $historia = EmpresaHistoria::first();

        return view('frontend.empresa', compact('historia'));
    }

    public function getDadosEmpresa($submenu)
    {
        switch ($submenu) {
            case "historia":
                $titulo = "HISTÓRIA";
                $imagem = "ico-historia.svg";
                $resultado = EmpresaHistoria::first();
                $equipe = "";
                break;
            case "nossa-otica":
                $titulo = "NOSSA ÓTICA";
                $imagem = "ico-otica.svg";
                $resultado = EmpresaNossaOtica::first();
                $equipe = "";
                break;
            case "corpo-tecnico":
                $titulo = "CORPO TÉCNICO";
                $imagem = "ico-corpotecnico.svg";
                $resultado = EmpresaCorpoTecnico::first();
                $equipe = EmpresaCorpoTecnicoEquipe::ordenados()->get();
                break;
        }

        return response()->json(['submenu' => $submenu, 'resultado' => $resultado, 'titulo' => $titulo, 'imagem' => $imagem, 'equipe' => $equipe]);
    }
}
