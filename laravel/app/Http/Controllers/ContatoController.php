<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContatosRecebidosRequest;
use App\Models\Contato;
use App\Models\ContatoRecebido;
use Illuminate\Support\Facades\Mail;

class ContatoController extends Controller
{
    public function removerAcentos()
    {
        $conversao = array(
            'á' => 'a', 'à' => 'a', 'ã' => 'a', 'â' => 'a', 'é' => 'e',
            'ê' => 'e', 'í' => 'i', 'ï' => 'i', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', "ö" => "o",
            'ú' => 'u', 'ü' => 'u', 'ç' => 'c', 'ñ' => 'n', 'Á' => 'A', 'À' => 'A', 'Ã' => 'A',
            'Â' => 'A', 'É' => 'E', 'Ê' => 'E', 'Í' => 'I', 'Ï' => 'I', "Ö" => "O", 'Ó' => 'O',
            'Ô' => 'O', 'Õ' => 'O', 'Ú' => 'U', 'Ü' => 'U', 'Ç' => 'C', 'N' => 'Ñ'
        );

        return $conversao;
    }
    
    public function index()
    {
        $contato = Contato::first();

        $assuntos = [
            "1" => "Solicitação", 
            "2" => "Dúvida", 
            "3" => "Orçamento", 
            "4" => "Reclamação", 
            "5" => "Sugestão"
        ];

        return view('frontend.contato', compact('contato', 'assuntos'));
    }

    public function post(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $assuntos = [
            "1" => "Solicitação", 
            "2" => "Dúvida", 
            "3" => "Orçamento", 
            "4" => "Reclamação", 
            "5" => "Sugestão"
        ];

        $data = $request->all();
        $data['assunto'] = $assuntos[$request->assunto];
        
        // dd($data);
        $file = $request->file('arquivo');
        // dd($file);

        if ($request->hasFile('arquivo')) {
            if ($request->file('arquivo')->isValid()) {
                $filename = strtoupper(str_replace(' ', '-', strtr($file->getClientOriginalName(), $this->removerAcentos())));
                $path = public_path() . '/assets/arquivos-contato';
                $file->move($path, $filename);
                $data['arquivo'] = $filename;
            }
        }

        $contatoRecebido->create($data);
        $this->sendMail($data);

        return redirect('contato')->with('enviado', true);
    }

    private function sendMail($data)
    {
        if (!$email = Contato::first()->email) {
            return false;
        }

        Mail::send('emails.contato', $data, function ($m) use ($email, $data) {
            $m->to($email, config('app.name'))
                ->subject('[CONTATO] ' . config('app.name'))
                ->replyTo($data['email'], $data['nome']);
        });
    }
}
