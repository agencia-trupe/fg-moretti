<?php

namespace App\Http\Controllers;

use App\Models\TermosDeUso;

class TermosDeUsoController extends Controller
{
    public function index()
    {
        $termos = TermosDeUso::first();

        return view('frontend.termos-de-uso', compact('termos'));
    }
}
