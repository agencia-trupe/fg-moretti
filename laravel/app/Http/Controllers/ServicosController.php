<?php

namespace App\Http\Controllers;

use App\Models\Aplicacao;
use App\Models\AreaDeNegocio;
use App\Models\Servico;
use App\Models\ServicoImagem;
use App\Models\ServicosGrupo;
use App\Models\ServicosPagina;

class ServicosController extends Controller
{
    public function index()
    {
        $textoServicos = ServicosPagina::first();

        $grupos = ServicosGrupo::orderBy('titulo', 'ASC')->get();

        return view('frontend.servicos', compact('textoServicos', 'grupos'));
    }

    public function show($servico)
    {
        $servicoShow = Servico::where('id', $servico)->first();

        $imagens = ServicoImagem::where('servico_id', $servico)->ordenados()->get();

        $grupos = ServicosGrupo::orderBy('titulo', 'ASC')->get();

        $aplicacoes = Aplicacao::join('aplicacoes_areas', 'aplicacoes_areas.aplicacao_id', '=', 'aplicacoes.id')
            ->join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.aplicacao_area_id', '=', 'aplicacoes_areas.id')
            ->select('aplicacoes_areas_servicos.servico_id', 'aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo', 'aplicacoes_areas.area_id as area_id')
            ->where('aplicacoes_areas_servicos.servico_id', $servico)
            ->orderBy('aplicacoes.titulo', 'ASC')->get();

        $areas = AreaDeNegocio::join('aplicacoes_areas', 'aplicacoes_areas.area_id', '=', 'areas_de_negocio.id')
            ->join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.aplicacao_area_id', '=', 'aplicacoes_areas.id')
            ->select('aplicacoes_areas_servicos.servico_id', 'areas_de_negocio.id as id', 'areas_de_negocio.slug as slug', 'areas_de_negocio.titulo as titulo')
            ->where('aplicacoes_areas_servicos.servico_id', $servico)
            ->distinct()->orderBy('areas_de_negocio.id', 'ASC')->get();

        return view('frontend.servicos', compact('servicoShow', 'grupos', 'imagens', 'areas', 'aplicacoes'));
    }

    public function showServicosGrupo($grupo)
    {
        $servicos = Servico::where('grupo_id', $grupo)->orderBy('titulo', 'ASC')->get();

        return response()->json(['servicos' => $servicos]);
    }

    public function getDadosServicos($servico)
    {
        $servico = Servico::where('slug', $servico)->first();
        $imagens = ServicoImagem::where('servico_id', $servico->id)->ordenados()->get();

        $aplicacoes = Aplicacao::join('aplicacoes_areas', 'aplicacoes_areas.aplicacao_id', '=', 'aplicacoes.id')
            ->join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.aplicacao_area_id', '=', 'aplicacoes_areas.id')
            ->select('aplicacoes_areas_servicos.servico_id', 'aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo', 'aplicacoes_areas.area_id as area_id')
            ->where('aplicacoes_areas_servicos.servico_id', $servico->id)
            ->orderBy('aplicacoes.titulo', 'ASC')->get();

        $areas = AreaDeNegocio::join('aplicacoes_areas', 'aplicacoes_areas.area_id', '=', 'areas_de_negocio.id')
            ->join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.aplicacao_area_id', '=', 'aplicacoes_areas.id')
            ->select('aplicacoes_areas_servicos.servico_id', 'areas_de_negocio.id as id', 'areas_de_negocio.slug as slug', 'areas_de_negocio.titulo as titulo')
            ->where('aplicacoes_areas_servicos.servico_id', $servico->id)
            ->distinct()->orderBy('areas_de_negocio.id', 'ASC')->get();

        return response()->json(['servico' => $servico, 'imagens' => $imagens, 'areas' => $areas, 'aplicacoes' => $aplicacoes]);
    }
}
