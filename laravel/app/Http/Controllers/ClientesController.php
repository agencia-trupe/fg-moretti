<?php

namespace App\Http\Controllers;

use App\Models\Cliente;

class ClientesController extends Controller
{
    public function index()
    {
        $clientes = Cliente::orderBy('nome', 'ASC')->get();

        return view('frontend.clientes', compact('clientes'));
    }
}
