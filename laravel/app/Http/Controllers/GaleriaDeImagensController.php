<?php

namespace App\Http\Controllers;

use App\Models\Aplicacao;
use App\Models\AreaDeNegocio;
use App\Models\Obra;
use App\Models\ObraArea;
use App\Models\ObraImagem;
use App\Models\ObraServico;
use App\Models\Servico;

class GaleriaDeImagensController extends Controller
{
    public function index()
    {
        $obras = Obra::join('aplicacoes', 'aplicacoes.id', '=', 'obras.aplicacao_id')
            ->select('obras.id as id', 'obras.ordem as ordem', 'obras.slug as slug', 'obras.titulo as titulo', 'obras.texto as texto', 'obras.capa as capa', 'aplicacoes.id as aplicacao_id', 'aplicacoes.slug as aplicacao_slug', 'aplicacoes.titulo as aplicacao_titulo')
            ->distinct()->ordenados()->get();

        $areasObra = AreaDeNegocio::join('obras_areas', 'obras_areas.area_id', '=', 'areas_de_negocio.id')
            ->join('obras', 'obras.id', '=', 'obras_areas.obra_id')
            ->select('areas_de_negocio.id as area_id', 'areas_de_negocio.slug as area_slug', 'areas_de_negocio.titulo as area_titulo', 'obras.id as obra_id')
            ->orderBy('area_id', 'ASC')->get();

        // FILTRO GALERIA
        $areas = ObraArea::join('areas_de_negocio', 'areas_de_negocio.id', '=', 'obras_areas.area_id')
            ->select('areas_de_negocio.id as id', 'areas_de_negocio.slug as slug', 'areas_de_negocio.titulo as titulo')
            ->distinct()->orderBy('areas_de_negocio.id', 'ASC')->get();
        $aplicacoes = Obra::join('aplicacoes', 'aplicacoes.id', '=', 'obras.aplicacao_id')
            ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
            ->distinct()->orderBy('aplicacoes.titulo', 'ASC')->get();
        $servicos = ObraServico::join('servicos', 'servicos.id', '=', 'obras_servicos.servico_id')
            ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo')
            ->distinct()->orderBy('servicos.titulo', 'ASC')->get();

        return view('frontend.galeria-de-imagens', compact('obras', 'areasObra', 'areas', 'aplicacoes', 'servicos'));
    }

    public function show($obra)
    {
        $imagens = ObraImagem::join('obras', 'obras.id', '=', 'obras_imagens.obra_id')
            ->where('obras.slug', $obra)
            ->select('obras_imagens.id as id', 'obras_imagens.ordem as ordem', 'obras_imagens.imagem as imagem')
            ->ordenados()->get();

        $obraDados = Obra::join('aplicacoes', 'aplicacoes.id', '=', 'obras.aplicacao_id')
            ->join('obras_areas', 'obras_areas.obra_id', '=', 'obras.id')
            ->select('obras.id as id', 'obras.ordem as ordem', 'obras.slug as slug', 'obras.titulo as titulo', 'obras.texto as texto', 'aplicacoes.id as aplicacao_id', 'aplicacoes.slug as aplicacao_slug', 'aplicacoes.titulo as aplicacao_titulo')
            ->where('obras.slug', $obra)->first();

        $areasObra = AreaDeNegocio::join('obras_areas', 'obras_areas.area_id', '=', 'areas_de_negocio.id')
            ->join('obras', 'obras.id', '=', 'obras_areas.obra_id')
            ->select('areas_de_negocio.id as area_id', 'areas_de_negocio.slug as area_slug', 'areas_de_negocio.titulo as area_titulo', 'obras.id as obra_id')
            ->where('obras.id', $obraDados->id)->orderBy('area_id', 'ASC')->get();

        $servicosObra = ObraServico::where('obra_id', $obraDados->id)
            ->join('servicos', 'servicos.id', '=', 'obras_servicos.servico_id')
            ->select('servicos.id as servico_id', 'servicos.titulo as servico_titulo', 'servicos.grupo_id as grupo_id', 'obras_servicos.id as id')
            ->orderBy('servicos.titulo', 'ASC')
            ->get();

        return view('frontend.galeria-de-imagens-show', compact('imagens', 'obraDados', 'areasObra', 'servicosObra'));
    }

    public function filtroGaleria($area, $aplicacao, $servico)
    {
        if ($area == 0 && $aplicacao == 0 && $servico == 0) {

            $aplicacoes = Obra::join('aplicacoes', 'aplicacoes.id', '=', 'obras.aplicacao_id')
                ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
                ->distinct()->orderBy('aplicacoes.titulo', 'ASC')->get();
            $servicos = ObraServico::join('servicos', 'servicos.id', '=', 'obras_servicos.servico_id')
                ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo')
                ->distinct()->orderBy('servicos.titulo', 'ASC')->get();
            $obras = Obra::join('aplicacoes', 'aplicacoes.id', '=', 'obras.aplicacao_id')
                ->select('obras.id as id', 'obras.ordem as ordem', 'obras.slug as slug', 'obras.titulo as titulo', 'obras.capa as capa', 'aplicacoes.id as aplicacao_id', 'aplicacoes.titulo as aplicacao_titulo')
                ->distinct()->ordenados()->get();
            $areasObra = ObraArea::join('areas_de_negocio', 'areas_de_negocio.id', '=', 'obras_areas.area_id')
                ->select('areas_de_negocio.id as area_id', 'areas_de_negocio.slug as area_slug', 'areas_de_negocio.titulo as area_titulo', 'obras_areas.obra_id as obra_id')
                ->orderBy('area_id', 'ASC')->get();
        } elseif ($area != 0 && $aplicacao == 0 && $servico == 0) {

            $aplicacoes = Obra::join('aplicacoes', 'aplicacoes.id', '=', 'obras.aplicacao_id')
                ->join('obras_areas', 'obras_areas.obra_id', '=', 'obras.id')
                ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo', 'obras_areas.area_id as area_id')
                ->where('obras_areas.area_id', $area)
                ->distinct()->orderBy('aplicacoes.titulo', 'ASC')->get();
            $servicos = ObraServico::join('servicos', 'servicos.id', '=', 'obras_servicos.servico_id')
                ->join('obras_areas', 'obras_areas.obra_id', '=', 'obras_servicos.obra_id')
                ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo', 'obras_areas.area_id as area_id')
                ->where('obras_areas.area_id', $area)
                ->distinct()->orderBy('servicos.titulo', 'ASC')->get();
            $obras = Obra::join('aplicacoes', 'aplicacoes.id', '=', 'obras.aplicacao_id')
                ->join('obras_areas', 'obras_areas.obra_id', '=', 'obras.id')
                ->join('areas_de_negocio', 'areas_de_negocio.id', '=', 'obras_areas.area_id')
                ->select('obras.id as id', 'obras.ordem as ordem', 'obras.slug as slug', 'obras.titulo as titulo', 'obras.capa as capa', 'aplicacoes.id as aplicacao_id', 'aplicacoes.titulo as aplicacao_titulo', 'areas_de_negocio.id as area_id')
                ->where('areas_de_negocio.id', $area)
                ->distinct()->ordenados()->get();
            $areasObra = ObraArea::join('areas_de_negocio', 'areas_de_negocio.id', '=', 'obras_areas.area_id')
                ->select('areas_de_negocio.id as area_id', 'areas_de_negocio.slug as area_slug', 'areas_de_negocio.titulo as area_titulo', 'obras_areas.obra_id as obra_id')
                ->orderBy('area_id', 'ASC')->get();
        } elseif ($area == 0 && $aplicacao != 0 && $servico == 0) {

            $aplicacoes = Obra::join('aplicacoes', 'aplicacoes.id', '=', 'obras.aplicacao_id')
                ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
                ->distinct()->orderBy('aplicacoes.titulo', 'ASC')->get();
            $servicos = ObraServico::join('servicos', 'servicos.id', '=', 'obras_servicos.servico_id')
                ->join('obras', 'obras.id', '=', 'obras_servicos.obra_id')
                ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo', 'obras.aplicacao_id as aplicacao_id')
                ->where('obras.aplicacao_id', $aplicacao)
                ->distinct()->orderBy('servicos.titulo', 'ASC')->get();
            $obras = Obra::join('aplicacoes', 'aplicacoes.id', '=', 'obras.aplicacao_id')
                ->select('obras.id as id', 'obras.ordem as ordem', 'obras.slug as slug', 'obras.titulo as titulo', 'obras.capa as capa', 'aplicacoes.id as aplicacao_id', 'aplicacoes.titulo as aplicacao_titulo')
                ->where('aplicacoes.id', $aplicacao)
                ->distinct()->ordenados()->get();
            $areasObra = ObraArea::join('areas_de_negocio', 'areas_de_negocio.id', '=', 'obras_areas.area_id')
                ->select('areas_de_negocio.id as area_id', 'areas_de_negocio.slug as area_slug', 'areas_de_negocio.titulo as area_titulo', 'obras_areas.obra_id as obra_id')
                ->orderBy('area_id', 'ASC')->get();
        } elseif ($area != 0 && $aplicacao != 0 && $servico == 0) {

            $aplicacoes = Obra::join('aplicacoes', 'aplicacoes.id', '=', 'obras.aplicacao_id')
                ->join('obras_areas', 'obras_areas.obra_id', '=', 'obras.id')
                ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo', 'obras_areas.area_id as area_id')
                ->where('obras_areas.area_id', $area)
                ->distinct()->orderBy('aplicacoes.titulo', 'ASC')->get();
            $servicos = ObraServico::join('servicos', 'servicos.id', '=', 'obras_servicos.servico_id')
                ->join('obras', 'obras.id', '=', 'obras_servicos.obra_id')
                ->join('obras_areas', 'obras_areas.obra_id', '=', 'obras.id')
                ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo', 'obras.aplicacao_id as aplicacao_id', 'obras_areas.area_id as area_id')
                ->where('obras_areas.area_id', $area)
                ->where('obras.aplicacao_id', $aplicacao)
                ->distinct()->orderBy('servicos.titulo', 'ASC')->get();
            $obras = Obra::join('aplicacoes', 'aplicacoes.id', '=', 'obras.aplicacao_id')
                ->join('obras_areas', 'obras_areas.obra_id', '=', 'obras.id')
                ->select('obras.id as id', 'obras.ordem as ordem', 'obras.slug as slug', 'obras.titulo as titulo', 'obras.capa as capa', 'aplicacoes.id as aplicacao_id', 'aplicacoes.titulo as aplicacao_titulo', 'obras_areas.area_id as area_id')
                ->where('obras_areas.area_id', $area)
                ->where('aplicacoes.id', $aplicacao)
                ->distinct()->ordenados()->get();
            $areasObra = ObraArea::join('areas_de_negocio', 'areas_de_negocio.id', '=', 'obras_areas.area_id')
                ->select('areas_de_negocio.id as area_id', 'areas_de_negocio.slug as area_slug', 'areas_de_negocio.titulo as area_titulo', 'obras_areas.obra_id as obra_id')
                ->orderBy('area_id', 'ASC')->get();
        } elseif ($area == 0 && $aplicacao == 0 && $servico != 0) {

            $aplicacoes = Obra::join('aplicacoes', 'aplicacoes.id', '=', 'obras.aplicacao_id')
                ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
                ->distinct()->orderBy('aplicacoes.titulo', 'ASC')->get();
            $servicos = ObraServico::join('servicos', 'servicos.id', '=', 'obras_servicos.servico_id')
                ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo')
                ->distinct()->orderBy('servicos.titulo', 'ASC')->get();
            $obras = Obra::join('aplicacoes', 'aplicacoes.id', '=', 'obras.aplicacao_id')
                ->join('obras_servicos', 'obras_servicos.obra_id', '=', 'obras.id')
                ->select('obras.id as id', 'obras.ordem as ordem', 'obras.slug as slug', 'obras.titulo as titulo', 'obras.capa as capa', 'aplicacoes.id as aplicacao_id', 'aplicacoes.titulo as aplicacao_titulo', 'obras_servicos.servico_id as servico_id')
                ->where('obras_servicos.servico_id', $servico)
                ->distinct()->ordenados()->get();
            $areasObra = ObraArea::join('areas_de_negocio', 'areas_de_negocio.id', '=', 'obras_areas.area_id')
                ->select('areas_de_negocio.id as area_id', 'areas_de_negocio.slug as area_slug', 'areas_de_negocio.titulo as area_titulo', 'obras_areas.obra_id as obra_id')
                ->orderBy('area_id', 'ASC')->get();
        } elseif ($area == 0 && $aplicacao != 0 && $servico != 0) {

            $aplicacoes = Obra::join('aplicacoes', 'aplicacoes.id', '=', 'obras.aplicacao_id')
                ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
                ->distinct()->orderBy('aplicacoes.titulo', 'ASC')->get();
            $servicos = ObraServico::join('servicos', 'servicos.id', '=', 'obras_servicos.servico_id')
                ->join('obras', 'obras.id', '=', 'obras_servicos.obra_id')
                ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo', 'obras.aplicacao_id as aplicacao_id')
                ->where('obras.aplicacao_id', $aplicacao)
                ->distinct()->orderBy('servicos.titulo', 'ASC')->get();
            $obras = Obra::join('aplicacoes', 'aplicacoes.id', '=', 'obras.aplicacao_id')
                ->join('obras_servicos', 'obras_servicos.obra_id', '=', 'obras.id')
                ->select('obras.id as id', 'obras.ordem as ordem', 'obras.slug as slug', 'obras.titulo as titulo', 'obras.capa as capa', 'aplicacoes.id as aplicacao_id', 'aplicacoes.titulo as aplicacao_titulo', 'obras_servicos.servico_id as servico_id')
                ->where('aplicacoes.id', $aplicacao)
                ->where('obras_servicos.servico_id', $servico)
                ->distinct()->ordenados()->get();
            $areasObra = ObraArea::join('areas_de_negocio', 'areas_de_negocio.id', '=', 'obras_areas.area_id')
                ->select('areas_de_negocio.id as area_id', 'areas_de_negocio.slug as area_slug', 'areas_de_negocio.titulo as area_titulo', 'obras_areas.obra_id as obra_id')
                ->orderBy('area_id', 'ASC')->get();
        } elseif ($area != 0 && $aplicacao != 0 && $servico != 0) {

            $aplicacoes = Obra::join('aplicacoes', 'aplicacoes.id', '=', 'obras.aplicacao_id')
                ->join('obras_areas', 'obras_areas.obra_id', '=', 'obras.id')
                ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo', 'obras_areas.area_id as area_id')
                ->where('obras_areas.area_id', $area)
                ->distinct()->orderBy('aplicacoes.titulo', 'ASC')->get();
            $servicos = ObraServico::join('servicos', 'servicos.id', '=', 'obras_servicos.servico_id')
                ->join('obras', 'obras.id', '=', 'obras_servicos.obra_id')
                ->join('obras_areas', 'obras_areas.obra_id', '=', 'obras.id')
                ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo', 'obras.aplicacao_id as aplicacao_id', 'obras_areas.area_id as area_id')
                ->where('obras_areas.area_id', $area)
                ->where('obras.aplicacao_id', $aplicacao)
                ->distinct()->orderBy('servicos.titulo', 'ASC')->get();
            $obras = Obra::join('aplicacoes', 'aplicacoes.id', '=', 'obras.aplicacao_id')
                ->join('obras_areas', 'obras_areas.obra_id', '=', 'obras.id')
                ->join('obras_servicos', 'obras_servicos.obra_id', '=', 'obras.id')
                ->select('obras.id as id', 'obras.ordem as ordem', 'obras.slug as slug', 'obras.titulo as titulo', 'obras.capa as capa', 'aplicacoes.id as aplicacao_id', 'aplicacoes.titulo as aplicacao_titulo', 'obras_areas.area_id as area_id', 'obras_servicos.servico_id as servico_id')
                ->where('obras_areas.area_id', $area)
                ->where('aplicacoes.id', $aplicacao)
                ->where('obras_servicos.servico_id', $servico)
                ->distinct()->ordenados()->get();
            $areasObra = ObraArea::join('areas_de_negocio', 'areas_de_negocio.id', '=', 'obras_areas.area_id')
                ->select('areas_de_negocio.id as area_id', 'areas_de_negocio.slug as area_slug', 'areas_de_negocio.titulo as area_titulo', 'obras_areas.obra_id as obra_id')
                ->orderBy('area_id', 'ASC')->get();
        } else {

            $aplicacoes = [];
            $servicos = [];
            $obras = [];
            $areasObra = [];
        }

        return response()->json(['aplicacoes' => $aplicacoes, 'servicos' => $servicos, 'area' => $area, 'obras' => $obras, 'areasObra' => $areasObra]);
    }
}
