<?php

namespace App\Http\Controllers;

use App\Models\Aplicacao;
use App\Models\AreaDeNegocio;
use App\Models\Obra;
use App\Models\Servico;

class FiltroController extends Controller
{
    public function index($area, $aplicacao, $servico)
    {
        if ($area == 0 && $aplicacao == 0 && $servico == 0) {
            $areas = AreaDeNegocio::select('areas_de_negocio.id as id', 'areas_de_negocio.slug as slug', 'areas_de_negocio.titulo as titulo')
                ->orderBy('id', 'ASC')->get();
            $aplicacoes = Aplicacao::select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
                ->orderBy('aplicacoes.titulo', 'ASC')->get();
            $servicos = Servico::select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo', 'servicos.grupo_id as grupo_id')
                ->orderBy('servicos.titulo', 'ASC')->get();
            $obras = Obra::select('obras.id as id', 'obras.ordem as ordem', 'obras.slug as slug', 'obras.titulo as titulo')
                ->ordenados()->get();
        } elseif ($area != 0 && $aplicacao == 0 && $servico == 0) {
            $areas = AreaDeNegocio::where('id', $area)
                ->select('areas_de_negocio.id as id', 'areas_de_negocio.slug as slug', 'areas_de_negocio.titulo as titulo')
                ->orderBy('id', 'ASC')->get();
            $aplicacoes = Aplicacao::join('aplicacoes_areas', 'aplicacoes_areas.aplicacao_id', '=', 'aplicacoes.id')
                ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
                ->where('aplicacoes_areas.area_id', $area)
                ->orderBy('aplicacoes.titulo', 'ASC')->get();
            $servicos = Servico::join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.servico_id', '=', 'servicos.id')
                ->join('aplicacoes_areas', 'aplicacoes_areas.id', '=', 'aplicacoes_areas_servicos.aplicacao_area_id')
                ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo', 'servicos.grupo_id as grupo_id')
                ->where('aplicacoes_areas.area_id', $area)
                ->distinct()->orderBy('servicos.titulo', 'ASC')->get();
            $obras = Obra::join('obras_areas', 'obras_areas.obra_id', '=', 'obras.id')
                ->select('obras.id as id', 'obras.ordem as ordem', 'obras.slug as slug', 'obras.titulo as titulo')
                ->where('obras_areas.area_id', $area)
                ->distinct()->ordenados()->get();
        } elseif ($area == 0 && $aplicacao != 0 && $servico == 0) {
            $areas = AreaDeNegocio::join('aplicacoes_areas', 'aplicacoes_areas.area_id', '=', 'areas_de_negocio.id')
                ->select('areas_de_negocio.id as id', 'areas_de_negocio.slug as slug', 'areas_de_negocio.titulo as titulo')
                ->where('aplicacoes_areas.aplicacao_id', $aplicacao)
                ->orderBy('areas_de_negocio.id', 'ASC')->get();
            $aplicacoes = Aplicacao::where('id', $aplicacao)
                ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
                ->orderBy('titulo', 'ASC')->get();
            $servicos = Servico::join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.servico_id', '=', 'servicos.id')
                ->join('aplicacoes_areas', 'aplicacoes_areas.id', '=', 'aplicacoes_areas_servicos.aplicacao_area_id')
                ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo', 'servicos.grupo_id as grupo_id')
                ->where('aplicacoes_areas.aplicacao_id', $aplicacao)
                ->distinct()->orderBy('servicos.titulo', 'ASC')->get();
            $obras = Obra::where('aplicacao_id', $aplicacao)
                ->select('obras.id as id', 'obras.ordem as ordem', 'obras.slug as slug', 'obras.titulo as titulo')
                ->distinct()->ordenados()->get();
        } elseif ($area != 0 && $aplicacao != 0 && $servico == 0) {
            $areas = AreaDeNegocio::join('aplicacoes_areas', 'aplicacoes_areas.area_id', '=', 'areas_de_negocio.id')
                ->select('areas_de_negocio.id as id', 'areas_de_negocio.slug as slug', 'areas_de_negocio.titulo as titulo')
                ->where('areas_de_negocio.id', $area)
                ->where('aplicacoes_areas.aplicacao_id', $aplicacao)
                ->orderBy('areas_de_negocio.id', 'ASC')->get();
            $aplicacoes = Aplicacao::join('aplicacoes_areas', 'aplicacoes_areas.aplicacao_id', '=', 'aplicacoes.id')
                ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
                ->where('aplicacoes_areas.area_id', $area)
                ->where('aplicacoes_areas.aplicacao_id', $aplicacao)
                ->orderBy('aplicacoes.titulo', 'ASC')->get();
            $servicos = Servico::join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.servico_id', '=', 'servicos.id')
                ->join('aplicacoes_areas', 'aplicacoes_areas.id', '=', 'aplicacoes_areas_servicos.aplicacao_area_id')
                ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo', 'servicos.grupo_id as grupo_id')
                ->where('aplicacoes_areas.area_id', $area)
                ->where('aplicacoes_areas.aplicacao_id', $aplicacao)
                ->distinct()->orderBy('servicos.titulo', 'ASC')->get();
            $obras = Obra::join('aplicacoes', 'aplicacoes.id', '=', 'obras.aplicacao_id')
                ->join('obras_areas', 'obras_areas.obra_id', '=', 'obras.id')
                ->select('obras.id as id', 'obras.ordem as ordem', 'obras.slug as slug', 'obras.titulo as titulo')
                ->where('obras_areas.area_id', $area)
                ->where('aplicacoes.id', $aplicacao)
                ->ordenados()->get();
        } elseif ($area != 0 && $aplicacao == 0 && $servico != 0) {
            $areas = AreaDeNegocio::join('aplicacoes_areas', 'aplicacoes_areas.area_id', '=', 'areas_de_negocio.id')
                ->join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.aplicacao_area_id', '=', 'aplicacoes_areas.id')
                ->select('areas_de_negocio.id as id', 'areas_de_negocio.slug as slug', 'areas_de_negocio.titulo as titulo')
                ->where('areas_de_negocio.id', $area)
                ->where('aplicacoes_areas_servicos.servico_id', $servico)
                ->distinct()->orderBy('areas_de_negocio.id', 'ASC')->get();
            $aplicacoes = Aplicacao::join('aplicacoes_areas', 'aplicacoes_areas.aplicacao_id', '=', 'aplicacoes.id')
                ->join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.aplicacao_area_id', '=', 'aplicacoes_areas.id')
                ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
                ->where('aplicacoes_areas.area_id', $area)
                ->where('aplicacoes_areas_servicos.servico_id', $servico)
                ->orderBy('aplicacoes.titulo', 'ASC')->get();
            $servicos = Servico::join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.servico_id', '=', 'servicos.id')
                ->join('aplicacoes_areas', 'aplicacoes_areas.id', '=', 'aplicacoes_areas_servicos.aplicacao_area_id')
                ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo', 'servicos.grupo_id as grupo_id')
                ->where('aplicacoes_areas.area_id', $area)
                ->where('servicos.id', $servico)
                ->distinct()->orderBy('servicos.titulo', 'ASC')->get();
            $obras = Obra::join('obras_servicos', 'obras_servicos.obra_id', '=', 'obras.id')
                ->join('obras_areas', 'obras_areas.obra_id', '=', 'obras.id')
                ->select('obras.id as id', 'obras.ordem as ordem', 'obras.slug as slug', 'obras.titulo as titulo')
                ->where('obras_areas.area_id', $area)
                ->where('obras_servicos.servico_id', $servico)
                ->distinct()->ordenados()->get();
        } elseif ($area == 0 && $aplicacao == 0 && $servico != 0) {
            $areas = AreaDeNegocio::join('aplicacoes_areas', 'aplicacoes_areas.area_id', '=', 'areas_de_negocio.id')
                ->join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.aplicacao_area_id', '=', 'aplicacoes_areas.id')
                ->select('areas_de_negocio.id as id', 'areas_de_negocio.slug as slug', 'areas_de_negocio.titulo as titulo')
                ->where('aplicacoes_areas_servicos.servico_id', $servico)
                ->distinct()->orderBy('areas_de_negocio.id', 'ASC')->get();
            $aplicacoes = Aplicacao::join('aplicacoes_areas', 'aplicacoes_areas.aplicacao_id', '=', 'aplicacoes.id')
                ->join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.aplicacao_area_id', '=', 'aplicacoes_areas.id')
                ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
                ->where('aplicacoes_areas_servicos.servico_id', $servico)
                ->distinct()->orderBy('aplicacoes.titulo', 'ASC')->get();
            $servicos = Servico::where('id', $servico)
                ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo', 'servicos.grupo_id as grupo_id')
                ->orderBy('servicos.titulo', 'ASC')->get();
            $obras = Obra::join('obras_servicos', 'obras_servicos.obra_id', '=', 'obras.id')
                ->select('obras.id as id', 'obras.ordem as ordem', 'obras.slug as slug', 'obras.titulo as titulo')
                ->where('obras_servicos.servico_id', $servico)
                ->distinct()->ordenados()->get();
        } elseif ($area == 0 && $aplicacao != 0 && $servico != 0) {
            $areas = AreaDeNegocio::join('aplicacoes_areas', 'aplicacoes_areas.area_id', '=', 'areas_de_negocio.id')
                ->join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.aplicacao_area_id', '=', 'aplicacoes_areas.id')
                ->select('areas_de_negocio.id as id', 'areas_de_negocio.slug as slug', 'areas_de_negocio.titulo as titulo')
                ->where('aplicacoes_areas.aplicacao_id', $aplicacao)
                ->where('aplicacoes_areas_servicos.servico_id', $servico)
                ->orderBy('areas_de_negocio.id', 'ASC')->get();
            $aplicacoes = Aplicacao::join('aplicacoes_areas', 'aplicacoes_areas.aplicacao_id', '=', 'aplicacoes.id')
                ->join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.aplicacao_area_id', '=', 'aplicacoes_areas.id')
                ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
                ->where('aplicacoes.id', $aplicacao)
                ->where('aplicacoes_areas_servicos.servico_id', $servico)
                ->orderBy('aplicacoes.titulo', 'ASC')->get();
            $servicos = Servico::join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.servico_id', '=', 'servicos.id')
                ->join('aplicacoes_areas', 'aplicacoes_areas.id', '=', 'aplicacoes_areas_servicos.aplicacao_area_id')
                ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo', 'servicos.grupo_id as grupo_id')
                ->where('aplicacoes_areas.aplicacao_id', $aplicacao)
                ->where('servicos.id', $servico)
                ->orderBy('servicos.titulo', 'ASC')->get();
            $obras = Obra::join('obras_servicos', 'obras_servicos.obra_id', '=', 'obras.id')
                ->select('obras.id as id', 'obras.ordem as ordem', 'obras.slug as slug', 'obras.titulo as titulo')
                ->where('obras.aplicacao_id', $aplicacao)
                ->where('obras_servicos.servico_id', $servico)
                ->ordenados()->get();
        } elseif ($area != 0 && $aplicacao != 0 && $servico != 0) {
            $areas = AreaDeNegocio::join('aplicacoes_areas', 'aplicacoes_areas.area_id', '=', 'areas_de_negocio.id')
                ->join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.aplicacao_area_id', '=', 'aplicacoes_areas.id')
                ->select('areas_de_negocio.id as id', 'areas_de_negocio.slug as slug', 'areas_de_negocio.titulo as titulo')
                ->where('areas_de_negocio.id', $area)
                ->where('aplicacoes_areas.aplicacao_id', $aplicacao)
                ->where('aplicacoes_areas_servicos.servico_id', $servico)
                ->orderBy('areas_de_negocio.id', 'ASC')->get();
            $aplicacoes = Aplicacao::join('aplicacoes_areas', 'aplicacoes_areas.aplicacao_id', '=', 'aplicacoes.id')
                ->join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.aplicacao_area_id', '=', 'aplicacoes_areas.id')
                ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
                ->where('aplicacoes_areas.area_id', $area)
                ->where('aplicacoes.id', $aplicacao)
                ->where('aplicacoes_areas_servicos.servico_id', $servico)
                ->orderBy('aplicacoes.titulo', 'ASC')->get();
            $servicos = Servico::join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.servico_id', '=', 'servicos.id')
                ->join('aplicacoes_areas', 'aplicacoes_areas.id', '=', 'aplicacoes_areas_servicos.aplicacao_area_id')
                ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo', 'servicos.grupo_id as grupo_id')
                ->where('aplicacoes_areas.area_id', $area)
                ->where('aplicacoes_areas.aplicacao_id', $aplicacao)
                ->where('servicos.id', $servico)
                ->distinct()->orderBy('servicos.titulo', 'ASC')->get();
            $obras = Obra::join('obras_servicos', 'obras_servicos.obra_id', '=', 'obras.id')
                ->join('obras_areas', 'obras_areas.obra_id', '=', 'obras.id')
                ->select('obras.id as id', 'obras.ordem as ordem', 'obras.slug as slug', 'obras.titulo as titulo')
                ->where('obras_areas.area_id', $area)
                ->where('obras.aplicacao_id', $aplicacao)
                ->where('obras_servicos.servico_id', $servico)
                ->distinct()->ordenados()->get();
        } else {
            $aplicacoes = [];
            $servicos = [];
            $obras = [];
            $areas = [];
        }

        return view('frontend.resultados-filtro', compact('aplicacoes', 'servicos', 'obras', 'areas'));
    }

    public function filtroGeral($area, $aplicacao, $servico)
    {
        if ($area == 0 && $aplicacao == 0 && $servico == 0) {
            $areas = AreaDeNegocio::select('areas_de_negocio.id as id', 'areas_de_negocio.slug as slug', 'areas_de_negocio.titulo as titulo')
                ->orderBy('id', 'ASC')->get();
            $aplicacoes = Aplicacao::select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
                ->orderBy('aplicacoes.titulo', 'ASC')->get();
            $servicos = Servico::select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo', 'servicos.grupo_id as grupo_id')
                ->orderBy('servicos.titulo', 'ASC')->get();
        } elseif ($area != 0 && $aplicacao == 0 && $servico == 0) {
            $areas = AreaDeNegocio::where('id', $area)
                ->select('areas_de_negocio.id as id', 'areas_de_negocio.slug as slug', 'areas_de_negocio.titulo as titulo')
                ->orderBy('id', 'ASC')->get();
            $aplicacoes = Aplicacao::join('aplicacoes_areas', 'aplicacoes_areas.aplicacao_id', '=', 'aplicacoes.id')
                ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
                ->where('aplicacoes_areas.area_id', $area)
                ->groupBy('aplicacoes.id')->orderBy('aplicacoes.titulo', 'ASC')->get();
            $servicos = Servico::join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.servico_id', '=', 'servicos.id')
                ->join('aplicacoes_areas', 'aplicacoes_areas.id', '=', 'aplicacoes_areas_servicos.aplicacao_area_id')
                ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo')
                ->where('aplicacoes_areas.area_id', $area)
                ->distinct()->orderBy('servicos.titulo', 'ASC')->get();
        } elseif ($area == 0 && $aplicacao != 0 && $servico == 0) {
            $areas = AreaDeNegocio::join('aplicacoes_areas', 'aplicacoes_areas.area_id', '=', 'areas_de_negocio.id')
                ->select('areas_de_negocio.id as id', 'areas_de_negocio.slug as slug', 'areas_de_negocio.titulo as titulo')
                ->where('aplicacoes_areas.aplicacao_id', $aplicacao)
                ->orderBy('areas_de_negocio.id', 'ASC')->get();
            $aplicacoes = Aplicacao::where('id', $aplicacao)
                ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
                ->orderBy('titulo', 'ASC')->get();
            $servicos = Servico::join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.servico_id', '=', 'servicos.id')
                ->join('aplicacoes_areas', 'aplicacoes_areas.id', '=', 'aplicacoes_areas_servicos.aplicacao_area_id')
                ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo')
                ->where('aplicacoes_areas.aplicacao_id', $aplicacao)
                ->distinct()->orderBy('servicos.titulo', 'ASC')->get();
        } elseif ($area != 0 && $aplicacao != 0 && $servico == 0) {
            $areas = AreaDeNegocio::join('aplicacoes_areas', 'aplicacoes_areas.area_id', '=', 'areas_de_negocio.id')
                ->select('areas_de_negocio.id as area_id', 'areas_de_negocio.slug as area_slug', 'areas_de_negocio.titulo as area_titulo')
                ->where('areas_de_negocio.id', $area)
                ->where('aplicacoes_areas.aplicacao_id', $aplicacao)
                ->orderBy('areas_de_negocio.id', 'ASC')->get();
            $aplicacoes = Aplicacao::join('aplicacoes_areas', 'aplicacoes_areas.aplicacao_id', '=', 'aplicacoes.id')
                ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
                ->where('aplicacoes_areas.area_id', $area)
                ->where('aplicacoes.id', $aplicacao)
                ->orderBy('aplicacoes.titulo', 'ASC')->get();
            $servicos = Servico::join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.servico_id', '=', 'servicos.id')
                ->join('aplicacoes_areas', 'aplicacoes_areas.id', '=', 'aplicacoes_areas_servicos.aplicacao_area_id')
                ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo')
                ->where('aplicacoes_areas.area_id', $area)
                ->where('aplicacoes_areas.aplicacao_id', $aplicacao)
                ->distinct()->orderBy('servicos.titulo', 'ASC')->get();
        } elseif ($area == 0 && $aplicacao == 0 && $servico != 0) {
            $areas = AreaDeNegocio::join('aplicacoes_areas', 'aplicacoes_areas.area_id', '=', 'areas_de_negocio.id')
                ->join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.aplicacao_area_id', '=', 'aplicacoes_areas.id')
                ->select('areas_de_negocio.id as id', 'areas_de_negocio.slug as slug', 'areas_de_negocio.titulo as titulo')
                ->where('aplicacoes_areas_servicos.servico_id', $servico)
                ->orderBy('areas_de_negocio.id', 'ASC')->get();
            $aplicacoes = Aplicacao::join('aplicacoes_areas', 'aplicacoes_areas.aplicacao_id', '=', 'aplicacoes.id')
                ->join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.aplicacao_area_id', '=', 'aplicacoes_areas.id')
                ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
                ->where('aplicacoes_areas_servicos.servico_id', $servico)
                ->distinct()->orderBy('aplicacoes.titulo', 'ASC')->get();
            $servicos = Servico::where('id', $servico)
                ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo')
                ->orderBy('servicos.titulo', 'ASC')->get();
        } elseif ($area == 0 && $aplicacao != 0 && $servico != 0) {
            $areas = AreaDeNegocio::join('aplicacoes_areas', 'aplicacoes_areas.area_id', '=', 'areas_de_negocio.id')
                ->join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.aplicacao_area_id', '=', 'aplicacoes_areas.id')
                ->select('areas_de_negocio.id as id', 'areas_de_negocio.slug as slug', 'areas_de_negocio.titulo as titulo')
                ->where('aplicacoes_areas.aplicacao_id', $aplicacao)
                ->where('aplicacoes_areas_servicos.servico_id', $servico)
                ->orderBy('areas_de_negocio.id', 'ASC')->get();
            $aplicacoes = Aplicacao::join('aplicacoes_areas', 'aplicacoes_areas.aplicacao_id', '=', 'aplicacoes.id')
                ->join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.aplicacao_area_id', '=', 'aplicacoes_areas.id')
                ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
                ->where('aplicacoes.id', $aplicacao)
                ->where('aplicacoes_areas_servicos.servico_id', $servico)
                ->orderBy('aplicacoes.titulo', 'ASC')->get();
            $servicos = Servico::join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.servico_id', '=', 'servicos.id')
                ->join('aplicacoes_areas', 'aplicacoes_areas.id', '=', 'aplicacoes_areas_servicos.aplicacao_area_id')
                ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo')
                ->where('aplicacoes_areas.aplicacao_id', $aplicacao)
                ->where('servicos.id', $servico)
                ->orderBy('servicos.titulo', 'ASC')->get();
        } elseif ($area != 0 && $aplicacao != 0 && $servico != 0) {
            $areas = AreaDeNegocio::join('aplicacoes_areas', 'aplicacoes_areas.area_id', '=', 'areas_de_negocio.id')
                ->join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.aplicacao_area_id', '=', 'aplicacoes_areas.id')
                ->select('areas_de_negocio.id as id', 'areas_de_negocio.slug as slug', 'areas_de_negocio.titulo as titulo')
                ->where('areas_de_negocio.id', $area)
                ->where('aplicacoes_areas.aplicacao_id', $aplicacao)
                ->where('aplicacoes_areas_servicos.servico_id', $servico)
                ->orderBy('areas_de_negocio.id', 'ASC')->get();
            $aplicacoes = Aplicacao::join('aplicacoes_areas', 'aplicacoes_areas.aplicacao_id', '=', 'aplicacoes.id')
                ->join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.aplicacao_area_id', '=', 'aplicacoes_areas.id')
                ->select('aplicacoes.id as id', 'aplicacoes.slug as slug', 'aplicacoes.titulo as titulo')
                ->where('aplicacoes_areas.area_id', $area)
                ->where('aplicacoes.id', $aplicacao)
                ->where('aplicacoes_areas_servicos.servico_id', $servico)
                ->orderBy('aplicacoes.titulo', 'ASC')->get();
            $servicos = Servico::join('aplicacoes_areas_servicos', 'aplicacoes_areas_servicos.servico_id', '=', 'servicos.id')
                ->join('aplicacoes_areas', 'aplicacoes_areas.id', '=', 'aplicacoes_areas_servicos.aplicacao_area_id')
                ->select('servicos.id as id', 'servicos.slug as slug', 'servicos.titulo as titulo')
                ->where('aplicacoes_areas.area_id', $area)
                ->where('aplicacoes_areas.aplicacao_id', $aplicacao)
                ->where('servicos.id', $servico)
                ->distinct()->orderBy('servicos.titulo', 'ASC')->get();
        } else {
            $aplicacoes = [];
            $servicos = [];
            $areas = [];
        }

        return response()->json(['aplicacoes' => $aplicacoes, 'servicos' => $servicos, 'areas' => $areas]);
    }
}
