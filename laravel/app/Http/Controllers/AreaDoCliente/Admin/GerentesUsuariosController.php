<?php

namespace App\Http\Controllers\AreaDoCliente\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\AreaDoCliente\ClienteUsuario;
use App\Models\AreaDoCliente\Empresa;

class GerentesUsuariosController extends Controller
{
    public function index($empresa_slug, $gerente_slug)
    {
        $empresa = Empresa::where('slug', $empresa_slug)->first();
        $gerente = ClienteUsuario::gerente()->where('slug', $gerente_slug)->where('empresa_id', $empresa->id)->first();

        $usuarios = ClienteUsuario::join('ac_gerentes_obras_usuarios', 'ac_gerentes_obras_usuarios.usuario_id', '=', 'ac_clientes_usuarios.id')
            ->where('ac_gerentes_obras_usuarios.gerente_id', $gerente->id)
            ->where('ac_clientes_usuarios.empresa_id', $empresa->id)
            ->select('ac_clientes_usuarios.*')
            ->distinct()->orderBy('ac_clientes_usuarios.nome', 'asc')->get();

        return view('area-do-cliente.admin.gerentes.usuarios.index', compact('empresa', 'gerente', 'usuarios'));
    }

    public function destroy($empresa_slug, $gerente_slug, $usuario_id)
    {
        try {
            $empresa = Empresa::where('slug', $empresa_slug)->first();
            $gerente = ClienteUsuario::gerente()->where('slug', $gerente_slug)->where('empresa_id', $empresa->id)->first();
            $usuario = ClienteUsuario::join('ac_gerentes_obras_usuarios', 'ac_gerentes_obras_usuarios.usuario_id', '=', 'ac_clientes_usuarios.id')
                ->where('ac_gerentes_obras_usuarios.gerente_id', $gerente->id)
                ->where('ac_clientes_usuarios.empresa_id', $empresa->id)
                ->where('ac_clientes_usuarios.id', $usuario_id)
                ->select('ac_clientes_usuarios.*')
                ->first();
            
            $usuario->delete();

            return redirect()->route('area-do-cliente.gerentes.usuarios', [$empresa->slug, $gerente->slug])->with('success', 'Usuário excluído com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar gerente: ' . $e->getMessage()]);
        }
    }
}
