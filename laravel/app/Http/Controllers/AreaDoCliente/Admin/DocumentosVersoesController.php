<?php

namespace App\Http\Controllers\AreaDoCliente\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ACDocumentosVersoesRequest;
use App\Models\AreaDoCliente\ClienteUsuario;
use App\Models\AreaDoCliente\Documento;
use App\Models\AreaDoCliente\DocumentoVersao;
use App\Models\AreaDoCliente\Obra;
use App\Models\Contato;
use Illuminate\Support\Facades\Mail;

class DocumentosVersoesController extends Controller
{
    private function getName($file)
    {
        $extension = $file->getClientOriginalExtension();
        $name  = str_slug(
            pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)
        );
        $name .= '_' . date('YmdHis');
        $name .= '.' . $extension;

        return $name;
    }

    public function index($obra_slug, $doc_id)
    {
        $obra = Obra::where('slug', $obra_slug)->first();
        $documento = Documento::where('id', $doc_id)->where('obra_id', $obra->id)->first();

        $versoes = DocumentoVersao::documento($documento->id)->orderBy('updated_at', 'desc')->get();
        // dd($versoes);

        return view('area-do-cliente.admin.documentos.versoes.index', compact('obra', 'documento', 'versoes'));
    }

    public function create($obra_slug, $doc_id)
    {
        $obra = Obra::where('slug', $obra_slug)->first();
        $documento = Documento::where('id', $doc_id)->where('obra_id', $obra->id)->first();

        return view('area-do-cliente.admin.documentos.versoes.create', compact('obra', 'documento'));
    }

    public function store(ACDocumentosVersoesRequest $request, $obra_slug, $doc_id)
    {
        try {
            $obra = Obra::where('slug', $obra_slug)->first();
            $documento = Documento::where('id', $doc_id)->where('obra_id', $obra->id)->first();

            $data = $request->all();
            $data['documento_id'] = $documento->id;
            $data['slug'] = str_slug($request->titulo, "-");

            $file = $request->file('arquivo');

            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = $this->getName($file);
                    $path = public_path() . '/assets/obras-documentos';
                    $file->move($path, $filename);
                    $data['arquivo'] = $filename;
                }
            }

            $versao = DocumentoVersao::create($data);

            $this->sendMailDocumento($documento, $versao, $obra);

            return redirect()->route('area-do-cliente.versoes', [$obra->slug, $documento->id])->with('success', 'Versão do documento adicionada com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar documento: ' . $e->getMessage()]);
        }
    }

    public function edit($obra_slug, $doc_id, $versao_id)
    {
        $obra = Obra::where('slug', $obra_slug)->first();
        $documento = Documento::where('id', $doc_id)->where('obra_id', $obra->id)->first();
        $versao = DocumentoVersao::documento($documento->id)->where('id', $versao_id)->first();

        return view('area-do-cliente.admin.documentos.versoes.edit', compact('obra', 'documento', 'versao'));
    }

    public function update(ACDocumentosVersoesRequest $request, $obra_slug, $doc_id, $versao_id)
    {
        try {
            $obra = Obra::where('slug', $obra_slug)->first();
            $documento = Documento::where('id', $doc_id)->where('obra_id', $obra->id)->first();
            $versao = DocumentoVersao::documento($documento->id)->where('id', $versao_id)->first();

            $data = $request->all();
            $data['documento_id'] = $documento->id;
            $data['slug'] = str_slug($request->titulo, "-");

            $file = $request->file('arquivo');

            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = $this->getName($file);
                    $path = public_path() . '/assets/obras-documentos';
                    $file->move($path, $filename);
                    $data['arquivo'] = $filename;
                }
            }

            $versao->update($data);

            return redirect()->route('area-do-cliente.versoes', [$obra->slug, $documento->id, $versao_id])->with('success', 'Versão do documento atualizada com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar documento: ' . $e->getMessage()]);
        }
    }

    public function destroy($obra_slug, $doc_id, $versao_id)
    {
        try {
            $obra = Obra::where('slug', $obra_slug)->first();
            $documento = Documento::where('id', $doc_id)->where('obra_id', $obra->id)->first();
            $versao = DocumentoVersao::documento($documento->id)->where('id', $versao_id)->first();

            $versao->delete();

            return redirect()->route('area-do-cliente.versoes', [$obra->slug, $documento->id, $versao_id])->with('success', 'Documento excluído com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar documento: ' . $e->getMessage()]);
        }
    }

    public function openPDF($obra_slug, $doc_id, $versao_id)
    {
        $obra = Obra::where('slug', $obra_slug)->first();
        $documento = Documento::where('id', $doc_id)->where('obra_id', $obra->id)->first();
        $arquivo = DocumentoVersao::documento($documento->id)->where('id', $versao_id)->select('arquivo')->first();

        $arquivoPath = public_path() . "/assets/obras-documentos/" . $arquivo->arquivo;

        return response()->file($arquivoPath);
    }

    private function sendMailDocumento($documento, $versao, $obra)
    {
        if (!$email = Contato::first()->email) {
            return false;
        }

        $usuariosObra = ClienteUsuario::join('ac_gerentes_obras_usuarios', 'ac_gerentes_obras_usuarios.usuario_id', '=', 'ac_clientes_usuarios.id')
            ->select('ac_clientes_usuarios.id', 'ac_clientes_usuarios.email', 'ac_clientes_usuarios.nome', 'ac_clientes_usuarios.ativo')
            ->where('ac_gerentes_obras_usuarios.obra_id', $obra->id)
            ->where('ac_clientes_usuarios.ativo', 1)
            ->get();
        $gerentesObra = ClienteUsuario::join('ac_gerentes_obras', 'ac_gerentes_obras.usuario_id', '=', 'ac_clientes_usuarios.id')
            ->select('ac_clientes_usuarios.id', 'ac_clientes_usuarios.email', 'ac_clientes_usuarios.nome', 'ac_clientes_usuarios.ativo')
            ->where('ac_gerentes_obras.obra_id', $obra->id)
            ->where('ac_clientes_usuarios.ativo', 1)
            ->get();

        $emails = [];
        foreach ($usuariosObra as $usuario) {
            $emails[] = $usuario->email;
        }
        foreach ($gerentesObra as $usuario) {
            $emails[] = $usuario->email;
        }
        $emails = array_unique($emails);
        // dd($usuariosEmail);

        Mail::send('area-do-cliente.emails.novo-documento', ['documento' => $documento, 'emails' => $emails, 'obra' => $obra, 'versao' => $versao], function ($m) use ($email, $emails) {
            $m->to($emails)
                ->subject('[NOVO DOCUMENTO] GRUPO FG MORETTI')
                ->replyTo($email, 'GRUPO FG MORETTI');
        });
    }
}
