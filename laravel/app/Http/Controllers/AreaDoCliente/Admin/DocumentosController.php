<?php

namespace App\Http\Controllers\AreaDoCliente\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ACDocumentosRequest;
use App\Models\AreaDoCliente\ClienteUsuario;
use App\Models\AreaDoCliente\Documento;
use App\Models\AreaDoCliente\DocumentoVersao;
use App\Models\AreaDoCliente\Obra;
use App\Models\Contato;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class DocumentosController extends Controller
{
    private function getName($file)
    {
        $extension = $file->getClientOriginalExtension();
        $name  = str_slug(
            pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)
        );
        $name .= '_' . date('YmdHis');
        $name .= '.' . $extension;

        return $name;
    }

    public function index($obra_slug)
    {
        $obra = Obra::where('slug', $obra_slug)->first();
        $documentos = Documento::obra($obra->id)->ordenados()->get();

        return view('area-do-cliente.admin.documentos.index', compact('obra', 'documentos'));
    }

    public function create($obra_slug)
    {
        $obra = Obra::where('slug', $obra_slug)->first();
        return view('area-do-cliente.admin.documentos.create', compact('obra'));
    }

    public function store(ACDocumentosRequest $request, $obra_slug)
    {
        try {
            $obra = Obra::where('slug', $obra_slug)->first();

            $data = $request->all();

            $dadosDoc = [
                'obra_id' => $obra->id,
                'slug'    => str_slug($data['titulo'], "-"),
                'titulo'  => $data['titulo'],
                'descricao' => $data['descricao']
            ];

            $file = $request->file('arquivo');

            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = $this->getName($file);
                    $path = public_path() . '/assets/obras-documentos';
                    $file->move($path, $filename);
                    $data['arquivo'] = $filename;
                }
            }

            $documento = Documento::create($dadosDoc);

            $versao = [
                'documento_id' => $documento->id,
                'versao' => 'N001',
                'id' => $documento->id,
                'titulo' => $documento->titulo,
                'arquivo' => $data['arquivo']
            ];

            $versao = DocumentoVersao::create($versao);

            $this->sendMailDocumento($documento, $versao, $obra);

            return redirect()->route('area-do-cliente.documentos', $obra->slug)->with('success', 'Documento adicionado com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar documento: ' . $e->getMessage()]);
        }
    }

    public function edit($obra_slug, $doc_id)
    {
        $obra = Obra::where('slug', $obra_slug)->first();
        $documento = Documento::where('id', $doc_id)->where('obra_id', $obra->id)->first();

        return view('area-do-cliente.admin.documentos.edit', compact('obra', 'documento'));
    }

    public function update(ACDocumentosRequest $request, $obra_slug, $doc_id)
    {
        try {
            $obra = Obra::where('slug', $obra_slug)->first();
            $documento = Documento::where('id', $doc_id)->where('obra_id', $obra->id)->first();

            $data = $request->all();

            $dadosDoc = [
                'obra_id' => $obra->id,
                'slug'    => str_slug($data['titulo'], "-"),
                'titulo'  => $data['titulo'],
                'descricao' => $data['descricao']
            ];

            $documento->update($dadosDoc);

            return redirect()->route('area-do-cliente.documentos', $obra->slug)->with('success', 'Documento atualizado com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar documento: ' . $e->getMessage()]);
        }
    }

    public function destroy($obra_slug, $doc_id)
    {
        try {
            $obra = Obra::where('slug', $obra_slug)->first();
            $documento = Documento::where('id', $doc_id)->where('obra_id', $obra->id)->first();

            $documento->delete();

            return redirect()->route('area-do-cliente.documentos', $obra->slug)->with('success', 'Documento excluído com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar documento: ' . $e->getMessage()]);
        }
    }

    public function order(Request $request)
    {
        if (!$request->ajax()) abort('404');

        $data  = $request->input('data');
        $table = $request->input('table');

        for ($i = 0; $i < count($data); $i++) {
            DB::table($table)
                ->where('id', $data[$i])
                ->update(array('ordem' => $i + 1));
        }

        return json_encode($data);
    }

    private function sendMailDocumento($documento, $versao, $obra)
    {
        if (!$email = Contato::first()->email) {
            return false;
        }

        $usuariosObra = ClienteUsuario::join('ac_gerentes_obras_usuarios', 'ac_gerentes_obras_usuarios.usuario_id', '=', 'ac_clientes_usuarios.id')
            ->select('ac_clientes_usuarios.id', 'ac_clientes_usuarios.email', 'ac_clientes_usuarios.nome', 'ac_clientes_usuarios.ativo')
            ->where('ac_gerentes_obras_usuarios.obra_id', $obra->id)
            ->where('ac_clientes_usuarios.ativo', 1)
            ->get();
        $gerentesObra = ClienteUsuario::join('ac_gerentes_obras', 'ac_gerentes_obras.usuario_id', '=', 'ac_clientes_usuarios.id')
            ->select('ac_clientes_usuarios.id', 'ac_clientes_usuarios.email', 'ac_clientes_usuarios.nome', 'ac_clientes_usuarios.ativo')
            ->where('ac_gerentes_obras.obra_id', $obra->id)
            ->where('ac_clientes_usuarios.ativo', 1)
            ->get();

        $emails = [];
        foreach ($usuariosObra as $usuario) {
            $emails[] = $usuario->email;
        }
        foreach ($gerentesObra as $usuario) {
            $emails[] = $usuario->email;
        }
        $emails = array_unique($emails);
        // dd($usuariosEmail);

        Mail::send('area-do-cliente.emails.novo-documento', ['documento' => $documento, 'emails' => $emails, 'obra' => $obra, 'versao' => $versao], function ($m) use ($email, $emails) {
            $m->to($emails)
                ->subject('[NOVO DOCUMENTO] GRUPO FG MORETTI')
                ->replyTo($email, 'GRUPO FG MORETTI');
        });
    }
}
