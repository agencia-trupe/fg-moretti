<?php

namespace App\Http\Controllers\AreaDoCliente\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\AreaDoCliente\ClienteUsuario;
use App\Models\AreaDoCliente\Empresa;
use App\Models\AreaDoCliente\Obra;
use App\Models\AreaDoCliente\ObraComunicacao;
use App\Models\Contato;
use Illuminate\Support\Facades\Mail;

class ObrasComunicacoesController extends Controller
{
    public function index($obra_slug)
    {
        $obra = Obra::where('slug', $obra_slug)->first();
        $adminLogado = auth('admin')->user();

        $comunicacoes = ObraComunicacao::where('obra_id', $obra->id)->orderBy('created_at', 'desc')->get();

        $empresas = Empresa::get();

        return view('area-do-cliente.admin.obras.comunicacao', compact('obra', 'adminLogado', 'comunicacoes', 'empresas'));
    }

    public function post(Request $request, $obra_slug)
    {
        try {
            $obra = Obra::where('slug', $obra_slug)->first();

            $data = $request->all();
            $data['obra_id'] = $obra->id;

            if ($request->admin_id == "null") {
                $data['admin_id'] = null;
            } else {
                $comunicacoes = ObraComunicacao::where('obra_id', $obra->id)->where('usuario_id', '!=', null)->get();
                foreach ($comunicacoes as $c) {
                    $c['respondido'] = 1;
                    $c->save();
                }
            }
            if ($request->empresa_id == "null") {
                $data['empresa_id'] = null;
            }
            if ($request->usuario_id == "null") {
                $data['usuario_id'] = null;
            }

            // dd($data);

            ObraComunicacao::create($data);

            $this->sendMailComunicacao($obra);

            return redirect()->route('area-do-cliente.obras.comunicacao', $obra->slug)->with('success', 'Mensagem enviada com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao enviar mensagem: ' . $e->getMessage()]);
        }
    }

    public function destroy($obra_slug, $comunicacao_id)
    {
        try {
            $obra = Obra::where('slug', $obra_slug)->first();

            $comunicacao = ObraComunicacao::where('id', $comunicacao_id)->where('obra_id', $obra->id)->first();
            $comunicacao->delete();

            return redirect()->route('area-do-cliente.clientes.obra.comunicacao', $obra->slug)->with('success', 'Mensagem excluída com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir mensagem: ' . $e->getMessage()]);
        }
    }

    private function sendMailComunicacao($obra)
    {
        if (!$email = Contato::first()->email) {
            return false;
        }

        $usuariosObra = ClienteUsuario::join('ac_gerentes_obras_usuarios', 'ac_gerentes_obras_usuarios.usuario_id', '=', 'ac_clientes_usuarios.id')
            ->select('ac_clientes_usuarios.id', 'ac_clientes_usuarios.email', 'ac_clientes_usuarios.nome', 'ac_clientes_usuarios.ativo')
            ->where('ac_gerentes_obras_usuarios.obra_id', $obra->id)
            ->where('ac_clientes_usuarios.ativo', 1)
            ->get();
        $gerentesObra = ClienteUsuario::join('ac_gerentes_obras', 'ac_gerentes_obras.usuario_id', '=', 'ac_clientes_usuarios.id')
            ->select('ac_clientes_usuarios.id', 'ac_clientes_usuarios.email', 'ac_clientes_usuarios.nome', 'ac_clientes_usuarios.ativo')
            ->where('ac_gerentes_obras.obra_id', $obra->id)
            ->where('ac_clientes_usuarios.ativo', 1)
            ->get();

        $emails = [];
        foreach ($usuariosObra as $usuario) {
            $emails[] = $usuario->email;
        }
        foreach ($gerentesObra as $usuario) {
            $emails[] = $usuario->email;
        }
        $emails = array_unique($emails);
        // dd($usuariosEmail);

        Mail::send('area-do-cliente.emails.nova-comunicacao', ['emails' => $emails, 'obra' => $obra], function ($m) use ($email, $emails) {
            $m->to($emails)
                ->subject('[NOVA MENSAGEM - COMUNICAÇÃO] GRUPO FG MORETTI')
                ->replyTo($email, 'GRUPO FG MORETTI');
        });
    }
}
