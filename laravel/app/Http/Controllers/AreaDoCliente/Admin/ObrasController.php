<?php

namespace App\Http\Controllers\AreaDoCliente\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ACObrasRequest;
use App\Models\AreaDoCliente\Obra;

class ObrasController extends Controller
{
    public function index()
    {
        $obras = Obra::orderBy('titulo', 'asc')->get();

        return view('area-do-cliente.admin.obras.index', compact('obras'));
    }

    public function create()
    {
        return view('area-do-cliente.admin.obras.create');
    }

    public function store(ACObrasRequest $request)
    {
        try {
            $data = $request->all();
            $data['slug'] = str_slug($request->titulo, "-");

            Obra::create($data);

            return redirect()->route('area-do-cliente.obras')->with('success', 'Obra adicionada com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar obra: ' . $e->getMessage()]);
        }
    }

    public function edit($obra_slug)
    {
        $obra = Obra::where('slug', $obra_slug)->first();

        return view('area-do-cliente.admin.obras.edit', compact('obra'));
    }

    public function update(ACObrasRequest $request, $obra_slug)
    {
        try {
            $obra = Obra::where('slug', $obra_slug)->first();

            $data = $request->all();
            $data['slug'] = str_slug($request->titulo, "-");

            $obra->update($data);

            return redirect()->route('area-do-cliente.obras')->with('success', 'Obra alterada com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar obra: ' . $e->getMessage()]);
        }
    }

    public function destroy($obra_slug)
    {
        try {
            $obra = Obra::where('slug', $obra_slug)->first();
            $obra->delete();

            return redirect()->route('area-do-cliente.obras')->with('success', 'Obra excluída com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar obra: ' . $e->getMessage()]);
        }
    }
}
