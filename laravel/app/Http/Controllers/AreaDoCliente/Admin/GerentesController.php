<?php

namespace App\Http\Controllers\AreaDoCliente\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ACGerentesRequest;
use App\Models\AreaDoCliente\ClienteUsuario;
use App\Models\AreaDoCliente\Empresa;
use App\Models\Contato;
use Illuminate\Support\Facades\Mail;

class GerentesController extends Controller
{
    public function index($empresa_slug)
    {
        $empresa = Empresa::where('slug', $empresa_slug)->first();
        $gerentes = ClienteUsuario::empresa($empresa->id)->where('gerente', 1)->orderBy('nome', 'asc')->get();

        return view('area-do-cliente.admin.gerentes.index', compact('empresa', 'gerentes'));
    }

    public function create($empresa_slug)
    {
        $empresa = Empresa::where('slug', $empresa_slug)->first();

        return view('area-do-cliente.admin.gerentes.create', compact('empresa'));
    }

    public function store(ACGerentesRequest $request, $empresa_slug)
    {
        try {
            $empresa = Empresa::where('slug', $empresa_slug)->first();

            $data = $request->all();
            $data['empresa_id'] = $empresa->id;
            $data['slug'] = str_slug($request->nome, "-");
            $data['password'] = bcrypt('alterar#123');
            $data['gerente'] = 1;

            $gerente = ClienteUsuario::create($data);

            $dadosEmail = array(
                'email' => $gerente->email,
                'nome' => $gerente->nome,
                'token' => str_random(60)
            );
            $this->sendMail($dadosEmail);

            return redirect()->route('area-do-cliente.gerentes', $empresa->slug)->with('success', 'Gerente adicionado com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar gerente: ' . $e->getMessage()]);
        }
    }

    public function edit($empresa_slug, $gerente_slug)
    {
        $empresa = Empresa::where('slug', $empresa_slug)->first();
        $gerente = ClienteUsuario::where('slug', $gerente_slug)->where('empresa_id', $empresa->id)->first();

        return view('area-do-cliente.admin.gerentes.edit', compact('empresa', 'gerente'));
    }

    public function update(ACGerentesRequest $request, $empresa_slug, $gerente_slug)
    {
        try {
            $empresa = Empresa::where('slug', $empresa_slug)->first();
            $gerente = ClienteUsuario::where('slug', $gerente_slug)->where('empresa_id', $empresa->id)->first();

            $data = $request->all();
            $data['empresa_id'] = $empresa->id;
            $data['slug'] = str_slug($request->nome, "-");
            $data['password'] = bcrypt('alterar#123');

            $gerente->update($data);

            $dadosEmail = array(
                'email' => $gerente->email,
                'nome' => $gerente->nome,
                'token' => str_random(60)
            );
            $this->sendMail($dadosEmail);

            return redirect()->route('area-do-cliente.gerentes', $empresa->slug)->with('success', 'Gerente atualizado com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar gerente: ' . $e->getMessage()]);
        }
    }

    public function destroy($empresa_slug, $gerente_slug)
    {
        try {
            $empresa = Empresa::where('slug', $empresa_slug)->first();
            $gerente = ClienteUsuario::where('slug', $gerente_slug)->where('empresa_id', $empresa->id)->first();

            $gerente->delete();

            return redirect()->route('area-do-cliente.gerentes', $empresa->slug)->with('success', 'Gerente excluído com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar gerente: ' . $e->getMessage()]);
        }
    }

    private function sendMail($dadosEmail)
    {
        if (!$email = Contato::first()->email) {
            return false;
        }

        Mail::send('area-do-cliente.emails.gerente-ativar-cadastro', $dadosEmail, function ($m) use ($email, $dadosEmail) {
            $m->to($dadosEmail['email'], $dadosEmail['nome'])
                ->subject('[ATIVAR CADASTRO] GRUPO FG MORETTI')
                ->replyTo($email, 'GRUPO FG MORETTI');
        });
    }
}
