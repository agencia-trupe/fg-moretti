<?php

namespace App\Http\Controllers\AreaDoCliente\Admin\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\AreaDoCliente\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SenhaController extends Controller
{
    public function edit($email, $token)
    {
        Auth::guard()->logout();
        Auth::guard('admin')->logout();
        return view('area-do-cliente.admin.auth.alterar-senha', compact('token', 'email'));
    }

    public function update(Request $request)
    {
        try {
            $this->validate($request, [
                'password'   => 'required',
                'nova_senha' => 'required|confirmed|min:6'
            ]);

            $usuario = Admin::where('email', $request->email)->first();

            if (!Hash::check($request->password, $usuario->password)) {
                return back()->withErrors('Senha atual inválida.');
            }

            $usuario->password = bcrypt($request->nova_senha);
            $usuario->ativo = 1;

            $usuario->save();

            if ($usuario->ativo == 1) {
                Auth::guard('admin')->login($usuario);
            }

            return redirect()->route('area-do-cliente.admin')->with('success', 'Senha alterada com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao redefinir senha: As senhas devem ser iguais e devem ter no mínimo 6 caracteres.']);
        }
    }
}
