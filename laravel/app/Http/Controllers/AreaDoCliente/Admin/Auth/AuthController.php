<?php

namespace App\Http\Controllers\AreaDoCliente\Admin\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $guard = 'admin';
    protected $redirectTo = 'area-do-cliente/admin';
    protected $redirectAfterLogout = 'area-do-cliente/admin/login';
    protected $loginView = 'area-do-cliente.admin.auth.login';

    public function __construct()
    {
        Auth::guard()->logout();
        Auth::guard('clientes')->logout();
        $this->middleware('admin.guest', ['except' => 'logout']);
    }
}
