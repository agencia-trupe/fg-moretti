<?php

namespace App\Http\Controllers\AreaDoCliente\Admin\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class PasswordController extends Controller
{
    use ResetsPasswords;

    protected $broker = 'admin';
    protected $redirectPath = 'area-do-cliente/admin/login';
    protected $linkRequestView = 'area-do-cliente.admin.auth.esqueci-minha-senha';
    protected $resetView = 'area-do-cliente.admin.auth.reset';

    public function __construct()
    {
        $this->middleware('admin.guest');
    }
}
