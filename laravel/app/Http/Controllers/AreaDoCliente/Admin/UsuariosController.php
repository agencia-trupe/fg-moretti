<?php

namespace App\Http\Controllers\AreaDoCliente\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ACAdminsRequest;
use App\Models\AreaDoCliente\Admin;
use App\Models\Contato;
use Illuminate\Support\Facades\Mail;

class UsuariosController extends Controller
{
    public function index()
    {
        $usuarios = Admin::orderBy('name', 'asc')->get();

        return view('area-do-cliente.admin.usuarios.index', compact('usuarios'));
    }

    public function create()
    {
        return view('area-do-cliente.admin.usuarios.create');
    }

    public function store(ACAdminsRequest $request)
    {
        try {
            $data = $request->all();
            $data['password'] = bcrypt('alterar#123');

            $usuario = Admin::create($data);

            $dadosEmail = array(
                'email' => $usuario->email,
                'nome' => $usuario->nome,
                'token' => str_random(60)
            );
            $this->sendMail($dadosEmail);

            return redirect()->route('area-do-cliente.usuarios')->with('success', 'Usuário adicionado com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar usuário: ' . $e->getMessage()]);
        }
    }

    public function edit($usuario_id)
    {
        $usuario = Admin::where('id', $usuario_id)->first();

        return view('area-do-cliente.admin.usuarios.edit', compact('usuario'));
    }

    public function update(ACAdminsRequest $request, $usuario_id)
    {
        try {
            $usuario = Admin::where('id', $usuario_id)->first();
            
            $data = $request->all();
            $data['password'] = bcrypt('alterar#123');
            $data['ativo'] = 0;

            $usuario->update($data);

            $dadosEmail = array(
                'email' => $usuario->email,
                'nome' => $usuario->nome,
                'token' => str_random(60)
            );
            $this->sendMail($dadosEmail);

            return redirect()->route('area-do-cliente.usuarios')->with('success', 'Usuário alterado com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar usuário: ' . $e->getMessage()]);
        }
    }

    public function destroy($usuario_id)
    {
        try {
            $usuario = Admin::where('id', $usuario_id)->first();
            $usuario->delete();

            return redirect()->route('area-do-cliente.usuarios')->with('success', 'Usuário excluído com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar usuário: ' . $e->getMessage()]);
        }
    }

    private function sendMail($dadosEmail)
    {
        if (!$email = Contato::first()->email) {
            return false;
        }

        Mail::send('area-do-cliente.emails.admin-ativar-cadastro', $dadosEmail, function ($m) use ($email, $dadosEmail) {
            $m->to($dadosEmail['email'], $dadosEmail['nome'])
                ->subject('[ATIVAR CADASTRO] GRUPO FG MORETTI')
                ->replyTo($email, 'GRUPO FG MORETTI');
        });
    }
}
