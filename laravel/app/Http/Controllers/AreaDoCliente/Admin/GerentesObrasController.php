<?php

namespace App\Http\Controllers\AreaDoCliente\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\AreaDoCliente\ClienteUsuario;
use App\Models\AreaDoCliente\Empresa;
use App\Models\AreaDoCliente\GerenteObra;
use App\Models\AreaDoCliente\Obra;

class GerentesObrasController extends Controller
{
    public function index($empresa_slug, $gerente_slug)
    {
        $empresa = Empresa::where('slug', $empresa_slug)->first();
        $gerente = ClienteUsuario::gerente()->empresa($empresa->id)->where('slug', $gerente_slug)->first();
        $obrasGerente = GerenteObra::where('usuario_id', $gerente->id)
            ->join('ac_obras', 'ac_obras.id', '=', 'ac_gerentes_obras.obra_id')
            ->orderBy('ac_obras.titulo', 'asc')->get();

        return view('area-do-cliente.admin.gerentes.obras.index', compact('empresa', 'gerente', 'obrasGerente'));
    }

    public function create($empresa_slug, $gerente_slug)
    {
        $empresa = Empresa::where('slug', $empresa_slug)->first();
        $gerente = ClienteUsuario::gerente()->empresa($empresa->id)->where('slug', $gerente_slug)->first();

        $obras = Obra::orderBy('titulo', 'asc')->get();

        return view('area-do-cliente.admin.gerentes.obras.create', compact('empresa', 'gerente', 'obras'));
    }

    public function store(Request $request, $empresa_slug, $gerente_slug)
    {
        try {
            $empresa = Empresa::where('slug', $empresa_slug)->first();
            $gerente = ClienteUsuario::gerente()->empresa($empresa->id)->where('slug', $gerente_slug)->first();

            $data = $request->all();
            $data['usuario_id'] = $gerente->id;

            $count = count(GerenteObra::where('usuario_id', $data['usuario_id'])->where('obra_id', $data['obra_id'])->get());

            if ($count == 0) {
                GerenteObra::create($data);
            } else {
                return back()->withErrors(['Erro ao adicionar registro: Verifique se essa obra ja está cadastrada.']);
            }

            return redirect()->route('area-do-cliente.gerentes.obras', [$empresa->slug, $gerente->slug])->with('success', 'Obra adicionada ao gerente com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar obra: ' . $e->getMessage()]);
        }
    }

    public function destroy($empresa_slug, $gerente_slug, $obra_slug)
    {
        try {
            $empresa = Empresa::where('slug', $empresa_slug)->first();
            $gerente = ClienteUsuario::gerente()->where('slug', $gerente_slug)->where('empresa_id', $empresa->id)->first();
            $obraGerente = GerenteObra::join('ac_obras', 'ac_obras.id', '=', 'ac_gerentes_obras.obra_id')
                ->where('ac_obras.slug', $obra_slug)
                ->where('usuario_id', $gerente->id)
                ->first();

            $obraGerente->delete();

            return redirect()->route('area-do-cliente.gerentes.obras', [$empresa->slug, $gerente->slug])->with('success', 'Obra excluída com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar obra: ' . $e->getMessage()]);
        }
    }
}
