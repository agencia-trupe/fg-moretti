<?php

namespace App\Http\Controllers\AreaDoCliente\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ACEmpresasRequest;
use App\Models\AreaDoCliente\Empresa;

class EmpresasController extends Controller
{
    public function index()
    {
        $empresas = Empresa::orderBy('nome', 'asc')->get();

        return view('area-do-cliente.admin.empresas.index', compact('empresas'));
    }

    public function create()
    {
        return view('area-do-cliente.admin.empresas.create');
    }

    public function store(ACEmpresasRequest $request)
    {
        try {
            $data = $request->all();
            $data['slug'] = str_slug($request->nome, "-");

            Empresa::create($data);

            return redirect()->route('area-do-cliente.empresas')->with('success', 'Empresa adicionada com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar empresa: ' . $e->getMessage()]);
        }
    }

    public function edit($empresa_slug)
    {
        $empresa = Empresa::where('slug', $empresa_slug)->first();

        return view('area-do-cliente.admin.empresas.edit', compact('empresa'));
    }

    public function update(ACEmpresasRequest $request, $empresa_slug)
    {
        try {
            $empresa = Empresa::where('slug', $empresa_slug)->first();

            $data = $request->all();
            $data['slug'] = str_slug($request->nome, "-");

            $empresa->update($data);

            return redirect()->route('area-do-cliente.empresas')->with('success', 'Empresa alterada com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar empresa: ' . $e->getMessage()]);
        }
    }

    public function destroy($empresa_slug)
    {
        try {
            $empresa = Empresa::where('slug', $empresa_slug)->first();
            $empresa->delete();

            return redirect()->route('area-do-cliente.empresas')->with('success', 'Empresa excluída com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar empresa: ' . $e->getMessage()]);
        }
    }
}
