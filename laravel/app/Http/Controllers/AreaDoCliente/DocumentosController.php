<?php

namespace App\Http\Controllers\AreaDoCliente;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\AreaDoCliente\ClienteUsuario;
use App\Models\AreaDoCliente\Documento;
use App\Models\AreaDoCliente\DocumentoVersao;
use App\Models\AreaDoCliente\Empresa;
use App\Models\AreaDoCliente\Obra;
use App\Models\AreaDoCliente\ObraComunicacao;
use App\Models\Contato;
use Illuminate\Support\Facades\Mail;

class DocumentosController extends Controller
{
    public function index()
    {
        $usuarioLogado = auth('clientes')->user();

        if ($usuarioLogado->gerente == 1) {
            $obras = Obra::join('ac_gerentes_obras', 'ac_gerentes_obras.obra_id', '=', 'ac_obras.id')
                ->where('ac_gerentes_obras.usuario_id', $usuarioLogado->id)
                ->select('ac_obras.*')
                ->distinct()->orderBy('ac_obras.titulo', 'asc')->get();
        } else {
            $obras = Obra::join('ac_gerentes_obras_usuarios', 'ac_gerentes_obras_usuarios.obra_id', '=', 'ac_obras.id')
                ->where('ac_gerentes_obras_usuarios.usuario_id', $usuarioLogado->id)
                ->select('ac_obras.*')
                ->distinct()->orderBy('ac_obras.titulo', 'asc')->get();
        }

        $documentos = Documento::join('ac_documentos_versoes', 'ac_documentos_versoes.documento_id', '=', 'ac_documentos.id')
            ->select('ac_documentos.obra_id', 'ac_documentos.id', 'ac_documentos_versoes.versao', 'ac_documentos_versoes.slug', 'ac_documentos_versoes.titulo', 'ac_documentos_versoes.arquivo', 'ac_documentos_versoes.updated_at')
            ->distinct()->orderBy('ac_documentos_versoes.updated_at', 'desc')->get();

        // dd($documentos);

        return view('area-do-cliente.documentos', compact('usuarioLogado', 'obras', 'documentos'));
    }

    public function showObraDocs($obra_slug)
    {
        $obra = Obra::where('slug', $obra_slug)->first();

        $documentos = Documento::where('obra_id', $obra->id)->orderBy('titulo', 'asc')->get();

        $versoes = DocumentoVersao::join('ac_documentos', 'ac_documentos.id', '=', 'ac_documentos_versoes.documento_id')
            ->where('ac_documentos.obra_id', $obra->id)
            ->select('ac_documentos_versoes.*')
            ->orderBy('updated_at', 'desc')->get();

        // dd($versoes);

        return view('area-do-cliente.documentos-obra', compact('obra', 'documentos', 'versoes'));
    }

    public function showObraDocsVersoes($obra_slug, $doc_id)
    {
        $obra = Obra::where('slug', $obra_slug)->first();

        $documento = Documento::where('id', $doc_id)->where('obra_id', $obra->id)->first();

        $versoes = DocumentoVersao::where('documento_id', $documento->id)->orderBy('updated_at', 'desc')->get();

        // dd($versoes);

        return view('area-do-cliente.versoes-documento', compact('obra', 'documento', 'versoes'));
    }

    public function showComunicacaoObra($obra_slug)
    {
        $obra = Obra::where('slug', $obra_slug)->first();
        $usuarioLogado = auth('clientes')->user();

        $comunicacoes = ObraComunicacao::where('obra_id', $obra->id)->orderBy('created_at', 'desc')->get();

        $empresas = Empresa::get();

        return view('area-do-cliente.comunicacao', compact('obra', 'usuarioLogado', 'comunicacoes', 'empresas'));
    }

    public function postComunicacaoObra(Request $request, $obra_slug)
    {
        try {
            $obra = Obra::where('slug', $obra_slug)->first();

            $data = $request->all();
            $data['obra_id'] = $obra->id;

            if ($request->admin_id == "null") {
                $data['admin_id'] = null;
            }
            if ($request->empresa_id == "null") {
                $data['empresa_id'] = null;
            }
            if ($request->usuario_id == "null") {
                $data['usuario_id'] = null;
            } else {
                $comunicacoes = ObraComunicacao::where('obra_id', $obra->id)->where('admin_id', '!=', null)->get();
                foreach ($comunicacoes as $c) {
                    $c['respondido'] = 1;
                    $c->save();
                }
            }

            // dd($data);

            ObraComunicacao::create($data);

            $this->sendMailComunicacao($obra);

            return redirect()->route('area-do-cliente.clientes.obra.comunicacao', $obra->slug)->with('success', 'Mensagem enviada com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao enviar mensagem: ' . $e->getMessage()]);
        }
    }

    public function destroy($obra_slug, $comunicacao_id)
    {
        try {
            $obra = Obra::where('slug', $obra_slug)->first();

            $comunicacao = ObraComunicacao::where('id', $comunicacao_id)->where('obra_id', $obra->id)->first();
            $comunicacao->delete();

            return redirect()->route('area-do-cliente.clientes.obra.comunicacao', $obra->slug)->with('success', 'Mensagem excluída com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir mensagem: ' . $e->getMessage()]);
        }
    }

    public function download($versao_id)
    {
        $arquivo = DocumentoVersao::where('id', $versao_id)->select('arquivo')->first();

        $arquivoPath = public_path() . "/assets/obras-documentos/" . $arquivo->arquivo;

        return response()->download($arquivoPath);
    }

    public function buscar(Request $request)
    {
        if ($request->has('palavraChave')) {
            $palavra = $request->palavraChave;

            $resultados = Documento::join('ac_documentos_versoes', 'ac_documentos_versoes.documento_id', '=', 'ac_documentos.id')
                ->join('ac_obras', 'ac_obras.id', '=', 'ac_documentos.obra_id')
                ->select('ac_documentos.id', 'ac_documentos.slug as doc_slug', 'ac_documentos.titulo as doc_titulo', 'ac_documentos.descricao', 'ac_obras.id as obra_id', 'ac_obras.slug as obra_slug', 'ac_documentos_versoes.id as versao_id', 'ac_documentos_versoes.versao', 'ac_documentos_versoes.slug as versao_slug', 'ac_documentos_versoes.titulo as versao_titulo', 'ac_documentos_versoes.arquivo', 'ac_documentos_versoes.updated_at')
                ->where(function ($query) use ($palavra) {
                    $query->where('ac_documentos.titulo', 'LIKE', "%" . $palavra . "%")
                        ->orWhere('ac_documentos.descricao', 'LIKE', "%" . $palavra . "%")
                        ->orWhere('ac_documentos_versoes.titulo', 'LIKE', "%" . $palavra . "%")
                        ->orWhere('ac_documentos_versoes.arquivo', 'LIKE', "%" . $palavra . "%");
                })->get();
        }
        // dd($resultados);

        return view('area-do-cliente.resultados-busca', compact('palavra', 'resultados'));
    }

    private function sendMailComunicacao($obra)
    {
        if (!$email = Contato::first()->email) {
            return false;
        }

        $usuariosObra = ClienteUsuario::join('ac_gerentes_obras_usuarios', 'ac_gerentes_obras_usuarios.usuario_id', '=', 'ac_clientes_usuarios.id')
            ->select('ac_clientes_usuarios.id', 'ac_clientes_usuarios.email', 'ac_clientes_usuarios.nome', 'ac_clientes_usuarios.ativo')
            ->where('ac_gerentes_obras_usuarios.obra_id', $obra->id)
            ->where('ac_clientes_usuarios.ativo', 1)
            ->get();
        $gerentesObra = ClienteUsuario::join('ac_gerentes_obras', 'ac_gerentes_obras.usuario_id', '=', 'ac_clientes_usuarios.id')
            ->select('ac_clientes_usuarios.id', 'ac_clientes_usuarios.email', 'ac_clientes_usuarios.nome', 'ac_clientes_usuarios.ativo')
            ->where('ac_gerentes_obras.obra_id', $obra->id)
            ->where('ac_clientes_usuarios.ativo', 1)
            ->get();

        $emails = [];
        foreach ($usuariosObra as $usuario) {
            $emails[] = $usuario->email;
        }
        foreach ($gerentesObra as $usuario) {
            $emails[] = $usuario->email;
        }
        $emails[] = $email;
        $emails = array_unique($emails);

        Mail::send('area-do-cliente.emails.nova-comunicacao', ['emails' => $emails, 'obra' => $obra], function ($m) use ($email, $emails) {
            $m->to($emails)
                ->subject('[NOVA MENSAGEM - COMUNICAÇÃO] GRUPO FG MORETTI')
                ->replyTo($email, 'GRUPO FG MORETTI');
        });
    }
}
