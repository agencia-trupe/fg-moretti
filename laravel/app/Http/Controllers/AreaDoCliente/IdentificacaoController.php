<?php

namespace App\Http\Controllers\AreaDoCliente;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\AreaDoCliente\ClienteUsuario;
use App\Models\AreaDoCliente\Empresa;
use App\Models\AreaDoCliente\GerenteObraUsuario;
use App\Models\AreaDoCliente\Obra;
use App\Models\Contato;
use Illuminate\Support\Facades\Mail;

class IdentificacaoController extends Controller
{
    public function index()
    {
        $usuario = auth('clientes')->user();
        $empresa = Empresa::where('id', $usuario->empresa_id)->first();
        $obras = Obra::join('ac_gerentes_obras', 'ac_gerentes_obras.obra_id', '=', 'ac_obras.id')
            ->where('ac_gerentes_obras.usuario_id', $usuario->id)
            ->select('ac_obras.*')
            ->orderBy('ac_obras.titulo', 'asc')->get();

        if ($usuario->gerente == 0) {
            $gerente = ClienteUsuario::join('ac_gerentes_obras_usuarios', 'ac_gerentes_obras_usuarios.gerente_id', '=', 'ac_clientes_usuarios.id')
                ->where('ac_gerentes_obras_usuarios.usuario_id', $usuario->id)
                ->where('ac_clientes_usuarios.gerente', 1)
                ->select('ac_clientes_usuarios.*')
                ->first();
            $obras = Obra::join('ac_gerentes_obras_usuarios', 'ac_gerentes_obras_usuarios.obra_id', '=', 'ac_obras.id')
                ->where('ac_gerentes_obras_usuarios.usuario_id', $usuario->id)
                ->where('ac_gerentes_obras_usuarios.gerente_id', $gerente->id)
                ->select('ac_obras.*')
                ->get();
        }

        return view('area-do-cliente.identificacao', compact('usuario', 'empresa', 'obras', 'gerente'));
    }

    public function update(Request $request, $usuario_slug)
    {
        try {
            $this->validate($request, [
                'nome'     => '',
                'email'    => 'email|max:255',
                'telefone' => '',
                'password' => 'confirmed|min:6'
            ]);

            $usuario = ClienteUsuario::where('slug', $usuario_slug)->first();

            $usuario->nome = $request->nome;
            if ($usuario->email != $request->email) {
                $usuario->email = $request->email;
            }
            $usuario->telefone = $request->telefone;
            $usuario->password = bcrypt($request->password);

            $usuario->save();

            return redirect()->route('area-do-cliente.clientes.identificacao')->with('success', 'Dados atualizados com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao atualizar cadastro: ' . $e->getMessage()]);
        }
    }

    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'email'    => 'required|email|max:255',
            ]);

            $usuario = ClienteUsuario::where('email', $request->email)->first();

            if (isset($usuario)) {
                if ($usuario->gerente == 1) {
                    return back()->withErrors(['Erro ao adicionar usuário: Usuário cadastrado como gerente']);
                }

                $usuarioGerentes = GerenteObraUsuario::join('ac_clientes_usuarios', 'ac_clientes_usuarios.id', '=', 'ac_gerentes_obras_usuarios.usuario_id')
                    ->where('usuario_id', $usuario->id)
                    ->select('ac_gerentes_obras_usuarios.gerente_id')->get();

                if (count($usuarioGerentes) > 1) {
                    return back()->withErrors(['Erro ao adicionar usuário: Usuário já cadastrado']);
                } else {
                    $gerente = ClienteUsuario::where('id', $request->gerente_id)->first();
                    $dadosLigacao = [
                        'gerente_id' => $gerente->id,
                        'obra_id'    => $request->obra_id,
                        'usuario_id' => $usuario->id,
                    ];

                    GerenteObraUsuario::create($dadosLigacao);

                    $dadosEmail = array(
                        'email' => $usuario->email,
                        'nome' => $usuario->nome,
                        'gerente' => $gerente->nome,
                        'token' => str_random(60)
                    );
                    $this->sendMailJaCadastrado($dadosEmail);

                    return back()->with('success', 'Usuário já possui cadastro e foi adicionado à obra com sucesso!');
                }
            } else {
                $dadosUsuario = [
                    'empresa_id' => $request->empresa_id,
                    'slug'       => "",
                    'nome'       => "",
                    'telefone'   => "",
                    'email'      => $request->email,
                    'password'   => bcrypt('alterar#123'),
                    'ativo'      => 0,
                    'gerente'    => 0,
                ];

                $usuarioNovo = ClienteUsuario::create($dadosUsuario);

                $dadosLigacao = [
                    'gerente_id' => $request->gerente_id,
                    'obra_id'    => $request->obra_id,
                    'usuario_id' => $usuarioNovo->id,
                ];

                GerenteObraUsuario::create($dadosLigacao);

                $dadosEmail = array(
                    'email' => $usuarioNovo->email,
                    'nome' => $usuarioNovo->nome,
                    'token' => str_random(60)
                );
                $this->sendMail($dadosEmail);

                return redirect()->route('area-do-cliente.clientes.identificacao')->with('success', 'Usuário adicionado com sucesso!');
            }
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar usuário: ' . $e->getMessage()]);
        }
    }

    public function getUsuariosObra($obra_id, $gerente_id)
    {
        $obra = Obra::where('id', $obra_id)->first();
        $gerente = ClienteUsuario::gerente()->where('id', $gerente_id)->first();

        $usuarios = ClienteUsuario::join('ac_gerentes_obras_usuarios', 'ac_gerentes_obras_usuarios.usuario_id', '=', 'ac_clientes_usuarios.id')
            ->where('ac_gerentes_obras_usuarios.obra_id', $obra->id)
            ->where('ac_gerentes_obras_usuarios.gerente_id', $gerente->id)
            ->select('ac_clientes_usuarios.*')
            ->orderBy('ac_clientes_usuarios.nome')->get();

        return response()->json(['usuarios' => $usuarios, 'obra' => $obra, 'gerente' => $gerente]);
    }

    public function destroy($gerente_id, $obra_id, $usuario_id)
    {
        try {
            $usuario = GerenteObraUsuario::where('gerente_id', $gerente_id)->where('obra_id', $obra_id)->where('usuario_id', $usuario_id)->first();
            $usuario->delete();

            return redirect()->route('area-do-cliente.clientes.identificacao')->with('success', 'Usuário excluído da obra com sucesso!');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir ligação de usuário com obra: ' . $e->getMessage()]);
        }
    }

    private function sendMail($dadosEmail)
    {
        if (!$email = Contato::first()->email) {
            return false;
        }

        Mail::send('area-do-cliente.emails.usuario-ativar-cadastro', $dadosEmail, function ($m) use ($email, $dadosEmail) {
            $m->to($dadosEmail['email'], $dadosEmail['nome'])
                ->subject('[ATIVAR CADASTRO] GRUPO FG MORETTI')
                ->replyTo($email, 'GRUPO FG MORETTI');
        });
    }

    private function sendMailJaCadastrado($dadosEmail)
    {
        if (!$email = Contato::first()->email) {
            return false;
        }

        Mail::send('area-do-cliente.emails.usuario-ja-cadastrado', $dadosEmail, function ($m) use ($email, $dadosEmail) {
            $m->to($dadosEmail['email'], $dadosEmail['nome'])
                ->subject('[NOVA OBRA] GRUPO FG MORETTI')
                ->replyTo($email, 'GRUPO FG MORETTI');
        });
    }
}
