<?php

namespace App\Http\Controllers\AreaDoCliente\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use App\Models\AreaDoCliente\Admin;
use Illuminate\Support\Facades\DB;

class PasswordController extends Controller
{
    use ResetsPasswords;

    protected $broker = 'clientes';
    protected $redirectPath = 'area-do-cliente/login';
    protected $linkRequestView = 'area-do-cliente.auth.esqueci-minha-senha';
    protected $resetView = 'area-do-cliente.auth.reset';

    public function __construct()
    {
        $this->middleware('clientes.guest');
    }
}
