<?php

namespace App\Http\Controllers\AreaDoCliente\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $guard = 'clientes';
    protected $redirectTo = 'area-do-cliente';
    protected $redirectAfterLogout = 'area-do-cliente/login';
    protected $loginView = 'area-do-cliente.auth.login';

    public function __construct()
    {
        Auth::guard()->logout();
        Auth::guard('admin')->logout();
        $this->middleware('clientes.guest', ['except' => 'logout']);
    }
}
