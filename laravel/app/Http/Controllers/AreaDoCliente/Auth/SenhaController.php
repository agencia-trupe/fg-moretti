<?php

namespace App\Http\Controllers\AreaDoCliente\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ACGerentesRequest;
use App\Models\AreaDoCliente\ClienteUsuario;
use App\Models\AreaDoCliente\Gerente;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SenhaController extends Controller
{
    public function edit($email, $token)
    {
        Auth::guard()->logout();
        Auth::guard('admin')->logout();
        Auth::guard('clientes')->logout();
        return view('area-do-cliente.auth.alterar-senha', compact('token', 'email'));
    }

    public function update(Request $request)
    {
        try {
            $this->validate($request, [
                'password'   => 'required',
                'nova_senha' => 'required|confirmed|min:6'
            ]);

            $gerente = ClienteUsuario::where('email', $request->email)->first();

            $verificacaoSenhas = Hash::check($request->password, $gerente->password);
            if (!$verificacaoSenhas) {
                return back()->withErrors('Senha atual inválida.');
            }

            $gerente->password = bcrypt($request->nova_senha);
            $gerente->ativo = 1;

            $gerente->save();

            if ($gerente->ativo == 1) {
                Auth::guard('clientes')->login($gerente);
            }

            return redirect()->route('area-do-cliente.clientes.identificacao')->with('success', 'Senha alterada com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao redefinir senha: As senhas devem ser iguais e devem ter no mínimo 6 caracteres.']);
        }
    }

    public function editUsuario($email, $token)
    {
        Auth::guard()->logout();
        Auth::guard('admin')->logout();
        Auth::guard('clientes')->logout();
        return view('area-do-cliente.auth.ativar-cadastro', compact('token', 'email'));
    }

    public function updateUsuario(Request $request)
    {
        try {
            $this->validate($request, [
                'nome'       => 'required',
                'email'      => 'required|email|max:255',
                'telefone'   => 'required',
                'password'   => 'required',
                'nova_senha' => 'required|confirmed|min:6'
            ]);

            $usuario = ClienteUsuario::where('email', $request->email)->first();

            $verificacaoSenhas = Hash::check($request->password, $usuario->password);
            if (!$verificacaoSenhas) {
                return back()->withErrors('Senha atual inválida.');
            }

            $usuario->slug     = str_slug($request->nome, "-");
            $usuario->nome     = $request->nome;
            $usuario->telefone = $request->telefone;
            $usuario->password = bcrypt($request->nova_senha);
            $usuario->ativo    = 1;

            $usuario->save();

            if ($usuario->ativo == 1) {
                Auth::guard('clientes')->login($usuario);
            }

            return redirect()->route('area-do-cliente.clientes.identificacao')->with('success', 'Cadastro atualizado com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao atualizar cadastro: ' . $e->getMessage()]);
        }
    }
}
