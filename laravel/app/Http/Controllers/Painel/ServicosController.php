<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ServicosRequest;
use App\Models\Servico;
use App\Models\ServicosGrupo;

class ServicosController extends Controller
{
    public function index()
    {
        $servicos = Servico::orderBy('titulo', 'ASC')
            ->join('servicos_grupos', 'servicos_grupos.id', '=', 'servicos.grupo_id')
            ->select('servicos_grupos.titulo as grupo_titulo', 'servicos_grupos.id as grupo_id', 'servicos.id as id', 'servicos.titulo as titulo', 'servicos.slug as slug')
            ->paginate(10);

        return view('painel.servicos.index', compact('servicos'));
    }

    public function create()
    {
        $grupos = ServicosGrupo::orderBy('titulo', 'ASC')->pluck('titulo', 'id');

        return view('painel.servicos.create', compact('grupos'));
    }

    public function store(ServicosRequest $request)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            $count = count(Servico::where('slug', $input['slug'])->where('grupo_id', $input['grupo_id'])->get());

            if ($count == 0) {
                Servico::create($input);
            } else {
                return back()->withErrors(['Erro ao adicionar registro: Verifique se esse serviço já está cadastrado']);
            }

            return redirect()->route('painel.servicos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Servico $servico)
    {
        $grupos = ServicosGrupo::orderBy('titulo', 'ASC')->pluck('titulo', 'id');

        return view('painel.servicos.edit', compact('servico', 'grupos'));
    }

    public function update(ServicosRequest $request, Servico $servico)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            $servico->update($input);

            return redirect()->route('painel.servicos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Servico $servico)
    {
        try {
            $servico->delete();

            return redirect()->route('painel.servicos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
