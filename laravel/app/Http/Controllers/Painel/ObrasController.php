<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ObrasRequest;
use App\Models\Aplicacao;
use App\Models\Obra;

class ObrasController extends Controller
{
    public function index()
    {
        $obras = Obra::join('aplicacoes', 'aplicacoes.id', '=', 'obras.aplicacao_id')
            ->select('aplicacoes.id as aplicacao_id', 'aplicacoes.titulo as aplicacao_titulo', 'obras.id as id', 'obras.titulo as titulo', 'obras.texto as texto', 'obras.capa as capa', 'obras.ordem as ordem')
            ->ordenados()->paginate(10);

        return view('painel.galeria-de-imagens.index', compact('obras'));
    }

    public function create()
    {
        $aplicacoes = Aplicacao::orderBy('titulo', 'ASC')->pluck('titulo', 'id');

        return view('painel.galeria-de-imagens.create', compact('aplicacoes'));
    }

    public function store(ObrasRequest $request)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            if (isset($input['capa'])) $input['capa'] = Obra::upload_capa();

            Obra::create($input);

            return redirect()->route('painel.galeria-de-imagens.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit($obra)
    {
        $obra = Obra::find($obra);

        $aplicacoes = Aplicacao::orderBy('titulo', 'ASC')->pluck('titulo', 'id');

        return view('painel.galeria-de-imagens.edit', compact('obra', 'aplicacoes'));
    }

    public function update(ObrasRequest $request, $obra)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            if (isset($input['capa'])) $input['capa'] = Obra::upload_capa();

            $obra = Obra::find($obra);

            $obra->update($input);

            return redirect()->route('painel.galeria-de-imagens.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy($obra)
    {
        try {
            $obra = Obra::find($obra);

            $obra->delete();

            return redirect()->route('painel.galeria-de-imagens.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
