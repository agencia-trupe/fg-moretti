<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmpresaCorpoTecnicoRequest;
use App\Models\EmpresaCorpoTecnico;

class EmpresaCorpoTecnicoController extends Controller
{
    public function index()
    {
        $registro = EmpresaCorpoTecnico::first();

        return view('painel.empresa.corpo-tecnico.index', compact('registro'));
    }

    public function edit(EmpresaCorpoTecnico $registro)
    {
        return view('painel.empresa.corpo-tecnico.edit', compact('registro'));
    }

    public function update(EmpresaCorpoTecnicoRequest $request, EmpresaCorpoTecnico $registro)
    {
        try {
            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.empresa-corpo-tecnico.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
