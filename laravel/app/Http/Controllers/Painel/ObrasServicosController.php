<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Obra;
use App\Models\ObraServico;
use App\Models\Servico;

class ObrasServicosController extends Controller
{
    public function index($obra_id)
    {
        $obra = Obra::find($obra_id);

        $servicos = ObraServico::obra($obra->id)
            ->join('servicos', 'servicos.id', '=', 'obras_servicos.servico_id')
            ->select('servicos.id as servico_id', 'servicos.titulo as servico_titulo', 'obras_servicos.id as id')
            ->orderBy('servicos.titulo', 'ASC')
            ->get();

        return view('painel.galeria-de-imagens.servicos.index', compact('servicos', 'obra'));
    }

    public function create($obra_id)
    {
        $obra = Obra::find($obra_id);

        $servicos = Servico::orderBy('titulo', 'ASC')->pluck('titulo', 'id');

        return view('painel.galeria-de-imagens.servicos.create', compact('obra', 'servicos'));
    }

    public function store(Request $request, $obra_id)
    {
        try {
            $obra = Obra::find($obra_id);

            $input = $request->all();
            $input['obra_id'] = $obra->id;

            $count = count(ObraServico::where('obra_id', $input['obra_id'])->where('servico_id', $input['servico_id'])->get());

            if ($count == 0) {
                ObraServico::create($input);
            } else {
                return back()->withErrors(['Erro ao adicionar registro: Verifique se esse serviço ja está cadastrado']);
            }

            return redirect()->route('painel.galeria-de-imagens.servicos.index', $obra->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy($obra_id, $servico_id)
    {
        try {
            $servico = ObraServico::where('id', $servico_id)->where('obra_id', $obra_id)->first();

            $servico->delete();

            return redirect()->route('painel.galeria-de-imagens.servicos.index', $obra_id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
