<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\AreasDeNegocioRequest;
use App\Models\AreaDeNegocio;

class AreasDeNegocioController extends Controller
{
    public function index()
    {
        $areas = AreaDeNegocio::orderBy('id', 'ASC')->get();

        return view('painel.areas-de-negocio.index', compact('areas'));
    }

    public function edit(AreaDeNegocio $area)
    {
        return view('painel.areas-de-negocio.edit', compact('area'));
    }

    public function update(AreasDeNegocioRequest $request, AreaDeNegocio $area)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($area->titulo, '-');

            if (isset($input['imagem'])) $input['imagem'] = AreaDeNegocio::upload_imagem();

            $area->update($input);

            return redirect()->route('painel.areas-de-negocio.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
