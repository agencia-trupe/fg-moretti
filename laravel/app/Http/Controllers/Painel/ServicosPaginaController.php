<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ServicosPaginaRequest;
use App\Models\ServicosPagina;

class ServicosPaginaController extends Controller
{
    public function index()
    {
        $registro = ServicosPagina::first();

        return view('painel.servicos.pagina.edit', compact('registro'));
    }

    public function update(ServicosPaginaRequest $request, ServicosPagina $registro)
    {
        try {
            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.servicos-pagina.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
