<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\AreaDeNegocio;
use App\Models\Obra;
use App\Models\ObraArea;
use App\Models\ObraDestaque;

class ObrasDestaqueController extends Controller
{
    public function index()
    {
        $destaques = ObraDestaque::join('obras', 'obras.id', '=', 'obras_destaque.obra_id')
            ->select('obras_destaque.id as id', 'obras_destaque.ordem as ordem', 'obras.id as obra_id', 'obras.titulo as titulo', 'obras.capa as capa')
            ->ordenados()->get();

        return view('painel.home.obras-destaque.index', compact('destaques'));
    }

    public function create()
    {
        $obras = Obra::ordenados()->pluck('titulo', 'id');

        return view('painel.home.obras-destaque.create', compact('obras'));
    }

    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $count = count(ObraDestaque::where('obra_id', $input['obra_id'])->get());

            if ($count == 0) {
                ObraDestaque::create($input);
            } else {
                return back()->withErrors(['Erro ao adicionar registro: Verifique se essa obra ja está cadastrada']);
            }

            return redirect()->route('painel.obras-destaque.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(ObraDestaque $obra)
    {
        try {
            $obra->delete();

            return redirect()->route('painel.obras-destaque.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
