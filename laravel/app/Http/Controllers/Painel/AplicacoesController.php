<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\AplicacoesRequest;
use App\Models\Aplicacao;

class AplicacoesController extends Controller
{
    public function index()
    {
        $aplicacoes = Aplicacao::orderBy('titulo', 'ASC')->get();

        return view('painel.aplicacoes.index', compact('aplicacoes'));
    }

    public function create()
    {
        return view('painel.aplicacoes.create');
    }

    public function store(AplicacoesRequest $request)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            if (isset($input['imagem'])) $input['imagem'] = Aplicacao::upload_imagem();

            Aplicacao::create($input);

            return redirect()->route('painel.aplicacoes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Aplicacao $aplicacao)
    {
        return view('painel.aplicacoes.edit', compact('aplicacao'));
    }

    public function update(AplicacoesRequest $request, Aplicacao $aplicacao)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            if (isset($input['imagem'])) $input['imagem'] = Aplicacao::upload_imagem();

            $aplicacao->update($input);

            return redirect()->route('painel.aplicacoes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Aplicacao $aplicacao)
    {
        try {
            $aplicacao->delete();

            return redirect()->route('painel.aplicacoes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
