<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ObrasImagensRequest;
use App\Models\Obra;
use App\Models\ObraImagem;

class ObrasImagensController extends Controller
{
    public function index($obra)
    {
        $obra = Obra::find($obra);

        $imagens = ObraImagem::obra($obra->id)->ordenados()->get();

        return view('painel.galeria-de-imagens.imagens.index', compact('imagens', 'obra'));
    }

    public function show(Obra $obra, ObraImagem $imagem)
    {
        return $imagem;
    }

    public function store($obra, ObrasImagensRequest $request)
    {
        try {
            $obra = Obra::find($obra);

            $input = $request->all();
            $input['obra_id'] = $obra->id;

            if (isset($input['imagem'])) $input['imagem'] = ObraImagem::upload_imagem();

            $imagem = ObraImagem::create($input);

            $view = view('painel.galeria-de-imagens.imagens.imagem', compact('obra', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: ' . $e->getMessage();
        }
    }

    public function destroy($obra, $imagem)
    {
        try {
            $obra = Obra::find($obra);

            $imagem = ObraImagem::find($imagem);
            $imagem->delete();

            return redirect()->route('painel.galeria-de-imagens.imagens.index', $obra)
                ->with('success', 'Imagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: ' . $e->getMessage()]);
        }
    }
}
