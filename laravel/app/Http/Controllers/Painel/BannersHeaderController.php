<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\BannersHeaderRequest;
use App\Models\BannerHeader;

class BannersHeaderController extends Controller
{
    public function index()
    {
        $banners = BannerHeader::ordenados()->get();

        return view('painel.home.banners.index', compact('banners'));
    }

    public function create()
    {
        return view('painel.home.banners.create');
    }

    public function store(BannersHeaderRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = BannerHeader::upload_imagem();

            BannerHeader::create($input);

            return redirect()->route('painel.banners-header.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(BannerHeader $banner)
    {
        return view('painel.home.banners.edit', compact('banner'));
    }

    public function update(BannersHeaderRequest $request, BannerHeader $banner)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = BannerHeader::upload_imagem();

            $banner->update($input);

            return redirect()->route('painel.banners-header.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(BannerHeader $banner)
    {
        try {
            $banner->delete();

            return redirect()->route('painel.banners-header.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
