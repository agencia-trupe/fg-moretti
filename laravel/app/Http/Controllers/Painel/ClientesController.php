<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClientesRequest;
use App\Models\Cliente;

class ClientesController extends Controller
{
    public function index()
    {
        $clientes = Cliente::orderBy('nome', 'ASC')->paginate(10);

        return view('painel.clientes.index', compact('clientes'));
    }

    public function create()
    {
        return view('painel.clientes.create');
    }

    public function store(ClientesRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Cliente::upload_imagem();

            Cliente::create($input);

            return redirect()->route('painel.clientes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Cliente $cliente)
    {
        return view('painel.clientes.edit', compact('cliente'));
    }

    public function update(ClientesRequest $request, Cliente $cliente)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Cliente::upload_imagem();

            $cliente->update($input);

            return redirect()->route('painel.clientes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Cliente $cliente)
    {
        try {
            $cliente->delete();

            return redirect()->route('painel.clientes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
