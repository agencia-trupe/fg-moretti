<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\AreaClientesRequest;
use App\Models\AreaCliente;
use Illuminate\Support\Facades\Auth;

class AreaClientesController extends Controller
{
    public function index() 
    {
        $empresas = AreaCliente::orderBy('empresa', 'ASC')->get();

        return view('painel.area-do-cliente.empresas.index', compact('empresas'));
    }

    public function create()
    {
        return view('painel.area-do-cliente.empresas.create');
    }

    public function store(AreaClientesRequest $request)
    {
        try {
            $input = $request->except('senha_confirmation');
            $input['senha'] = bcrypt($request->get('senha'));

            $empresa = AreaCliente::create($input);

            Auth::guard('cliente')->loginUsingId($empresa->id);

            return redirect()->route('painel.area-do-cliente.empresas.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit($id)
    {
        $empresa = AreaCliente::find($id);
        
        return view('painel.area-do-cliente.empresas.edit', compact('empresa'));
    }

    public function update(AreaClientesRequest $request, $id)
    {
        try {
            $input = $request->all();
            $input = $request->except('senha_confirmation');
            $input['senha'] = bcrypt($request->get('senha'));

            $empresa = AreaCliente::find($id);

            $empresa->update($input);

            return redirect()->route('painel.area-do-cliente.empresas.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy($id)
    {
        try {
            $empresa = AreaCliente::find($id);
            
            $empresa->delete();

            return redirect()->route('painel.area-do-cliente.empresas.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
