<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ServicosGruposRequest;
use App\Models\ServicosGrupo;

class ServicosGruposController extends Controller
{
    public function index()
    {
        $grupos = ServicosGrupo::orderBy('titulo', 'ASC')->paginate(10);

        return view('painel.servicos.grupos.index', compact('grupos'));
    }

    public function create()
    {
        return view('painel.servicos.grupos.create');
    }

    public function store(ServicosGruposRequest $request)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            ServicosGrupo::create($input);

            return redirect()->route('painel.servicos-grupos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(ServicosGrupo $grupo)
    {
        return view('painel.servicos.grupos.edit', compact('grupo'));
    }

    public function update(ServicosGruposRequest $request, ServicosGrupo $grupo)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            $grupo->update($input);

            return redirect()->route('painel.servicos-grupos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(ServicosGrupo $grupo)
    {
        try {
            $grupo->delete();

            return redirect()->route('painel.servicos-grupos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
