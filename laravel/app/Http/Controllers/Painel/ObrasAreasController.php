<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\AreaDeNegocio;
use App\Models\Obra;
use App\Models\ObraArea;

class ObrasAreasController extends Controller
{
    public function index($obra)
    {
        $obra = Obra::find($obra);

        $areas = ObraArea::obra($obra->id)
            ->join('areas_de_negocio', 'areas_de_negocio.id', '=', 'obras_areas.area_id')
            ->select('areas_de_negocio.id as area_id', 'areas_de_negocio.titulo as area_titulo', 'obras_areas.id as id')
            ->orderBy('area_id', 'ASC')
            ->get();

        return view('painel.galeria-de-imagens.areas.index', compact('areas', 'obra'));
    }

    public function create($obra)
    {
        $obra = Obra::find($obra);

        $areas = AreaDeNegocio::orderBy('id', 'ASC')->pluck('titulo', 'id');

        return view('painel.galeria-de-imagens.areas.create', compact('obra', 'areas'));
    }

    public function store(Request $request, $obra)
    {
        try {
            $obra = Obra::find($obra);

            $input = $request->all();
            $input['obra_id'] = $obra->id;

            $count = count(ObraArea::where('obra_id', $input['obra_id'])->where('area_id', $input['area_id'])->get());

            if ($count == 0) {
                ObraArea::create($input);
            } else {
                return back()->withErrors(['Erro ao adicionar registro: Verifique se essa área de negócio ja está cadastrada']);
            }

            return redirect()->route('painel.galeria-de-imagens.areas.index', $obra->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy($obra, $area)
    {
        try {
            $obra = Obra::find($obra);

            $area = ObraArea::find($area);

            $area->delete();

            return redirect()->route('painel.galeria-de-imagens.areas.index', $obra->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
