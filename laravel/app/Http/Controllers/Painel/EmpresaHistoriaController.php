<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmpresaHistoriaRequest;
use App\Models\EmpresaHistoria;

class EmpresaHistoriaController extends Controller
{
    public function index()
    {
        $registro = EmpresaHistoria::first();

        return view('painel.empresa.historia.edit', compact('registro'));
    }

    public function update(EmpresaHistoriaRequest $request, EmpresaHistoria $registro)
    {
        try {
            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.empresa-historia.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
