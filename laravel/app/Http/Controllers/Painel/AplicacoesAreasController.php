<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\AplicacoesAreasRequest;
use App\Models\Aplicacao;
use App\Models\AplicacaoArea;
use App\Models\AreaDeNegocio;

class AplicacoesAreasController extends Controller
{
    public function index(Aplicacao $aplicacao)
    {
        $areas = AplicacaoArea::aplicacao($aplicacao->id)
            ->join('areas_de_negocio', 'areas_de_negocio.id', '=', 'aplicacoes_areas.area_id')
            ->join('aplicacoes', 'aplicacoes.id', '=', 'aplicacoes_areas.aplicacao_id')
            ->select('areas_de_negocio.id as area_id', 'areas_de_negocio.titulo as area_titulo', 'aplicacoes.id as aplicacao_id', 'aplicacoes.titulo as aplicacao_titulo', 'aplicacoes_areas.id as id')
            ->orderBy('areas_de_negocio.id', 'ASC')
            ->get();

        return view('painel.aplicacoes.areas.index', compact('areas', 'aplicacao'));
    }

    public function create(Aplicacao $aplicacao)
    {
        $areas = AreaDeNegocio::orderBy('id', 'ASC')->pluck('titulo', 'id');

        return view('painel.aplicacoes.areas.create', compact('aplicacao', 'areas'));
    }

    public function store(AplicacoesAreasRequest $request, Aplicacao $aplicacao)
    {
        try {
            $input = $request->all();
            $input['aplicacao_id'] = $aplicacao->id;

            $count = count(AplicacaoArea::where('aplicacao_id', $aplicacao->id)->where('area_id', $input['area_id'])->get());
            
            if ($count == 0) {
                AplicacaoArea::create($input);
            } else {
                return back()->withErrors(['Erro ao adicionar registro: Verifique se essa área de negócio ja está cadastrada']);
            }

            return redirect()->route('painel.aplicacoes.areas.index', $aplicacao->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Aplicacao $aplicacao, $aplicacaoArea)
    {
        $areas = AreaDeNegocio::orderBy('id', 'ASC')->pluck('titulo', 'id');

        $aplicacaoArea = AplicacaoArea::find($aplicacaoArea);

        return view('painel.aplicacoes.areas.edit', compact('aplicacao', 'areas', 'aplicacaoArea'));
    }

    public function update(AplicacoesAreasRequest $request, Aplicacao $aplicacao, $aplicacaoArea)
    {
        try {
            $input = $request->all();
            $input['aplicacao_id'] = $aplicacao->id;

            $aplicacaoArea = AplicacaoArea::find($aplicacaoArea);

            $aplicacaoArea->update($input);

            return redirect()->route('painel.aplicacoes.areas.index', $aplicacao->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Aplicacao $registro, $aplicacaoArea)
    {
        try {
            $aplicacaoArea = AplicacaoArea::find($aplicacaoArea);

            $aplicacaoArea->delete();

            return redirect()->route('painel.aplicacoes.areas.index', $registro->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
