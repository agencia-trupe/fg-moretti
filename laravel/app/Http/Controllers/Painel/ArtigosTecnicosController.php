<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArtigosTecnicosRequest;
use App\Models\ArtigoTecnico;

class ArtigosTecnicosController extends Controller
{
    public function removerAcentos()
    {
        $conversao = array(
            'á' => 'a', 'à' => 'a', 'ã' => 'a', 'â' => 'a', 'é' => 'e',
            'ê' => 'e', 'í' => 'i', 'ï' => 'i', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', "ö" => "o",
            'ú' => 'u', 'ü' => 'u', 'ç' => 'c', 'ñ' => 'n', 'Á' => 'A', 'À' => 'A', 'Ã' => 'A',
            'Â' => 'A', 'É' => 'E', 'Ê' => 'E', 'Í' => 'I', 'Ï' => 'I', "Ö" => "O", 'Ó' => 'O',
            'Ô' => 'O', 'Õ' => 'O', 'Ú' => 'U', 'Ü' => 'U', 'Ç' => 'C', 'N' => 'Ñ'
        );

        return $conversao;
    }

    public function index()
    {
        $artigos = ArtigoTecnico::ordenados()->paginate(10);

        return view('painel.artigos-tecnicos.index', compact('artigos'));
    }

    public function create()
    {
        return view('painel.artigos-tecnicos.create');
    }

    public function store(ArtigosTecnicosRequest $request)
    {
        try {
            $input = $request->all();

            $file = $request->file('arquivo');

            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = strtoupper(str_replace(' ', '-', strtr($file->getClientOriginalName(), $this->removerAcentos())));
                    $path = public_path() . '/assets/artigos';
                    $file->move($path, $filename);
                    $input['arquivo'] = $filename;
                }
            }

            ArtigoTecnico::create($input);

            return redirect()->route('painel.artigos-tecnicos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(ArtigoTecnico $artigo)
    {
        return view('painel.artigos-tecnicos.edit', compact('artigo'));
    }

    public function update(ArtigosTecnicosRequest $request, ArtigoTecnico $artigo)
    {
        try {
            $input = $request->all();

            $file = $request->file('arquivo');

            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = strtoupper(str_replace(' ', '-', strtr($file->getClientOriginalName(), $this->removerAcentos())));
                    $path = public_path() . '/assets/artigos';
                    $file->move($path, $filename);
                    $input['arquivo'] = $filename;
                }
            }

            $artigo->update($input);

            return redirect()->route('painel.artigos-tecnicos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(ArtigoTecnico $artigo)
    {
        try {
            $artigo->delete();

            return redirect()->route('painel.artigos-tecnicos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
