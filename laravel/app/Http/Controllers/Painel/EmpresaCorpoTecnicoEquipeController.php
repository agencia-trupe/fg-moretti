<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmpresaCorpoTecnicoEquipeRequest;
use App\Models\EmpresaCorpoTecnico;
use App\Models\EmpresaCorpoTecnicoEquipe;

class EmpresaCorpoTecnicoEquipeController extends Controller
{
    public function index(EmpresaCorpoTecnico $registro)
    {
        $equipe = EmpresaCorpoTecnicoEquipe::corpoTecnico($registro->id)->ordenados()->get();

        return view('painel.empresa.corpo-tecnico.equipe.index', compact('equipe', 'registro'));
    }

    public function create(EmpresaCorpoTecnico $registro)
    {
        return view('painel.empresa.corpo-tecnico.equipe.create', compact('registro'));
    }

    public function store(EmpresaCorpoTecnico $registro, EmpresaCorpoTecnicoEquipeRequest $request)
    {
        try {
            $input = $request->all();
            $input['corpo_tecnico_id'] = $registro->id;

            if (isset($input['foto'])) $input['foto'] = EmpresaCorpoTecnicoEquipe::upload_foto();

            EmpresaCorpoTecnicoEquipe::create($input);

            return redirect()->route('painel.empresa-corpo-tecnico.equipe.index', $registro)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: ' . $e->getMessage();
        }
    }

    public function edit(EmpresaCorpoTecnico $registro, EmpresaCorpoTecnicoEquipe $equipe)
    {
        return view('painel.empresa.corpo-tecnico.equipe.edit', compact('registro', 'equipe'));
    }

    public function update(EmpresaCorpoTecnicoEquipeRequest $request, EmpresaCorpoTecnico $registro, EmpresaCorpoTecnicoEquipe $equipe)
    {
        try {
            $input = $request->all();
            $input['corpo_tecnico_id'] = $registro->id;

            if (isset($input['foto'])) $input['foto'] = EmpresaCorpoTecnicoEquipe::upload_foto();

            $equipe->update($input);

            return redirect()->route('painel.empresa-corpo-tecnico.equipe.index', $registro->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(EmpresaCorpoTecnico $registro, EmpresaCorpoTecnicoEquipe $equipe)
    {
        try {
            $equipe->delete();

            return redirect()->route('painel.empresa-corpo-tecnico.equipe.index', $registro->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
