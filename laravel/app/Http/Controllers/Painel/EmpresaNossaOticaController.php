<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmpresaNossaOticaRequest;
use App\Models\EmpresaNossaOtica;

class EmpresaNossaOticaController extends Controller
{
    public function index()
    {
        $registro = EmpresaNossaOtica::first();

        return view('painel.empresa.nossa-otica.edit', compact('registro'));
    }

    public function update(EmpresaNossaOticaRequest $request, EmpresaNossaOtica $registro)
    {
        try {
            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.empresa-nossa-otica.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
