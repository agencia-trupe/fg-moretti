<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CapasMenusRequest;
use App\Models\CapaMenu;

class CapasMenusController extends Controller
{
    public function index()
    {
        $capas = CapaMenu::orderBy('id', 'ASC')->get();

        return view('painel.home.capas-menus.index', compact('capas'));
    }

    public function edit(CapaMenu $capa)
    {
        return view('painel.home.capas-menus.edit', compact('capa'));
    }

    public function update(CapasMenusRequest $request, CapaMenu $capa)
    {
        try {
            $input = $request->all();
            $input['titulo'] = $capa->titulo;

            if (isset($input['imagem'])) $input['imagem'] = CapaMenu::upload_imagem();

            $capa->update($input);

            return redirect()->route('painel.capas-menus.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
