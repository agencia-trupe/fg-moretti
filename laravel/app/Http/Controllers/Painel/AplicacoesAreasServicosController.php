<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Aplicacao;
use App\Models\AplicacaoArea;
use App\Models\AplicacaoAreaServico;
use App\Models\AreaDeNegocio;
use App\Models\Servico;

class AplicacoesAreasServicosController extends Controller
{
    public function index(Aplicacao $aplicacao, $area)
    {
        $aplicacaoArea = AplicacaoArea::find($area);
        
        $servicos = AplicacaoAreaServico::where('aplicacao_area_id', $aplicacaoArea->id)
            ->join('servicos', 'servicos.id', '=', 'aplicacoes_areas_servicos.servico_id')
            ->select('servicos.id as servico_id', 'servicos.titulo as servico_titulo', 'aplicacoes_areas_servicos.id as id')
            ->orderBy('servico_titulo', 'ASC')
            ->get();

        $areaDeNegocio = AreaDeNegocio::find($aplicacaoArea->area_id);

        return view('painel.aplicacoes.areas.servicos.index', compact('area', 'aplicacao', 'servicos', 'areaDeNegocio'));
    }

    public function create(Aplicacao $aplicacao, $area)
    {
        $aplicacaoArea = AplicacaoArea::find($area);

        $servicos = Servico::orderBy('titulo', 'ASC')->pluck('titulo', 'id');

        $areaDeNegocio = AreaDeNegocio::find($aplicacaoArea->area_id);

        return view('painel.aplicacoes.areas.servicos.create', compact('aplicacao', 'area', 'servicos', 'areaDeNegocio'));
    }

    public function store(Request $request, Aplicacao $aplicacao, $area)
    {
        try {
            $input = $request->all();
            $input['aplicacao_area_id'] = $area;

            $count = count(AplicacaoAreaServico::where('aplicacao_area_id', $input['aplicacao_area_id'])->where('servico_id', $input['servico_id'])->get());
            
            if ($count == 0) {
                AplicacaoAreaServico::create($input);
            } else {
                return back()->withErrors(['Erro ao adicionar registro: Verifique se esse serviço ja está cadastrado']);
            }

            return redirect()->route('painel.aplicacoes.areas.servicos.index', [$aplicacao->id, $area])->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Aplicacao $aplicacao, $area_id, $servico_id)
    {
        try {
            $area = AplicacaoArea::where('id', $area_id)->where('aplicacao_id', $aplicacao->id)->first();
            $servico = AplicacaoAreaServico::where('servico_id', $servico_id->id)->where('aplicacao_area_id', $area->id)->first();

            $servico->delete();

            return redirect()->route('painel.aplicacoes.areas.servicos.index', [$aplicacao->id, $area->id])->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
