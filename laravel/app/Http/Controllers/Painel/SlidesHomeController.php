<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\SlidesHomeRequest;
use App\Models\SlideHome;

class SlidesHomeController extends Controller
{
    public function index()
    {
        $slides = SlideHome::ordenados()->get();

        return view('painel.home.slides.index', compact('slides'));
    }

    public function create()
    {
        return view('painel.home.slides.create');
    }

    public function store(SlidesHomeRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = SlideHome::upload_imagem();

            SlideHome::create($input);

            return redirect()->route('painel.slides-home.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(SlideHome $slide)
    {
        return view('painel.home.slides.edit', compact('slide'));
    }

    public function update(SlidesHomeRequest $request, SlideHome $slide)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = SlideHome::upload_imagem();

            $slide->update($input);

            return redirect()->route('painel.slides-home.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(SlideHome $slide)
    {
        try {
            $slide->delete();

            return redirect()->route('painel.slides-home.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
