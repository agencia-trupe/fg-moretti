<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAcGerentesObrasTable extends Migration
{
    public function up()
    {
        Schema::create('ac_gerentes_obras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('usuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('ac_clientes_usuarios')->onDelete('cascade');
            $table->integer('obra_id')->unsigned();
            $table->foreign('obra_id')->references('id')->on('ac_obras')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ac_gerentes_obras');
    }
}
