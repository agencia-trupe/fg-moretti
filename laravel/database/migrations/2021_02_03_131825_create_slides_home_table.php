<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateSlidesHomeTable extends Migration
{
    public function up()
    {
        Schema::create('slides_home', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('frase');
            $table->string('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('slides_home');
    }
}
