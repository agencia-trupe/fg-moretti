<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAcObrasComunicacoesTable extends Migration
{
    public function up()
    {
        Schema::create('ac_obras_comunicacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('obra_id')->unsigned();
            $table->foreign('obra_id')->references('id')->on('ac_obras')->onDelete('cascade');
            $table->integer('empresa_id')->unsigned()->nullable();
            $table->integer('usuario_id')->unsigned()->nullable();
            $table->integer('admin_id')->unsigned()->nullable();
            $table->text('mensagem');
            $table->boolean('respondido')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ac_obras_comunicacoes');
    }
}
