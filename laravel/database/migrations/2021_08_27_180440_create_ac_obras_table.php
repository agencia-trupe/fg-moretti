<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAcObrasTable extends Migration
{
    public function up()
    {
        Schema::create('ac_obras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('titulo');
            $table->string('localizacao');
            $table->string('codigo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ac_obras');
    }
}
