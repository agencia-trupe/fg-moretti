<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateEmpresaNossaOticaTable extends Migration
{
    public function up()
    {
        Schema::create('empresa_nossa_otica', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->text('visao');
            $table->text('missao');
            $table->text('valores');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresa_nossa_otica');
    }
}
