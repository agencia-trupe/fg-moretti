<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAplicacoesAreasTable extends Migration
{
    public function up()
    {
        Schema::create('aplicacoes_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('aplicacao_id')->unsigned();
            $table->foreign('aplicacao_id')->references('id')->on('aplicacoes')->onDelete('cascade');
            $table->integer('area_id')->unsigned();
            $table->foreign('area_id')->references('id')->on('areas_de_negocio')->onDelete('cascade');
            $table->text('texto_area');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('aplicacoes_areas');
    }
}
