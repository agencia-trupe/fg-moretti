<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateEmpresaCorpoTecnicoTable extends Migration
{
    public function up()
    {
        Schema::create('empresa_corpo_tecnico', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresa_corpo_tecnico');
    }
}
