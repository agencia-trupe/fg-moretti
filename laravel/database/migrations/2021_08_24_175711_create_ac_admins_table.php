<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAcAdminsTable extends Migration
{
    public function up()
    {
        Schema::create('ac_admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->boolean('ativo')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ac_admins');
    }
}
