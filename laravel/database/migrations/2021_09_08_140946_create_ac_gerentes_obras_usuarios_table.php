<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAcGerentesObrasUsuariosTable extends Migration
{
    public function up()
    {
        Schema::create('ac_gerentes_obras_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gerente_id')->unsigned();
            $table->foreign('gerente_id')->references('id')->on('ac_clientes_usuarios')->onDelete('cascade');
            $table->integer('obra_id')->unsigned();
            $table->foreign('obra_id')->references('id')->on('ac_obras')->onDelete('cascade');
            $table->integer('usuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('ac_clientes_usuarios')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ac_gerentes_obras_usuarios');
    }
}
