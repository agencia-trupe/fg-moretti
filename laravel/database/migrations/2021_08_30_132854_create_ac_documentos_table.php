<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAcDocumentosTable extends Migration
{
    public function up()
    {
        Schema::create('ac_documentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('obra_id')->unsigned();
            $table->foreign('obra_id')->references('id')->on('ac_obras')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo');
            $table->text('descricao');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ac_documentos');
    }
}
