<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAcDocumentosVersoesTable extends Migration
{
    public function up()
    {
        Schema::create('ac_documentos_versoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('documento_id')->unsigned();
            $table->foreign('documento_id')->references('id')->on('ac_documentos')->onDelete('cascade');
            $table->string('versao');
            $table->string('slug');
            $table->string('titulo');
            $table->string('arquivo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ac_documentos_versoes');
    }
}
