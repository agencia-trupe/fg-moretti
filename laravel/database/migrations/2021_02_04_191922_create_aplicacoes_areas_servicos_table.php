<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAplicacoesAreasServicosTable extends Migration
{
    public function up()
    {
        Schema::create('aplicacoes_areas_servicos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('servico_id')->unsigned();
            $table->foreign('servico_id')->references('id')->on('servicos')->onDelete('cascade');
            $table->integer('aplicacao_area_id')->unsigned();
            $table->foreign('aplicacao_area_id')->references('id')->on('aplicacoes_areas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('aplicacoes_areas_servicos');
    }
}
