<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateCapasMenusTable extends Migration
{
    public function up()
    {
        Schema::create('capas_menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('titulo');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('capas_menus');
    }
}
