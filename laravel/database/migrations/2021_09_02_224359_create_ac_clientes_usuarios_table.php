<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAcClientesUsuariosTable extends Migration
{
    public function up()
    {
        Schema::create('ac_clientes_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('id')->on('ac_empresas')->onDelete('cascade');
            $table->string('slug');
            $table->string('nome');
            $table->string('telefone');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->rememberToken();
            $table->boolean('ativo')->default(false);
            $table->boolean('gerente')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ac_clientes_usuarios');
    }
}
