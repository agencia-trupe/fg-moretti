<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAreasDeNegocioTable extends Migration
{
    public function up()
    {
        Schema::create('areas_de_negocio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('titulo');
            $table->text('texto_inicial');
            $table->text('texto_servicos');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('areas_de_negocio');
    }
}
