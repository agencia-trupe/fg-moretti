<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateArtigosTecnicosTable extends Migration
{
    public function up()
    {
        Schema::create('artigos_tecnicos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo');
            $table->string('arquivo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('artigos_tecnicos');
    }
}
