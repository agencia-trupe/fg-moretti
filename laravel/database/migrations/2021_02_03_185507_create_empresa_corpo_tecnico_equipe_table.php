<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateEmpresaCorpoTecnicoEquipeTable extends Migration
{
    public function up()
    {
        Schema::create('empresa_corpo_tecnico_equipe', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->integer('corpo_tecnico_id')->unsigned();
            $table->foreign('corpo_tecnico_id')->references('id')->on('empresa_corpo_tecnico')->onDelete('cascade');
            $table->string('nome');
            $table->string('foto');
            $table->text('texto_sobre');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresa_corpo_tecnico_equipe');
    }
}
