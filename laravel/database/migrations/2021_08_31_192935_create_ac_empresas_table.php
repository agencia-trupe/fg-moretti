<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAcEmpresasTable extends Migration
{
    public function up()
    {
        Schema::create('ac_empresas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cnpj');
            $table->string('slug');
            $table->string('nome');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ac_empresas');
    }
}
