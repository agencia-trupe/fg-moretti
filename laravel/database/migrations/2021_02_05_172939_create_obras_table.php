<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateObrasTable extends Migration
{
    public function up()
    {
        Schema::create('obras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->integer('aplicacao_id')->unsigned();
            $table->foreign('aplicacao_id')->references('id')->on('aplicacoes')->onDelete('cascade');
            $table->string('slug');
            $table->string('titulo');
            $table->text('texto');
            $table->string('capa');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('obras');
    }
}
