<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ConfiguracoesTableSeeder::class);
        $this->call(ContatoTableSeeder::class);
        $this->call(HomeTableSeeder::class);
        $this->call(CapasMenusTableSeeder::class);
        $this->call(EmpresaHistoriaTableSeeder::class);
        $this->call(EmpresaNossaOticaTableSeeder::class);
        $this->call(EmpresaCorpoTecnicoTableSeeder::class);
        $this->call(AreasDeNegocioTableSeeder::class);
        $this->call(ServicosPaginaTableSeeder::class);
        $this->call(ServicosGruposTableSeeder::class);
        $this->call(TermosDeUsoTableSeeder::class);
        $this->call(PoliticaDePrivacidadeTableSeeder::class);
        $this->call(ACAdminsTableSeeder::class);
    }
}
