<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServicosGruposTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('servicos_grupos')->insert([
            'id'     => 1,
            'slug'   => 'carga',
            'titulo' => 'Carga',
        ]);

        DB::table('servicos_grupos')->insert([
            'id'     => 2,
            'slug'   => 'deformacao',
            'titulo' => 'Deformação',
        ]);

        DB::table('servicos_grupos')->insert([
            'id'     => 3,
            'slug'   => 'deslocamento',
            'titulo' => 'Deslocamento',
        ]);

        DB::table('servicos_grupos')->insert([
            'id'     => 4,
            'slug'   => 'ensaios-em-fundacoes',
            'titulo' => 'Ensaios em Fundações',
        ]);

        DB::table('servicos_grupos')->insert([
            'id'     => 5,
            'slug'   => 'inclinacao',
            'titulo' => 'Inclinação',
        ]);

        DB::table('servicos_grupos')->insert([
            'id'     => 6,
            'slug'   => 'pressao-terra',
            'titulo' => 'Pressão (Terra)',
        ]);

        DB::table('servicos_grupos')->insert([
            'id'     => 7,
            'slug'   => 'pressao-nivel-dagua',
            'titulo' => "Pressão/Nível D'Água",
        ]);

        DB::table('servicos_grupos')->insert([
            'id'     => 8,
            'slug'   => 'temperatura',
            'titulo' => 'Temperatura',
        ]);

        DB::table('servicos_grupos')->insert([
            'id'     => 9,
            'slug'   => 'vibracao',
            'titulo' => 'Vibração',
        ]);
    }
}
