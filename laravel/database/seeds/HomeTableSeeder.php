<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HomeTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('home')->insert([
            'titulo' => '',
            'texto'  => '',
        ]);
    }
}
