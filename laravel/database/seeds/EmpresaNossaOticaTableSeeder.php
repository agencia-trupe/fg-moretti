<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmpresaNossaOticaTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('empresa_nossa_otica')->insert([
            'texto'   => '',
            'visao'   => '',
            'missao'  => '',
            'valores' => '',
        ]);
    }
}
