<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CapasMenusTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('capas_menus')->insert([
            'id'     => 1,
            'slug'   => 'empresa',
            'titulo' => 'Empresa',
            'imagem' => '',
        ]);

        DB::table('capas_menus')->insert([
            'id'     => 2,
            'slug'   => 'areas-de-negocio',
            'titulo' => 'Áreas de negócio',
            'imagem' => '',
        ]);

        DB::table('capas_menus')->insert([
            'id'     => 3,
            'slug'   => 'aplicacoes',
            'titulo' => 'Aplicações',
            'imagem' => '',
        ]);

        DB::table('capas_menus')->insert([
            'id'     => 4,
            'slug'   => 'servicos',
            'titulo' => 'Serviços',
            'imagem' => '',
        ]);

        DB::table('capas_menus')->insert([
            'id'      => 5,
            'slug'   => 'galeria-de-imagens',
            'titulo' => 'Galeria de imagens',
            'imagem' => '',
        ]);

        DB::table('capas_menus')->insert([
            'id'      => 6,
            'slug'   => 'artigos-tecnicos',
            'titulo' => 'Artigos técnicos',
            'imagem' => '',
        ]);

        DB::table('capas_menus')->insert([
            'id'      => 7,
            'slug'   => 'clientes',
            'titulo' => 'Clientes',
            'imagem' => '',
        ]);

        DB::table('capas_menus')->insert([
            'id'      => 8,
            'slug'   => 'contato',
            'titulo' => 'Contato',
            'imagem' => '',
        ]);
    }
}
