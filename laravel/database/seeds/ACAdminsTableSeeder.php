<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ACAdminsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('ac_admins')->insert([
            'name'       => 'Trupe Admin',
            'email'      => 'trupe@trupe.net',
            'password'   => bcrypt('senhatrupe'),
            'ativo'      => 1,
            'created_at' => Carbon::now()
        ]);
    }
}
