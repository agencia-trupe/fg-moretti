<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AreasDeNegocioTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('areas_de_negocio')->insert([
            'id'             => 1,
            'slug'           => 'projetos-e-consultoria-em-geotecnia',
            'titulo'         => 'Projetos e Consultoria em Geotecnia',
            'texto_inicial'  => '',
            'texto_servicos' => '',
            'imagem'         => '',
        ]);

        DB::table('areas_de_negocio')->insert([
            'id'             => 2,
            'slug'           => 'instrumentacao-geotecnica-e-estrutural',
            'titulo'         => 'Instrumentação Geotécnica e Estrutural',
            'texto_inicial'  => '',
            'texto_servicos' => '',
            'imagem'         => '',
        ]);

        DB::table('areas_de_negocio')->insert([
            'id'             => 3,
            'slug'           => 'investigacao-geotecnica',
            'titulo'         => 'Investigação Geotécnica',
            'texto_inicial'  => '',
            'texto_servicos' => '',
            'imagem'         => '',
        ]);

        DB::table('areas_de_negocio')->insert([
            'id'             => 4,
            'slug'           => 'obras-geotecnicas',
            'titulo'         => 'Obras Geotécnicas',
            'texto_inicial'  => '',
            'texto_servicos' => '',
            'imagem'         => '',
        ]);
    }
}
