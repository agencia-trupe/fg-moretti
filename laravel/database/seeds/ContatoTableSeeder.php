<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contato')->insert([
            'nome'        => 'FG MORETTI',
            'email'       => 'contato@trupe.net',
            'telefone'    => '11 3624 8979',
            'celular'     => '11 98765 4321',
            'endereco'    => 'Rua André de Leão 81 • Brás 03101-010 • São Paulo, SP',
            'horario'     => 'Segunda à Sexta - das 8h às 18h',
            'facebook'    => 'https://www.facebook.com/Grupo-FG-Moretti-101801544960118',
            'linkedin'    => 'https://www.linkedin.com/company/fgmoretti/',
            'instagram'   => 'https://www.instagram.com/fgmoretti/',
            'google_maps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.467711066183!2d-46.61585628502248!3d-23.55163988468772!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5917eaf67ae5%3A0xb2bce225728b357!2zUnVhIEFuZHLDqSBkZSBMZcOjbywgODEgLSBCcsOhcywgU8OjbyBQYXVsbyAtIFNQLCAwMzEwMS0wMTA!5e0!3m2!1spt-BR!2sbr!4v1612289293927!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>',
        ]);
    }
}
